$(document).ready(function() {


    /*******************************************
    
        reset

    ********************************************/

    $("#result .pairing_box").empty().css("border","none");	
    $(".blank1").css("display","none");
    $(".Doc_Maj, #lang_title, #btnSubmit").hide();

    /*******************************************
    
        parse 버튼 

    ********************************************/

    $("body").on("click","#btnParse", function(e){
        e.preventDefault();
        
        $(".Doc_Maj, #lang_title, #btnSubmit").show();
        $("#result .text_box , .pairing_box, .blank1, .blank2").remove();

        var lang1 = $("#lang1 option:selected").val();
        var lang2 = $("#lang2 option:selected").val();

        var lang1_t = $("#lang1 option:selected").text();
        var lang2_t = $("#lang2 option:selected").text();
        
        var lang1Text = $("#userTxt1").val();
        var lang2Text = $("#userTxt2").val();

        var formData = new FormData();
        formData.append("original_string", lang1Text);
        formData.append("translated_string", lang2Text);

		$.ajax({
            url: "/api/v2/admin/dataManager/parseSentence",
            type: "post",
            dataType: "json",
            data: formData,
            contentType: false,
            processData: false
        }).done(function(data) {

            var splitedText1=[];
            var splitedText2=[];
            $.each(data.original_string.sentences, function(index, item){
                splitedText1.push(item);
            });

            $.each(data.translated_string.sentences, function(index, item){
                splitedText2.push(item);
            });


        /*******************************************
        
            마로님 코드 (DB submit data ajax)

        ********************************************/
            
       /* for(var item in data.original_string) {
            console.log(item);
            for(var item2 in data.original_string[item].sentences){
                splitedText1.push(data.original_string[item].sentences[]);
            }
        }
        for(var item in data.translated_string) {
            console.log(item);
            for(var item2 in data.translated_string[item].sentences){
                splitedText1.push(data.translated_string[item].sentences[]);
            }
        }*/

            if(lang1 == lang2) {
                alert ("언어를 다르게 설정해 주세요.");
            } else if (lang1Text=='') {
                alert ("언어1 텍스트 박스가 비어있습니다.");
            } else if (lang2Text=='') {
                alert ("언어2 텍스트 박스가 비어있습니다.");
            } else {
                $(".lang_box:nth-child(1) span").text("Original : "+lang1_t);
                $(".lang_box:nth-child(2) span").text("Target : "+lang2_t);

                var splitedTextVal = splitedText1.length > splitedText2.length ? splitedText1.length : splitedText2.length;
                var textLength1 = splitedText1.length;
                var textLength2 = splitedText2.length;
                
                for(i=0; i<splitedTextVal; i++){
                    if ( splitedText1[i] == undefined) {
                        splitedText1[i] = '';
                    } else if ( splitedText2[i] == undefined) {
                        splitedText2[i] = '';
                    } 

                    if(i<splitedTextVal){
                        $("#result").append("<div class='pairing_box'><div class='text_box'><span class='ui-state-default' id='lang1_"+i+"'>"
                        +splitedText1[i]+"</span></div>"+
                        "<div class='text_box'><span class='ui-state-default' id='lang2_"+i+"'>"+splitedText2[i]+
                        "</span></div></div>"+"<div class='blank1'><p>문단으로 나누시려면 클릭하세요.</p></div>"+"<div class='clearFix'></div>");
                        
                    } 
                
                };

                var spanVal = $(".pairing_box").find("span");
                var spanLength = $(".pairing_box").find("span").length;
                $(spanVal).each(function(index){
                    if(index<=spanLength-1){
                        if($(this).text()==""){

                            $(this).remove();
                        }
                    }
                });

                var textBox = $("span").parents(".text_box");
                textBox.addClass("spanText");

                var last_blank = $(".blank1").last();
                last_blank.remove();

                var Doc1_t = $("#kind_Doc option:selected").text();
                var Maj1_t = $("#kind_Maj option:selected").text();
                var Tone1_t = $("#kind_tone option:selected").text();
                var c_Name = $("#cName").val();

                $(".kind_box:nth-child(1) span").text(Doc1_t);
                $(".kind_box:nth-child(2) span").text(Maj1_t);
                $(".kind_box:nth-child(3) span").text(Tone1_t);
                $(".kind_box:nth-child(4) span").text(c_Name);

                $(".blank1").on("click", function(e){
                    e.preventDefault();
                    $(this).toggleClass("blank2");
                });

                $("body").off("mouseenter mouseleave").on("mouseenter mouseleave", ".blank1", function(){
                    $(this).children("p").toggleClass("section");
                    $(this).children("p").text("문단으로 나누시려면 클릭하세요.");
                });

                $("body").on("mouseenter mouseleave", ".blank2", function(){
                    $(this).children("p").text("문단 나누기 취소");
                });

                $(".text_box span").hover(function(){
                    $(this).css({"background":"rgba(0,0,0,0.2)"});
                }, function(){
                    $(this).css({"background":"#fff"});
                });

        /*******************************************
        
            문장 drop & drag event

        ********************************************/
            $(function(){

                $(".text_box span").draggable({
                    axis:"y",
                    start: function(event, ui){
                        dropped = false;
                    },
                    stop: function(event, ui){
                        if(dropped==true){
                            $(this).remove();
                        }
                    },
                    refreshPositions:true,
                    revert:"invalid",
                    zIndex:1
                });

                $(".text_box").droppable({
                        hoverClass:"hoverText",
                        accept:".text_box span",
                        drop:function(event, ui){
                            var dropped = ui.draggable;
                            var droppedOn = $(this);
                            var pairText = $(".pairing_box").find(".text_box");
                            var last_blank = $(".blank1").last();
                            var last_clear = $(".clearFix").last();
                            $(dropped).detach().css({top:0}).prependTo(droppedOn);
                            $.each(pairText, function(index,item){
                                if($(this).text()==""){
                                    $(this).removeClass("spanText");
                                } else {
                                    $(this).addClass("spanText");
                                }
                            });
                            $.each($(".pairing_box"),function(index, item){
                                console.log($(".pairing_box").length+", "+$(".text_box:nth-child(1) span").length);
                                if($(".pairing_box").length>$(".text_box:nth-child(1) span").length){

                                    if($(this).text()==""){
                                        $(this).remove();
                                        last_blank.remove();
                                        last_clear.remove();
                                    }
                                }
                            });
                        }
                    });
                    
                    $(".text_box span").droppable({
                        hoverClass:"spanText",
                        accept:".text_box span",
                        drop:function(event, ui){
                            var dropped = ui.draggable;
                            var droppedOn = $(this);

                            var right_textBox = $(".text_box:nth-child(2)");
                            var last_blank = $(".blank1").last();
                            var last_clear = $(".clearFix").last();

                            $(dropped).detach().css({top:0}).insertAfter(droppedOn);
                            for(i=0; i<right_textBox.length; i++){
                                if(right_textBox[i].children.length==0){
                                    for(j=i; j<right_textBox.length - 1;j++){
                                        $(right_textBox[j]).append(right_textBox[j+1].children);
                                    }
                                    for(j=i; j<right_textBox.length;j++){
                                        if(right_textBox[j].children.length==0){
                                            $(right_textBox[j]).removeClass("spanText");
                                        } else {
                                            $(right_textBox[j]).addClass("spanText");
                                        }
                                    }
                                    break;
                                    
                                }
                                // last_blank.remove();  
                            }
                            $.each($(".pairing_box"),function(index, item){
                                /*if($(this).find(".spanText:nth-child(1) span").length>1){
                                    alert("Original언어는 1문장씩 넣어주세요.");
                                    return false;
                                };*/
                                if($(".pairing_box").length>$(".text_box:nth-child(1) span").length){

                                    if($(this).text()==""){
                                        $(this).remove();
                                        last_blank.remove();
                                        last_clear.remove();
                                    }
                                } 
                            });
                        }
                    });
                    
                    $("div, span").disableSelection();

                });
            };

        }).fail(function(err) {
            console.log(err);
        });
        
    }); 


    /*******************************************
    
        submit 버튼 

    ********************************************/  

    
    $("body").on("click", "#btnSubmit", function(e) { 
        e.preventDefault();

        var pairText = $(".pairing_box");
        var isOk = true;
        $.each(pairText, function(index, item){
            if($(this).find(".text_box:nth-child(1) span").length>1){
                alert("Original언어는 1문장씩 넣어주세요.");
                isOk = false;
                return false;
            }
            if($(this).find(".spanText:nth-child(2)").length==0){
                alert("Original언어와 Target언어의 짝이 맞지 않습니다.");
                isOk = false;
                return false;
            } else if ($(this).find(".spanText:nth-child(1)").length==0){
                alert("Original언어와 Target언어의 짝이 맞지 않습니다.");
                isOk = false;
                return false;
            }
        });

        if(isOk){
            var lang1 = $('#lang1 option:selected').val();	
            var lang2 = $('#lang2 option:selected').val();
            var Doc1 = $('#kind_Doc option:selected').val();
            var Maj1 = $('#kind_Maj option:selected').val();
            var Tone1 = $('#kind_tone option:selected').val();
            var c_Name = $('#cName').val();
            var str = '';
            var spanText = $('.pairing_box');

            var formData = {
                original_language_id: lang1,
                target_language_id:lang2,
                subject_id: Maj1,
                format_id: Doc1,
                tone_id: Tone1,
                data: []
            };

            var nowParagraph = 1;
            var nowSentence = 1;        

            formData.data.push({"paragraph_id": nowParagraph, "sentences": []}); 
            
            $.each($("#result").find(".pairing_box, .blank2"), function(index, item){
                if($(item).hasClass("pairing_box")){
                    formData.data[nowParagraph - 1].sentences.push(
                        {
                            "sentence_id": nowSentence,
                            "original_sentence": $($(item).find(".text_box")[0]).text(),
                            "translated_sentence": $($(item).find(".text_box")[1]).text()
                        });
                    nowSentence++;
                }
                else if($(item).hasClass("blank2")){
                    nowParagraph++;
                    nowSentence = 1;
                    formData.data.push({"paragraph_id": nowParagraph, "sentences": []}); 
                    
                }
            });

            // console.log(JSON.stringify(formData, null, 4));

            $.ajax({
                url: "/api/v2/admin/dataManager/import",
                type: "post",
                data: JSON.stringify(formData),
                contentType: "application/json;",
            }).done(function(data) {
                alert("전송");
                console.log("성공");
                // location.reload();
            }).fail(function(err) {
                console.log(err);
            });
        }

    });
});