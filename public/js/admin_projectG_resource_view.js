//프로젝트 리소스 정보 보기
function get_resource_view(data) {
    $.each(data.data, function (index, item) {
        if (item.id == QueryString.rid) {
            var translator = "";
            // var is_original = "";
            var count = 0;
            $.each(item.translator_list, function (index2, item2) {
                if (item2.length == 1) translator = item2.name;
                else {
                    count++;
                    if (item2.length > count) {
                        translator += item2.name + ", ";
                    } else {
                        translator += item2.name;
                    }
                }
            });

            // if(item.is_original == true) is_original = "있음";
            // else is_original = "없음";

            $(".text").eq(0).text(item.id);
            $(".text").eq(1).text(item.target_language);
            $(".text").eq(2).text(item.theme);
            $(".text").eq(3).text(translator);
            $(".text").eq(4).text(item.description);
            $(".text").eq(5).text(tone[item.tone_id]);
            $(".text").eq(6).text(permission_level[item.read_permission_level]);
            $(".text").eq(7).text(item.price + "원");
            // $(".text").eq(8).text(is_original);
            // console.log(item.resource_info.length);
            if (item.resource_info.length === 0) {
                $(".fileTable").append(
                    "<tr>" +
                    "<td colspan='3'>파일이 없습니다.</td>" +
                    "</tr>"
                );
            } else {
                $.each(item.resource_info, function (index2, item2) {
                    var fileName = item2.file_url.split("/").pop();
                    $(".fileTable").append(
                        "<tr>" +
                        "<td>" +
                        "<a title='" + item2.file_url + "' class='popup_btn'>" + fileName + "</a>" +
                        "</td>" +
                        "<td> - </td>" +
                        "<td class='text_center'>" + preview_permission[item2.preview_permission] + "</td>" +
                        "</tr>"
                    );
                });
            }
        }
    });
}

$("body").on("click", ".popup_btn", function () {
    $(".popup_box").find("iframe").attr("src", $(this).attr("title"));
    $(".popup").find("h3").text($(this).text());
});
