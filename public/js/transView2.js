/*******************************************
    
    번역글을 가지고 이동되었을 때,

********************************************/
window.onload = function() {
    var $getText = $("textarea");
    
    var $text = sessionStorage.getItem("textValue");
    console.log($text);
    if($text) {
        $getText.val($text);
        $(document).ready(function() {
            $(".translate button").trigger("click");
        });
    }
    sessionStorage.removeItem("textValue");
}


$(document).ready(function() {
    /*******************************************
    
        전역변수 : 
            var language: 언어선택 값 받는 변수.

    ********************************************/
    var trans = false;
    var language = 0;
    var lang = $("select[name='lang_select'] option:selected").text().trim();
    var arr = lang.split("->");
    var translation_store_id = 0;
    
    $("#original_lang").text(arr[0]);
    $("#target_lang").text(arr[1]);

    /*******************************************
    
        번역물 글자수 체크 및 제한

    ********************************************/
    $("body").on("keyup", "textarea", function() {
        var content_count = $(this).val();
        $('.count').text(content_count.length + '/1000');
    });

    /*******************************************
    
        로딩 글자 이벤트

    ********************************************/
    var count = 0;
    var comma = "";
    function loadingSart() {
        count++;
        comma += ".";
        if(count == 4) {
            count = 0;
            comma = "";
        }
        $("#loadingText span").text(comma);
    }
    var loadingStop = setInterval(loadingSart, 600);

    /*******************************************
    
        번역하기 클릭시 이벤트 내용

    ********************************************/
    $("body").on("click", ".translate button", function() {
        var google_data = "";
        var bing_data = "";
        var ciceron_data = "";

        if($(".input_translations2 div textarea").val()) {
            // $("#loading2").css("display", "block");
            var formData = new FormData();
            if(language == 0) {
                formData.append("source_lang_id", 2);
                formData.append("target_lang_id", 1);
            } else if(language == 1) {
                formData.append("source_lang_id", 1);
                formData.append("target_lang_id", 2);
            }
            formData.append("sentence", $(".input_translations2 div textarea").val());
            formData.append("user_email", "admin@sexycookie.com");
            formData.append("where", "phone");
            $.ajax({
                //url: "/api/v2/user/translator",
                //type: "post",
                //dataType: "json",
                //processData: false,
                //contentType: false,
                //data: formData
                url: "https://translator.ciceron.me/translate",
                type: "post",
                dataType: "json",
                contentType: false,
                processData: false,
                data: formData
            }).done(function(data) {
                console.log("here");
                // console.log(data)
                google_data = data.google;
                bing_data = data.bing;
                ciceron_data = data.ciceron;

                $("div.get_left").text(ciceron_data);
                if($("body").width() <= 779) {
                    var offset = $(".get_translations2").offset();
                    $("html, body").animate({scrollTop : (offset.top - 10)}, 600);
                }
                $(".popUp").css("display", "block");
                $(".popUp_checked").css("display", "none");
                $("div.get_right").text(bing_data);

                // 번역 결과 삽입
                var resultData = new FormData();
                if(language == 0) {
                    resultData.append("source_lang", 'en');
                    resultData.append("target_lang", 'ko');
                } else if(language == 1) {
                    resultData.append("source_lang", 'ko');
                    resultData.append("target_lang", 'en');
                }

                resultData.append("original_text", $(".input_translations2 div textarea").val());
                resultData.append("google_result", google_data);
                resultData.append("bing_result", bing_data);
                resultData.append("ciceron_result", ciceron_data);

                $.ajax({
                    url: "/api/v2/user/translator/store",
                    type: "post",
                    dataType: "json",
                    contentType: false,
                    processData: false,
                    data: resultData
                }).done(function(data){
                    translation_store_id = data.new_id;
                });

                trans = true;
                clearInterval(loadingStop);
                // console.log(trans);
                //var transData = new FormData();
                //if(language == 0) {
                //    transData.append("source_lang_id", 2);
                //    transData.append("target_lang_id", 1);
                //} else if(language == 1) {
                //    transData.append("source_lang_id", 1);
                //    transData.append("target_lang_id", 2);
                //}
            }).fail(function(err) {
                console.log(err)
                $("#loadingText").text("서버에러");
            });

            $(".get_translations a").css("display", "block");
            $(".popUp button").text("선택하기");


        } else {
            alert("번역할 것이 없습니다. 입력해주세욧!!");
        }
    });

    /*******************************************
    
        언어설정 변경

    ********************************************/
    $("body").on("change", "select[name='lang_select']", function() {
        $("textarea").val("");
        var lang = $("select[name='lang_select'] option:selected").text().trim();
        var langChage = lang.split("->")
        $("#target_lang").text(langChage[1]);
        $("#original_lang").text(langChage[0]);
        // console.log($("#original_lang_list").text());
        // console.log(langChage[0]);
        // console.log(typeof(langChage[0]));
        if(langChage[0].indexOf("한국어")>-1) {
            language = 1;
            // console.log(language);
        } else if(langChage[0].indexOf("영어")>-1) {
            language = 0;
            // console.log(language);
        }
    });

    /*******************************************
    
        스크롤 이벤트

    ********************************************/
    var screenWidth = $(window).innerWidth() + 15;
    var scroll = $(window).scrollTop();
    // console.log(screenWidth);
    // console.log(scroll);
    $(window).scroll(function() {
        // console.log($(window).scrollTop());
        if(screenWidth <= 760) {
            if($(window).scrollTop() >= 260 && $(window).scrollTop() <= 1100) {
                $("#slogan").css({
                    "display": "block",
                    "position": "fixed",
                    "bottom": "0px",
                    "left": "0px"
                });
            } else {
                $("#slogan").css({
                    "display": "none"
                });
            }
        }
    });

    /*******************************************
    
        반응형 이벤트

    ********************************************/
    $(window).resize(function() {
        screenWidth = $(window).innerWidth() + 15;
        // console.log($(window).scrollTop());
        if(screenWidth <= 760) {
            if($(window).scrollTop() >= 260) {
                $("#slogan").css({
                    "display": "block",
                    "position": "fixed",
                    "bottom": "0px",
                    "left": "0px"
                });
            }
        } else {
            $("#slogan").css({
                "display": "block",
                "position": "static",
            });
        }
        if(screenWidth <= 640) {
            $("#ad img").attr("src", "img/baogao_M.png")
        } else {
            $("#ad img").attr("src", "img/baogao_banner.png")
        }
        // console.log(screenWidth);
    });

    /*******************************************
    
        textarea 이벤트

    ********************************************/
    $("body").on("focus", "textarea", function() {
        $(this).parent().css({
            "box-shadow": "0 7px 14px rgba(0,0,0,0.25), 0 5px 10px rgba(0,0,0,0.22)",
            "-webkit-transition": "all 0.05s ease-in-out",
            "-moz-transition": "all 0.05s ease-in-out",
            "transition": "all 0.05s ease-in-out"
        });
    });
    $("body").on("blur", "textarea", function() {
        $(this).parent().css({
            "box-shadow": "1px 1px 1px 1px #bbbdc0",
            "-webkit-transition": "all 0.05s ease-in-out",
            "-moz-transition": "all 0.05s ease-in-out",
            "transition": "all 0.05s ease-in-out"
        });
    });
    
    /*******************************************
    
        번열물 선택 이벤트

    ********************************************/
    $("body").on("click", ".popUp button", function() {
        var selection = "";
        if(trans == true) {
            $("div.get_left").on('click', function(){
                selection = "ciceron";
            });
            $("div.get_right").on('click', function(){
                selection = "bing";
            });

            var voteData = new FormData();
            voteData.append('result_id', translation_store_id);
            voteData.append('versus', 'bing');
            voteData.append('vote_to', selection);

            $.ajax({
                url: "/api/v2/user/translator/vote",
                type: "post",
                dataType: "json",
                contentType: false,
                processData: false,
                data: voteData
            });

            $(".popUp").css("display", "none");
            $(".popUp_checked").css("display", "block");
            $(".popUp_checked").text("선택하지 않았습니다.");
            $(this).parent().next().text("선택했습니다.");

        }
    });
    
});
