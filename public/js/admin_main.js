//의뢰물 및 번역 건수(메인)
function get_overview(data) { //번역물 총 개수
    $("#total_requests span.num").text(data.total_requests);
    $("#pendings span.num").text(data.pendings);
    $("#ongoings span.num").text(data.ongoings);
    $("#completes span.num").text(data.completes);
}
//급한 의뢰물(메인)
function get_aboutDeadline(data) { //급한 번역물 목록
    //개수 제한 변수
    var num = 0;
    //빠른 날짜 올림순서
    data.data.sort(function(a, b){
        var adate = new Date(a.deadline).getTime();
        var bdate = new Date(b.deadline).getTime();
        return adate-bdate;
    });
    $.each(data.data, function(index, item) {
        if(num >= 5) return false;
        $("#translationM table tbody").append(
            "<tr>" +
                "<td class='text_center'><span>" + item.id + "</span></td>" +
                "<td>" + item.theme + "</td>" +
                "<td>" + item.requester_name + "</td>" +
                "<td>" + item.translator_name + "</td>" +
                "<td class='text_center'>" + item.deadline + "</td>" +
            "</tr>"
        );
        num++;
    });
}