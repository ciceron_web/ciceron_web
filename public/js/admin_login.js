$(document).ready(function() {

    /*******************************************
    
        로그인

    ********************************************/
    $("body").on("click", "#loginB", function(e) {
        e.preventDefault();

        var admin_id = $("#my_id").val();
        var admin_pass = $("#my_pass").val();
        $.ajax({
            url: "/api/v2/login",
            type: "GET",
            dataType: "JSON",
            contentType: false,
            processData: false
        }).done(function(data) {
            identifier = data.identifier;
            admin_pass = $.sha256(identifier + $.sha256(admin_pass) + identifier);
            
            var formData = new FormData();
            formData.append("email", admin_id);
            formData.append("password", admin_pass);
            $.ajax({
                url: "/api/v2/login",
                type: "POST",
                dataType: "JSON",
                contentType: false,
                processData: false,
                data: formData
            }).done(function(data) {
                console.log(data);
                location.href = "/admin/main";
            }).fail(function(err) {
                console.log(err);
                alert("아이디 혹은 비밀번호를 확인해주세요.");
            });
        }).fail(function(err) {
            console.log(err);
        });
    });

});