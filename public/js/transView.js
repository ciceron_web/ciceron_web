/********************************************************
    
    번역기 2차 뷰에서 사용
        - 제작날짜 : 2017년 4월 26일 수요일 제작(4차 수정)
        -> 2017년 5월 8일 월요일 제작(5차 수정)
        -> 2017년 11월 1일 수정 시작

*********************************************************/

//로케이션 끝 값 받기
var location_info = location.pathname.split("/").pop();
var objThis;
//실행
$(document).ready(function () {
    var view = new transView();
    //기본 설정.
    view.init();
    //반응형 이벤트
    view.resizeEvent();
    //스크롤 이벤트
    view.scrollEvent();
    //언어 바꾸기
    view.langChange();
    //로딩화면
    view.loading();
    //박스 그림자 이벤트
    view.textShadow();
    //버튼 이벤트
    // view.transButton();
    //키보드 이벤트
    view.keyEvent();

    objThis = view;

    $("body").on("click", ".translate button", function () {
        view.transButton();
    });
    $(".translate button").click();

});


//클래스 인스턴스 객체
function transView() {
    this.name = null;
    this.langNum = null;
    this.langStr = null;
    this.langStrArr = null;
    this.screeWidth = $(window).innerWidth() + 15;;
    this.scroll = true;
    this.loadingStop = null;
    this.trans = false;
    this.formData = null;

    this.data_ciceron = '';
    this.data_bing = '';
    this.data_google = '';
    this.data_papago = '';
}

var random_sentences_ko = [
    "여야는 중소창업기업부 명칭과 관련, 국회 안전행정위에서 중소기업부나 중소벤처기업부 등의 방안도 같이 검토키로 했다. 또 해양경찰청의 경우 해양수산부가 아닌 행정안전부의 외청으로 하는 방안도 안전행정위에서 판단키로 했다.",

    "여섯 살 적에 나는 \“체험한 이야기\“라는 제목의, 원시림에 관한 책에서 기막힌 그림 하나를 본 적이 있다. 맹수를 집어삼키고 있는 보아 구렁이 그림이었다. 위의 그림은 그것을 옮겨 그린 것이다. 그 책에는 이렇게 씌어 있었다. \"보아 구렁이는 먹이를 씹지도 않고 통째로 집어삼킨다. 그리고는 꼼짝도 하지 못하고 여섯 달 동안 잠을 자면서 그것을 소화시킨다.\" 나는 그래서 밀림 속에서의 모험에 대해 한참 생각해 보고 난 끝에 색연필을 가지고 내 나름대로 내 생애 첫 번째 그림을 그려보았다. 나의 그림 제 1호였다. 그것은 이런 그림이었다.",

    // "아들 범조가 자신의 죄를 대신 뒤집어쓰고 자수하자 박회장은 범조를 위해 어쩔 수 없이 경찰에 출두하기로 한다. 하명과 인하는 박회장에게 할 질문들을 준비한다.",

    "그래서 여섯 해 전에 사하라 사막에서 비행기가 고장을 일으킬 때까지 나는 마음을 털어놓고 진정어린 이야기를 할 수 있는 상대를 갖지 못한 채 홀로 살아왔다. 내 비행기의 모터가 한 군데 부서져 버린 것이다. 기사도 승객도 없었으므로 나는 혼자서 어려운 수선을 시도해 보려는 채비를 갖추었다. 그것은 나에게는 죽느냐 사느냐의 문제였다. 이렛날 동안 마실 수 있는물밖에 남아 있지 않았다.",

    "그래서 나는 어른들이 알아볼 수 있도록 보아 구렁이의 속을 그렸다. 어른들은 언제나 설명을 해주어야만 한다. 나의 그림 제 2호는 이러했다. 어른들은 속이 보이거나 보이지 않거나 하는 보아 구렁이의 그림들은 집어치우고 차라리 지리, 역사, 계산, 그리고 문법 쪽에 관심을 가져보는 게 좋을 것이라고 충고해 주었다. 그래서 나는 여섯 살 적에 화가라는 멋진 직업을 포기해 버렸다. 내 그림 제 1호와 제 2호가 성공을 거두지 못한 데 낙심해 버렸던 것이다. 어른들은 언제나 스스로는 아무것도 이해하지 못한다. 자꾸자꾸 설명을 해주어야 하니 맥 빠지는 노릇이 아닐 수 없다.",

    // "다음주면 일이 너무 바빠질꺼 같은데. 심하게 걱정된다. 자유를 내놔라.",

    "첫째는 인류가 더 이상 지역이나 국경으로 나뉘어 있지 않고, 지구촌이라는 하나의 울타리 안에 함께 사는 운명 공동체가 됐다는 사실입니다. 유라시아 대륙의 서쪽 끝에서 발생한 식품 파동이 유라시아 대륙의 동쪽 끝에서 그대로 재현되고, 세계의 수의사 여러분이 해마다 한 자리에 모여 지혜를 나누시는 이유가 바로 이것입니다.",

    "인류의 건강을 위협하는 요소는 끊임없이 새롭게 생겨나지만 그것을 극복하려는 인류의 지혜와 관리체제 또한 끊임없이 발전합니다. 오늘 개막한 세계수의사 대회가 바로 그런 인류의 지혜와 관리체제의 발전을 더욱 촉진시키는 계기가 되기를 바랍니다."
];

var random_sentences_en = [
    "A great writer is, so to speak, a second government in his country. And for that reason no regime has ever loved great writers, only minor ones.",
    "Some of the parasites removed were as long as 27 centimeters (more than 10 inches), according to the South Korean doctors who treated him. One type of worm they discovered is typically found in dogs.",
    "If you haven’t already, I urge you to register as an organ donor and to share your wishes with loved ones. Talking about organ donation may feel morbid and unnecessary, but it mattered to my family. We were able to carry out Trey’s wishes with peace and confidence. I struggled with the decision to donate his eyes.",
    "By this time my patience was exhausted, because I was in a hurry to start taking my engine apart. So I tossed off this drawing. And I threw out an explanation with it. “This is only his box. The sheep you asked for is inside.”"
];

//처음 기본 설정
transView.prototype.init = function () {
    //언어 설정 값 받기
    this.langObjVal = $("select[name='lang_select'] option:selected").val();
    this.langStr = $("select[name='lang_select'] option:selected").text().trim();
    this.langNum = 0;
    //언어 설정 값을 배열로 나누기
    this.langStrArr = this.langStr.split(">>");
    //값 입력
    $("#original_lang").text(this.langStrArr[0]);
    $("#target_lang").text(this.langStrArr[1]);

    $("header a").eq(0).attr("href", "/" + location_info);
    //사이트구분
    // console.log(location);
    if (location_info == "ai1") {
        // $("#loading1").css("display", "block");
        // $("#loading1").delay(3000).slideUp();
        $(".mobile_ai_link").remove();

        if (this.langObjVal == 1) {
            var random_texts = transView.prototype.shuffle_evt(random_sentences_ko);
            $(".translation_box textarea").val(random_texts[0]);
        } else if (this.langObjVal == 0) {
            var random_texts = transView.prototype.shuffle_evt(random_sentences_en);
            $(".translation_box textarea").val(random_texts[0]);
        }

    } else if (location_info == "ai2") {
        //바로 셋션값 받아서 번역비교하기
        window.onload = function () {
            this.$getText = $("textarea");
            this.$text = sessionStorage.getItem("textValue");

            var previousLanguage = sessionStorage.getItem("languageValve");

            // console.log(this.$text);
            // console.log(previousLanguage);
            if ($text) {
                this.$getText.val(this.$text);
                $(document).ready(function () {
                    $(".translate button").trigger("click");
                });
            }
            if (previousLanguage) {
                $("select[name='lang_select'] option[value='" + previousLanguage + "']").attr("selected", "selected");
                $("select[name='lang_select']").change();
            }
            sessionStorage.removeItem("textValue");
            sessionStorage.removeItem("languageValve");
        }

        // console.log("ekhgd");
        if (this.langObjVal == 1) {
            var random_texts = transView.prototype.shuffle_evt(random_sentences_ko);
            $(".translation_box textarea").val(random_texts[0]);
        } else if (this.langObjVal == 0) {
            var random_texts = transView.prototype.shuffle_evt(random_sentences_en);
            $(".translation_box textarea").val(random_texts[0]);
        }

    }

}

//반응형 이벤트
transView.prototype.resizeEvent = function () {
    $(window).resize(function () {
        this.screenWidth = $(window).innerWidth() + 15;
        if (this.screenWidth <= 640) {
            $("#ad img").attr("src", "img/baogao_M.png");
        } else {
            $("#ad img").attr("src", "img/baogao_banner.png");
        }

        // if(location_info == "ai2") {
        //     if(this.screenWidth <= 760) {
        //         if($(window).scrollTop() >= 260 && $(window).scrollTop() <= 900) {
        //             $("#slogan").css({
        //                 "display": "block",
        //                 "position": "fixed",
        //                 "bottom": "0px",
        //                 "left": "0px"
        //             });
        //         } else {
        //             $("#slogan").css({
        //                 "position": "relative"
        //             });
        //         }
        //     } 
        // }
    });
}

//스크롤이벤트
transView.prototype.scrollEvent = function () {
    //  if(location_info == "ai2") {
    //     if(this.screenWidth <= 760) {
    //         if($(window).scrollTop() >= 260 && $(window).scrollTop() <= 900) {
    //             $("#slogan").css({
    //                 "display": "block",
    //                 "position": "fixed",
    //                 "bottom": "0px",
    //                 "left": "0px"
    //             });
    //         } else {
    //             $("#slogan").css({
    //                 "position": "relative"
    //             });
    //         }
    //     }
    // }
}

//언어바꾸기 이벤트
transView.prototype.langChange = function () {
    $("body").on("change", "select[name='lang_select']", function () {
        $("textarea").val("");
        $("div.get_text").text("");
        // $("select[name='lang_select'] option:selected").attr("selected", "selected");
        objThis.langStr = $("select[name='lang_select'] option:selected").text().trim();
        objThis.langStrArr = objThis.langStr.split(">>");
        $("#target_lang").text(objThis.langStrArr[1]);
        $("#original_lang").text(objThis.langStrArr[0]);
        // console.log($("#original_lang_list").text())
        // console.log(langChage[0])
        // console.log(typeof(langChage[0]))
        if (objThis.langStrArr[0].indexOf("한국어") > -1) {
            objThis.langNum = 0;

            var random_texts = transView.prototype.shuffle_evt(random_sentences_ko);
            $(".translation_box textarea").val(random_texts[0]);

        } else if (objThis.langStrArr[0].indexOf("영어") > -1) {
            objThis.langNum = 1;

            var random_texts = transView.prototype.shuffle_evt(random_sentences_en);
            $(".translation_box textarea").val(random_texts[0]);
        }

        $(".translate button").click();
    });
}

//로딩 이벤트
transView.prototype.loading = function () {
    var count = 0;
    var comma = "...";

    function loadingStart() {
        count++;
        // comma += ".";
        // if(count == 4) {
        //     count = 0;
        //     comma = "";
        // }
        // $("#loadingText span").text(comma);
    }
    this.loadingStop = setInterval(loadingStart, 600);
}

//텍스트 그림자 이벤트
transView.prototype.textShadow = function () {
    //포커스를 받았을 경우
    var first_time = true;
    $("body").on("focus", "textarea", function (e) {

        e.preventDefault();
        if (first_time == true) {
            $(this).val("");
        } else {
            $(this).get(0).setSelectionRange(0, 9999);
        }

        first_time = false;
        // $(this).val("");
    });
    //포커스를 벗어났을 경우
    $("body").on("blur", "textarea", function () {

    });
}

// 셔플 이벤트
transView.prototype.shuffle_evt = function (o) {

    // var Shuffle = function (o) {
    for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);

    return o;
    // }
}

//텍스트 그림자 이벤트
transView.prototype.transButton = function () {

    $(".translation_box a").removeClass("other_translation");

    if ($(".container div textarea").val()) {
        $("#loading2").css("display", "block");
        // $(".cssload-wrap").css("display", "block");
        if (location_info == "ai2") {
            $(".popUp").css("display", "inline-block");
            $(".popUp_checked").css("display", "none");
            $(".popUp button").prop("disabled", false).removeClass("selected");

        } else {
            setTimeout(() => {}, 6000);
        }
        objThis.transData = new FormData();

        if (objThis.langNum == 0) {
            objThis.transData.append("source_lang_id", 1);
            objThis.transData.append("target_lang_id", 2);
        } else if (objThis.langNum == 1) {
            // alert('영-한 번역기는 인프라 관계로 시스템 관리자에게 엔진 가동 문의 후 진행하시기 바랍니다!');
            objThis.transData.append("source_lang_id", 2);
            objThis.transData.append("target_lang_id", 1);
        }
        //objThis.transData.append("source_lang_id", 1);
        //objThis.transData.append("target_lang_id", 2);

        // console.log(objThis.transData);

        objThis.transData.append("user_email", "admin@sexycookie.com");
        // objThis.transData.append("sentence", "no one can't live again.");
        objThis.transData.append("sentence", $(".container div textarea").val());
        objThis.transData.append("where", "phone");
        $.ajax({
            url: "https://translator.ciceron.me/translate",
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: objThis.transData
        }).done(function (data) {

            objThis.data_ciceron = data.ciceron;
            objThis.data_bing = data.bing;
            objThis.data_google = data.google;
            objThis.data_papago = data.papago;

            var translation_Boxs = ["<div class='translation_box'>" +
                "<div class='get_text_wrap'>" +
                "<div class='get_text_cover'>" +
                "<div class='get_text'>" +
                objThis.data_bing +
                "</div>" +
                "</div>" +
                "</div>" +
                "<div class='popUp'>" +
                "<span class='popUp_checked class_A' style='display:none;'>" +
                "<h2>A사</h2>" +
                "</span>" +
                "<button>선택</button>" +
                "</div>" +
                "</div>",

                "<div class='translation_box'>" +
                "<div class='get_text_wrap'>" +
                "<div class='get_text_cover'>" +
                "<div class='get_text'>" +
                objThis.data_google +
                "</div>" +
                "</div>" +
                "</div>" +
                "<div class='popUp'>" +
                "<span class='popUp_checked class_B' style='display:none;'>" +
                "<h2>B사</h2>" +
                "</span>" +
                "<button>선택</button>" +
                "</div>" +
                "</div>",
                "<div class='translation_box'>" +
                "<div class='get_text_wrap'>" +
                "<div class='get_text_cover'>" +
                "<div class='get_text'>" +
                objThis.data_ciceron +
                "</div>" +
                "</div>" +
                "</div>" +
                "<div class='popUp'>" +
                "<span class='popUp_checked class_C' style='display:none;'>" +
                "<img src='img/translation_choice.png'><br>" +
                "<span>3사 중 씨세론 번역을 택하셨군요!</span>" +
                "</span>" +
                "<button>선택</button>" +
                "</div>" +
                "</div>"
            ];

            var data_arr = new Array();
            data_arr.push(objThis.data_google);
            data_arr.push(objThis.data_ciceron);
            data_arr.push(objThis.data_bing);

            // 번역 결과 셔플
            // Shuffle(translation_Boxs);
            transView.prototype.shuffle_evt(translation_Boxs);

            if (location_info == "ai1") { //1차 번역기일 경우 
                $("div.get_text").text(objThis.data_ciceron);
                save();

            } else if (location_info == "ai2") { //2차 번역기일 경우 
                objThis.trans = true;
                /*$("div.get_text").each(function(index, item){
                    $(this).text(data_arr[index]);
                });   */

                $(".translation_table").empty();
                for (var i = 0; i < translation_Boxs.length; i++) {
                    $(".translation_table").append(translation_Boxs[i]);
                }

                clearInterval(objThis.loadingStop);
                $(".container a").css("display", "block");
                // $(".popUp button").text("선택하기");


                //넓이가 779 아래일 경우 스크롤이벤트 발생.
                if ($("body").width() <= 779) {
                    var offset = $(".get_translations").offset();
                    $("html, body").animate({
                        scrollTop: (offset.top - 10)
                    }, 600);
                }
            }
            $("#loading2").css("display", "none");
            // $(".cssload-wrap").css("display", "none");

            // 번역 결과 저장
            var resultData = new FormData();
            if (objThis.langNum == 0) {
                resultData.append("source_lang", 'ko');
                resultData.append("target_lang", 'en');
            } else if (objThis.langNum == 1) {
                resultData.append("source_lang", 'en');
                resultData.append("target_lang", 'ko');
            }

            resultData.append("original_text", $(".container div textarea").val());
            resultData.append("google_result", objThis.data_google);
            resultData.append("bing_result", objThis.data_bing);
            resultData.append("ciceron_result", objThis.data_ciceron);
            resultData.append("papago_result", objThis.data_papago);

            $.ajax({
                url: "/api/v2/user/translator/store",
                type: "post",
                dataType: "json",
                contentType: false,
                processData: false,
                data: resultData
            }).done(function (data) {
                objThis.translation_store_id = data.new_id;

                // console.log(objThis.translation_store_id);
            });
            $(".translation_box a").addClass("other_translation");

        }).fail(function (err) {
            console.log(err);
            $("#loadingText").text("서버에러");
        });

        $(".get_translations a").css("display", "block");
        // 번역할 내용 세션에 저장.
        function save() {
            var $text = $("textarea");
            var $text_value = $text.val();
            sessionStorage.setItem("languageValve", $("select[name='lang_select'] option:selected").val())
            sessionStorage.setItem("textValue", $text_value);
        }
    } else {

        $(".get_text").text("");
        $(".popUp button").prop("disabled", true);
        setTimeout(() => {
            alert("번역할 내용을 입력해주세요.");
        }, 10);
    }

    //번역물 선택 이벤트
    $("body").on("click", ".popUp button", function () {
        if (objThis.trans == true) {
            var selection = '';
            /*if ($(this).parent().prev().hasClass('get_left')){
                selection = "papago";*/
            if ($(this).parent().prev().hasClass('get_left')) {
                selection = "google";
            } else if ($(this).parent().prev().hasClass('get_center')) {
                selection = "ciceron";
            } else if ($(this).parent().prev().hasClass('get_right')) {
                selection = "bing";
            }

            var voteData = new FormData();
            voteData.append('result_id', objThis.translation_store_id);
            voteData.append('versus', 'bing');
            voteData.append('vote_to', selection);

            $.ajax({
                url: "/api/v2/user/translator/vote",
                type: "post",
                dataType: "json",
                contentType: false,
                processData: false,
                data: voteData
            });

            $(this).addClass("selected");
            // $(this).siblings(".popUp_checked").css("display","inline-block");
            $(".popUp_checked").css("display", "inline-block");
            $(".popUp_checked").find("span").css("display", "none");
            $(this).siblings(".popUp_checked").find("span").css("display", "inline-block");
        }
        objThis.trans = false;
    });
}

//키보드 이벤트
transView.prototype.keyEvent = function () {
    //글자수 제한
    $("body").on("keyup", "textarea", function () {
        var content_count = $(this).val();
        $('.count').text(content_count.length + '/1000');
    });
    $(".translation_box textarea").keyup();
}
