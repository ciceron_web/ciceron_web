$(document).ready(function () {

	/*******************************************
    
		데이터 카운트 

    ********************************************/
	$.ajax({
		url: "/api/v2/admin/dataManager/dataCounter",
		type: "get",
		dataType: "json",
		contentType: false,
		processData: false
	}).done(function (data) {
		console.log("성공");
		// $(".Doc_Maj").hide();
		$("#data_rst").empty();
		$('#data_rst').append("<p>위 조건에 해당하는 데이터는 <span>" + data.number + "</span> 건 입니다.</p>");
	}).fail(function (err) {
		console.log(err);
	});

	/*******************************************
    
        change  이벤트 (.kind_box text)

    ********************************************/

	$("body").on("change", "#lang1", function () {
		// $(".Doc_Maj").show();
		var original_text = $("#lang1 option:selected").text();
		var original_val = $("#lang1 option:selected").val(); 
		if (original_val != "") {
			$('.kind_box:nth-child(1) span').text(original_text);
		} else {
			$('.kind_box:nth-child(1) span').html("  <span>&nbsp;&nbsp;</span>");			
		}

	});

	$("body").on("change", "#lang2", function () {
		var target_text = $('#lang2 option:selected').text();
		var target_val = $("#lang2 option:selected").val(); 
		if (target_val != "") {
			$('.kind_box:nth-child(2) span').text(target_text);
		} else {
			$('.kind_box:nth-child(2) span').html("  <span>&nbsp;&nbsp;</span>");			
		}
	});

	$("body").on("change", "#kind_Maj", function () {
		var subject_text = $("#kind_Maj option:selected").text();
		var subject_val = $("#kind_Maj option:selected").val(); 
		if (subject_val != "") {
			$('.kind_box:nth-child(3) span').text(subject_text);
		} else {
			$('.kind_box:nth-child(3) span').html("  <span>&nbsp;&nbsp;</span>");			
		}
	});

	$("body").on("change", "#kind_Doc", function () {
		var format_text = $('#kind_Doc option:selected').text();
		var format_val = $("#kind_Doc option:selected").val(); 
		if (format_val != "") {
			$('.kind_box:nth-child(4) span').text(format_text);
		} else {
			$('.kind_box:nth-child(4) span').html("  <span>&nbsp;&nbsp;</span>");			
		}
	});

	$("body").on("change", "#kind_tone", function () {
		var tone_text = $('#kind_tone option:selected').text();
		var tone_val = $("#kind_tone option:selected").val(); 
		if (tone_val != "") {
			$('.kind_box:nth-child(5) span').text(tone_text);
		} else {
			$('.kind_box:nth-child(5) span').html("  <span>&nbsp;&nbsp;</span>");			
		}
	});

	$("body").on("click", "#btnSelect", function () {
		var client = $('#cName').val();
		$('.kind_box:nth-child(6) span').text(client);
	});

	/*******************************************
    
        change 이벤트 (ajax)

    ********************************************/

	$("body").on("change", "form", function () {

		var original_language_id = $("#lang1 option:selected").val();
		var target_language_id = $('#lang2 option:selected').val();
		var subject_id = $('#kind_Maj option:selected').val();
		var format_id = $('#kind_Doc option:selected').val();
		var tone_id = $('#kind_tone option:selected').val();

		/*var urlData = "original_language_id="+original_language_id+"&target_language_id="+target_language_id+"&subject_id="+subject_id+"&format_id="+format_id+"&tone_id="+tone_id;*/

		var urlData = ''
		if (original_language_id != '') {
			urlData += 'original_language_id=' + original_language_id + '&';
		}

		if (target_language_id != '') {
			urlData += 'target_language_id=' + target_language_id + '&';
		}

		if (subject_id != '') {
			urlData += 'subject_id=' + subject_id + '&';
		}

		if (format_id != '') {
			urlData += 'format_id=' + format_id + '&';
		}

		if (tone_id != '') {
			urlData += 'tone_id=' + tone_id + '&';
		}

		urlData.slice(0, -1);

		var a = urlData.slice(-1);

		if (urlData.slice(-1) == "&") {
			urlData = urlData.substring(0, urlData.length - 1);
		}

		$.ajax({
			url: "/api/v2/admin/dataManager/dataCounter",
			type: "get",
			dataType: "json",
			data: urlData,
			contentType: false,
			processData: false
		}).done(function (data) {
			$('#data_rst').empty();
			
			if (data.number == 0) {

				$('#data_rst').append("<p>위 조건에 해당하는 데이터는 <span>" + data.number + "</span> 건 입니다.</p>");

				setTimeout(function() {					
					alert("데이터가 없습니다.");
				}, 10);

			} else {
				$('#data_rst').append("<p>위 조건에 해당하는 데이터는 <span>" + data.number + "</span> 건 입니다.</p>");
			}

		}).fail(function (err) {
			console.log(err);
		});
	});

	// scv파일 변환
	$("body").on("click", "#btnExport", function (e) {
		var original_language_id = $("#lang1 option:selected").val();
		var target_language_id = $('#lang2 option:selected').val();
		var subject_id = $('#kind_Maj option:selected').val();
		var format_id = $('#kind_Doc option:selected').val();
		var tone_id = $('#kind_tone option:selected').val();

		var urlData = ''

		if (original_language_id != '') {
			urlData += 'original_language_id=' + original_language_id + '&';
		}

		if (target_language_id != '') {
			urlData += 'target_language_id=' + target_language_id + '&';
		}

		if (subject_id != '') {
			urlData += 'subject_id=' + subject_id + '&';
		}

		if (format_id != '') {
			urlData += 'format_id=' + format_id + '&';
		}

		if (tone_id != '') {
			urlData += 'tone_id=' + tone_id + '&';
		}

		urlData.slice(0, -1);

		if (urlData.slice(-1) == "&") {
			urlData = urlData.substring(0, urlData.length - 1);
		}

		$.ajax({
			url: "/api/v2/admin/dataManager/export",
			type: "get",
			dataType: "text",
			data: urlData,
			contentType: "text/csv; charset=utf-8;",
			processData: false,
		}).done(function (data) {
			if (data == "") {
				alert("다운받을 데이터가 없습니다.")
			} else {
				var d = new Date().toISOString().slice(0, 10).replace(/-/g, "");
				var fileName = "export_" + d + ".csv";
				var a = document.createElement("a");
				var uriEncodeData = "data:text/csv;charset=utf-8,\uFEFF" + encodeURI(data);

				a.href = uriEncodeData;
				a.download = fileName;
				a.click();

				e.preventDefault();
			}


		}).fail(function (err) {
			console.log(err);
		});

	});
});
