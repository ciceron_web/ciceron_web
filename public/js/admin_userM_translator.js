//번역가정보
function get_userM_translator(data) {
    $.each(data.data, function(index, item) {
        var language = "";
        var is_working = "-";
        // var lang_num = item.translatableLang.length;
        var langArr = []    
        if(item.is_working) is_working = "진행중";
        $.each(item.translatableLang, function(index2, item2) {
            langArr.push(lang[item2])
            // if(lang_num <= 1) language = lang[item2];
            // else {
            //     if(lang_num > (index2+1)) language += lang[item2] + ", ";
            //     else language += lang[item2];
            // }
        });
        var language = langArr.join(",");
        $(".table_board tbody").append(
            "<tr class='cursor'>" +
                "<td value='" + item.id + "' class='text_center'><span>" + item.id + "</span></td>" +
                "<td class='info'>" + item.name + "</td>" +
                "<td>" + item.email + "</td>" +
                "<td class='translator' title='" + language + "'>" + language + "</td>" +
                // "<td>" + item.keywords + "</td>" +
                "<td>-</td>" +
                "<td class='text_right'>" + item.numoftranslationcompleted + "</td>" +
                // "<td>" + item.recent_work_timestamp + "</td>" +
                "<td class='text_center'>-</td>" +
                "<td class='text_center'>" + is_working + "</td>" +
            "</tr>" +
            "<tr class='details'></tr>"
        );
    });

    var totalCnt;
    if((data.data.length/20)<0){
        totalCnt = 1;
    } else {
        totalCnt = Math.ceil(data.data.length/20);
    }

    pageNumber("table", 20, ".cursor", totalCnt);
    $("tr.details").hide();
}