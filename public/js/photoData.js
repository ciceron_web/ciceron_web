$(document).ready(function () {

    /*******************************************
     
        tagData

    ********************************************/
    // tag Data에서 분류된 태그의 이미지를 받음.

    // scroll 할 때마다 다음 페이지 태그 불러오기
    var $tagV = $("#tagView");
    var page = 1;
    $("#loading").hide();
    loadTag(page);

    $tagV.scroll(function () {
        if ($(this).scrollTop() >= $(this).prop("scrollHeight") - $(this).height() - 100) {
            loadTag(page);
        }
        else if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            return false;
        }
    });

    var isLoading = false;

    function loadTag(pageNumber) {
        if (!isLoading) {
            $("#loading").show();
            isLoading = true;
            $.ajax({
                url: "/api/v2/admin/kangaroo/tag/status/1",
                type: "get",
                dataType: "json",
                contentType: false,
                processData: false,
                data: "page=" + page + "&itemPerPage=8"
            }).done(function (data) {
                // $("#loading").delay(5000).hide();
                $.each(data.data, function (index, item) {
                    // console.log(item);
                    $("#tagView ul").append("<li value='" + item.id + "'>" + item.name + "</li>");
                });

                // 선택된 태그가 없을 때 첫번째 태그 클릭
                if ($(".selected").length <= 0) {
                    $("#tagView ul li").first().click();
                }
                if (data.data.length > 0) {
                    isLoading = false;

                }
                $("#loading").hide();
                page++;

            }).fail(function (err) {
                console.log(err);
                isLoading = false;
                $("#loading").hide();

            });
        }
        return false;
    }

    /*******************************************
     
        tagView + 이미지 리스팅

    ********************************************/
    var imgPage = 1;
    var totalPage = -1;
    var itemPerPage = 6;

    $("body").on("click", "#tagView ul li", function (e) {
        e.preventDefault();

        $("input:checkbox[id='allCheck']").prop("checked", false);
        $("#tagView ul li").removeClass("selected");
        $(this).addClass("selected");
        $(".tagName").text($(this).text());
        $("#photo_view #photoName").remove();
        $("#photo_view .photoSort").remove();

        var tag_id = $(this).attr("value");

        imgPage = 1;
        // page event
        $(".pager").hide();
        loadImg(imgPage, tag_id);


        // img 로딩
    });

    $("body").on("click", ".prev", function () {
        var tag_id = $("#tagView ul li.selected").attr("value");
        if (imgPage > 1) {
            imgPage--;
            loadImg(imgPage, tag_id);
        }

    });

    $("body").on("click", ".next", function () {
        var tag_id = $("#tagView ul li.selected").attr("value");
        if (totalPage > imgPage) {
            imgPage++;
            loadImg(imgPage, tag_id);
        }
    });

    var loadImg = function (pageNum, tag_id) {
        $("#tagImg").empty();
        var selBox = "<div class='selBox'><div class='cropImg'>Crop</div><div class='stayImg'>Add</div><div class='delImg'>Delete</div></div>"

        $.ajax({
            url: "/api/v2/admin/kangaroo/tag/" + tag_id,
            beforeSend: function () { $("#page_loading").show(); },
            complete: function () { $("#page_loading").hide() },
            type: "get",
            dataType: "json",
            contentType: false,
            processData: false,
            data: "page=" + imgPage + "&itemPerPage=" + itemPerPage
        }).done(function (data) {
            // $("#tagImg").empty();

            totalPage = Math.ceil(data.total / itemPerPage);

            if (pageNum <= 1) {
                $(".prev").addClass("disabled");
            }
            else {
                $(".prev").removeClass("disabled");
            }
            if (pageNum >= totalPage) {
                $(".next").addClass("disabled");
            }
            else {
                $(".next").removeClass("disabled");
            }

            $(".page-number").text(imgPage);
            $(".pageSum").text("/ " + totalPage);
            if (data.total == 0 && $("#tagImg li").length == 0) {
                $(".pager").hide();
                alert("이미지가 없습니다.");
            } else {
                $(".pager").show();
                $("#photo_view").prepend("<div id='photoName'>" + $(this).text() + "</div>");
                $("#photoName").attr("value", $(this).attr("value"));
                $.each(data.data, function (index, item) {
                    $("#photo_view #tagImg").append(
                        "<li class='photoSort' value=" + item.id + ">" + "<input type='checkbox' class='imgCheck' name='box'>" +
                        "<img value='" + item.id + "' src='" + item.image_url + "' alt='이미지" + item.id + "' />" +
                        selBox +
                        "</li>"
                    );
                });
            }
        }).fail(function (err) {
            console.log(err);
        });
    }

    // keydown 이벤트
    $("body").on("keydown", function (e) {

        if (e.keyCode == 37) {
            $(".prev").click();
        }
        if (e.keyCode == 39) {
            $(".next").click();
        }

    });
    /* paging 화살표 이벤트 끝 */

    /*******************************************
     
        전체이미지 checkbox 제어
    
    ********************************************/

    $("body").on("click", "#allCheck", function (event) {

        if (this.checked) {
            $("input:checkbox[class='imgCheck']").each(function () {
                this.checked = true;
            });
        } else {
            $("input:checkbox[class='imgCheck']").each(function () {
                this.checked = false;
            });
        }
    });

    /*******************************************
     
        각 이미지 checkbox 제어
    
    ********************************************/

    $("body").on("click", ".photoSort img", function (e) {

        var floatCheck = $(this).siblings("input:checkbox[class='imgCheck']");
        if (floatCheck.prop("checked") == false) {
            floatCheck.prop("checked", true);
        } else if (floatCheck.prop("checked") == true) {
            floatCheck.prop("checked", false);
        }

    });


    /*******************************************
     
        tag 별 이미지 stay
    
    ********************************************/
    // API 없음.

    // 선택 이미지 남김
    $("body").on("click", "#stayImg", function () {

        $("input:checkbox[id='allCheck']").prop("checked", false);
        var checkedImg = $("input:checkbox[name='box']:checked");

        $.each(checkedImg, function (index, item) {

            var stayVal = new FormData();
            stayVal.append("", "")

            $.ajax({
                url: " ",
                type: "post",
                dataType: "json",
                data: stayVal,
                contentType: false,
                processData: false
            }).done(function (data) {
                console.log("정보 전송");
                $(".selected").remove();
                // $("#tagView ul li").first().click();
            }).fail(function (err) {
                console.log(err);
            });
        })
    });

    // 이미지 1개 남김
    $("body").on("click", ".stayImg", function () {

        var imgsrc = $(this).parents(".photoSort").find("img").attr("src");
        var imgTag = $(this).parents("#photo_contents").find("li.selected").attr("value");

        var stayVal = new FormData();

        $.ajax({
            url: " ",
            type: "post",
            dataType: "json",
            data: img_id,
            contentType: false,
            processData: false
        }).done(function (data) {
            console.log("정보 전송");
            // $("#tagView ul li").first().click();
        }).fail(function (err) {
            console.log(err);
        });
    });

    /*******************************************
     
        tag 별 이미지 삭제
    
    ********************************************/
    // 선택 이미지 삭제
    $("body").on("click", "#delImg", function (e) {
        e.preventDefault();
        $("input:checkbox[id='allCheck']").prop("checked", false);
        var DelCheck = $("input:checkbox[name=box]:checked");

        $.each(DelCheck, function (index, item) {

            var imgurl = $("input:checkbox[name='box']:checked").siblings("img").attr("src");

            $.ajax({

                url: imgurl,
                type: "delete",
                dataType: "json",
                contentType: false,
                processData: false
            }).done(function (data) {
                console.log("삭제완료");
                DelCheck.parents('.photoSort').remove();
                history.go(1);
            }).fail(function (err) {
                console.log(err);
            });
        });
    });

    // 이미지 1개 삭제
    $("body").on("click", ".delImg", function (e) {
        e.preventDefault();

        var imgurl = $(this).parents(".photoSort").find("img").attr("src");

        $.ajax({

            url: imgurl,
            type: "delete",
            dataType: "json",
            contentType: false,
            processData: false
        }).done(function (data) {
            console.log("삭제완료");
            $(this).parents('.photoSort').remove();
            history.go(1);
        }).fail(function (err) {
            console.log(err);
        });
    });


    /*******************************************
     
        Crop 팝업창 불러오기 + Crop 데이터 전송
    
    ********************************************/
    $("body").on("click", ".cropImg", function (e) {
        e.preventDefault();

        var childImg = $(this).parents('.selBox').siblings('img').attr('src');
        var sameImg = $(this).parents('.photoSort');

        $("body").css("overflow", "hidden");
        // $("#pop_crop").css("display", "block");
        $(".popup").addClass("visible");
        $('#crop_pic img').attr("src", childImg);
        $('#crop_pic img').cropper('destroy');
        $('#crop_pic img').cropper({
            minContainerHeight: 1,
            zoomable: false,
            movable: false,
            autoCropArea: 0.9,
            crop: function (e) {
                // console.log(e);
                if (e.x < 0) e.x = 0;
                if (e.y < 0) e.y = 0;
                // if(e.width)
                $('#cropBtn button').off('click').on('click', function () {
                    $(this).parents('#cropBtn').siblings('#crop_pic').children('img').cropper('destroy');

                    var Cropdata = new FormData();
                    Cropdata.append("x", e.x);
                    Cropdata.append("y", e.y);
                    Cropdata.append("w", e.width);
                    Cropdata.append("h", e.height);

                    // console.log(childImg);

                    $.ajax({
                        url: childImg,
                        type: "put",
                        dataType: "json",
                        data: Cropdata,
                        contentType: false,
                        processData: false
                    }).done(function (data) {
                        alert('데이터 전송 완료');
                        // $(this).parents("#pop_crop").css("display","none");
                        $("#pop_crop").css("display", "none");
                        $("body").css("overflow", "visible");
                        // $("#tagImg").load();
                        sameImg.find('img').attr('src', $(sameImg.find('img')[0]).attr('src'));
                    }).fail(function (err) {
                        console.log(err);
                        alert("에러!")
                    });

                    return false; // 새로고침 방지
                });

            }
        });

    });

    // crop창 close
    $("body").on("keydown", function (e) {
        if (e.keyCode == 27) {
            $("#crop1 #close").click();
        }
    });
    $("body").on("click", "#crop1 #close", function () {
        // $("#pop_crop").css("display", "none");
        $(".popup").removeClass("visible");
        $("body").css("overflow", "visible");
    });

    $("body").on("click", ".popup", function(e){
        var $container = $(".popup_box");

        if($(this).hasClass("visible") && !$container.is(e.target) && $container.has(e.target).length===0){
            $(this).removeClass("visible");
        }
    });

});