$(document).ready(function(){
	$('body').on('click', '.popup', function(e){
		if($(e.target).hasClass('popup')){
			$(this).remove();
		}
	});

	$('body').on('click', '#header-signin', function(e){
		location.href = secure_url + '/app/signin';
	});
});

var is_popup_clicked = false;
var timer_popup;
function popup(filename){

	if(is_popup_clicked) return;
	var p = $('<div class="popup" id="popup_'+filename+'"></div>');

	p.load('/div/' + filename, function(){
		$('body').prepend(p);

	});
	is_popup_clicked = true;

	timer_popup = setTimeout(function() {
		is_popup_clicked = false;
		clearTimeout(timer_popup);	
	}, 1000);
}
