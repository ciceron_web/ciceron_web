//paging event
function pageNumber(set, limit_num, tagClass, amount_num) {
    /* paging js 시작 */
    $(set).each(function () {
        var currentPage = 0;
        var numPerPage = limit_num;
        var $wrapper = $(this);
        var repaginate = function () {
            $("table tbody").find(tagClass).hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            // $wrapper.find(tagClass).hide();
            //기본 세팅. 모두 숨김.
            //현재 페이지+1 * 현재페이지까지만 보여줌.
        };
        
        // currentPage로 초기값은 0으로. numPerPage는 기본값 6을 주고 계산. slice()메소드를 이용해서 필요한 만큼 보여줌.

        //length로 전체 길이 구함.
        var imgNum;
        if (!amount_num) {
            imgNum = $(tagClass).length;
        } else {
            imgNum = amount_num;
        }
        // var imgNum = $(tagClass).length;
        // var imgNum = amount_num;
        // Math.ceil을 이용해 반올림한다.

        // var numPages = Math.ceil(imgNum / numPerPage);
        // numPages = (Math.ceil(imgNum/numPerPage)>0)?numPages:1;

        // div.pager 생성 변수
        var $pager = $("<div class='pager'></div>");

        // for 구문을 이용하여 페이지 수만큼 버튼 구현;
        // for (var page = 0; page < numPages; page++) {
        for (var page = 0; page < imgNum; page++) {
            $("<span class='page-number'></span>").text(page + 1).bind('click', { newPage: page }, function (e) {
                currentPage = e.data["newPage"];
                // repaginate();

                //활성화 페이지에 active 클래스 명 붙이기.
                $(this).addClass("active").siblings().removeClass("active");
                $(".active").siblings(".page-number").hide();
            }).appendTo($pager);

            // $(".prev").removeClass("disabled");
            // $(".next").removeClass("disabled");
        }
        
        // 화살표 추가
        $pager.prepend("<span class='prev disabled'><i class='fa fa-angle-double-left' aria-hidden='true'></i></span>");
        $pager.append("<span class='page-number'>1</span>");
        $pager.append("<span class='next'><i class='fa fa-angle-double-right' aria-hidden='true'></i></span>");
        $pager.append("<span class='pageSum'>/ " + imgNum + "</span>");

        // 설정한 페이지키 삽입
        $pager.insertAfter($wrapper).find("span.page-number:first").addClass("active");

        $(".active").click();
        $wrapper.trigger("repaginate");

        if (imgNum === 1) {
            $(".prev").addClass("disabled");
            $(".next").addClass("disabled");
        }
    });
    /* paging 화살표 이벤트 시작 */

    //.prev 이벤트
    var chk_num = 0;
    $("body").on("click", ".prev", function (e) {
        e.preventDefault();
        $("tr.details").hide();
        if ($(".active").text() == 1) {
            return false;
        } else {
            $(this).removeClass("disabled");
            $(".active").prev().click();
            $(".active").show();
            return false;
        }
    });

    //.next 이벤트
    $("body").on("click", ".next", function (e) {
        e.preventDefault();
        $("tr.details").hide();
        if (parseInt($(".active").text())+1 == $(".page-number").length) {
            $(this).addClass("disabled");
            return false;
        } else {
            $(this).removeClass("disabled");
            $(".active").next().click();
            $(".active").show();
            return false;
        }
    });

    // .active 이벤트
    $("body").on("click", ".page-number.active", function (e) {
        e.preventDefault();
        $("tr.details").hide();

        $(".prev").removeClass("disabled");
        $(".next").removeClass("disabled");

        if (parseInt($(this).text()) == 1) {
            $(".first").addClass("disabled");
            $(".prev").addClass("disabled");
        }

        if (parseInt($(this).text())+1 >= ($(".page-number").length)) {
            $(".next").addClass("disabled");
            $(".last").addClass("disabled");
        }

    });

    // keydown 이벤트
    $("body").on("keydown", function (e) {

        if (e.keyCode == 37) {
            $(".prev").click();
        }
        if (e.keyCode == 39) {
            $(".next").click();
        }

    });
    /* paging 화살표 이벤트 끝 */
}