var getData = function () {
	//후술
};
$(document).ready(function () {
	///api/v2/user/pretranslated/project/7/resource

	// var resHtml = $(".resourceList").html();
	// $(".resourceList").empty();
	// $.ajax({
	// 	url: '/api/v2/user/pretranslated/project/' + project_id + '/resource',
	// 	processData: false,
	// 	contentType: false,
	// 	dataType: "json",
	// 	cache: false,
	// 	type: 'GET'
	// }).done(function (data) {
	// 	var html = data.data.map(data => eval('`' + resHtml + '`')).join('');
	// 	$(".resourceList").html(html);
	// }).fail(function (xhr, ajaxOptions, thrownError) {
	// 	console.log('error');
	// });


	var resHtml = $(".projectInformation").html();
	$(".projectInformation").empty();
	var fileHtml = $(".fileList").html();
	$(".fileList").empty();

	getData = function () {
		loading(true);
		$.ajax({
			url: '/api/v2/user/pretranslated/project/' + project_id + '/resource',
			processData: false,
			contentType: false,
			dataType: "json",
			cache: false,
			type: 'GET'
		}).done(function (data) {
			data = data.data.filter(data => data.id == resource_id)[0];
			var html = eval('`' + resHtml + '`');
			var html2 = data.resource_info.map(data => eval('`' + fileHtml + '`')).join('');
			// var html = data.data.filter(data => data.id == resource_id ).map(data => eval('`' + resHtml + '`')).join('');
			$(".projectInformation").html(html);
			$(".fileList").html(html2);

			const getFileType = file => ['png', 'jpg', 'jpeg', 'gif', 'bmp'].indexOf(file.file_url.split(".").pop().toLowerCase()) > -1 ? 'image' : 'html';
			const renderImage = function (file) {
				$(".fileList #file" + file.id).append(`<img src="${file.file_url}">`);
			};
			const renderHtml = function (file) {
				$.ajax({
					url: file.file_url,
					processData: false,
					contentType: false,
					dataType: "html",
					cache: false,
					type: 'GET'
				}).done(function (data) {
					// data = data.indexOf("<") > -1 ? data : data.split("\n").join("<br />");
					$(`.fileList #file${file.id}`).append("<textarea>" + data + "</textarea><button type='submit'>텍스트 수정완료</button>");
				}).fail(function (xhr, ajaxOptions, thrownError) {
					console.log(`file${file.id} error`);
				});
			};

			data.resource_info.map(function (file) {
				var val = getFileType(file) == 'image' ?
					renderImage(file) :
					renderHtml(file);
			});

		}).fail(function (xhr, ajaxOptions, thrownError) {
			console.log('error');
		}).always(function () {
			loading(false);
		});
	}

	getData();





	$("body").on("click", "#deleteResource", function () {
		///api/v2/admin/pretranslated/project/(int: project_id)/resource/(int: resource_id)
		if (confirm("정말 삭제하겠습니까?")) {
			var url = "/api/v2/admin/pretranslated/project/" + project_id + "/resource/" + resource_id;
			var method = "delete";
			loading(true);
			$.ajax({
				url: url,
				processData: false,
				contentType: false,
				dataType: "html",
				cache: false,
				type: method
			}).done(function (data) {
				console.log(data);
				history.back();
			}).always(function () {
				loading(false);
			});
		}
	});


	$("body").on("click", ".fileList-item", function () {
		$(".fileList-item").removeClass("selected");
		$(this).addClass("selected");

	});

	$("body").on("change", "#addFile", function (e) {
		e.preventDefault();
		uploadFile(this.files[0]);
	});

	$("body").on("click", "#addText", function (e) {
		e.preventDefault();
		uploadFile("");
	});

	$("body").on("submit", ".fileList-item", function (e) {
		e.preventDefault();
		var file_id = $(this).attr('id').split('file').pop();
		var text = $(this).find('textarea').val();
		uploadFile(text, file_id);
	});

	$("body").on("submit", "#editResource", function (e) {
		e.preventDefault();
		//PUT /api/v2/admin/pretranslated/project/(int: project_id)/resource/(int: resource_id)
		var formData = new FormData($(this)[0]);
		loading(true);
		$.ajax({
			url: "/api/v2/admin/pretranslated/project/" + project_id + "/resource/" + resource_id,
			processData: false,
			contentType: false,
			dataType: "html",
			cache: false,
			type: "PUT",
			data: formData
		}).done(function (data) {
			console.log(data);
			loading(false);
			getData();
		}).fail(function(){
			loading(false);
		});
	})



});


var uploadFile = function (file, index) {
	file = (typeof file === 'string') ? new File([file], "upload.txt") : file;
	var formData = new FormData();
	var formDataName = index ? 'file' : 'file_list[]';

	formData.append(formDataName, file, file.name);
	formData.append('preview_permission', '0');

	var url = index ?
		'/api/v2/admin/pretranslated/project/' + project_id + '/resource/' + resource_id + '/file/' + index :
		'/api/v2/admin/pretranslated/project/' + project_id + '/resource/' + resource_id + '/file';
	var method = index ? "PUT" : "POST";

	//'/api/v2/admin/pretranslated/project/' + project_id + '/resource/' + resource_id + '/file/' + index
	loading(true);
	$.ajax({
		url: url,
		processData: false,
		contentType: false,
		dataType: "html",
		cache: false,
		type: method,
		data: formData
	}).done(function (data) {
		console.log(data);
		loading(false);
		getData();
	}).fail(function(){
		loading(false);
	});
}

var deleteFile = function (file_id) {
	///api/v2/admin/pretranslated/project/(int: project_id)/resource/(int: resource_id)/file/(int: file_id)
	loading(true);
	$.ajax({
		url: "/api/v2/admin/pretranslated/project/" + project_id + "/resource/" + resource_id + "/file/" + file_id,
		processData: false,
		contentType: false,
		dataType: "html",
		cache: false,
		type: "DELETE"
	}).done(function (data) {
		console.log(data);
		loading(false);
		getData();
	}).fail(function(){
		loading(false);
	});
}

var editFile = function (file, file_id) {
	var formData = new FormData();
	formData.append("file", file.files[0], file.files[0].name);
	loading(true);
	$.ajax({
		url: "/api/v2/admin/pretranslated/project/" + project_id + "/resource/" + resource_id + "/file/" + file_id,
		processData: false,
		contentType: false,
		dataType: "html",
		cache: false,
		type: "PUT",
		data: formData
	}).done(function (data) {
		console.log(data);
		loading(false);
		getData();
	}).fail(function(){
		loading(false);
	});
}

var editFilePermission = function (selectBox, file_id) {
	var formData = new FormData();
	formData.append("preview_permission", $(selectBox).val());
	loading(true);
	$.ajax({
		url: "/api/v2/admin/pretranslated/project/" + project_id + "/resource/" + resource_id + "/file/" + file_id,
		processData: false,
		contentType: false,
		dataType: "html",
		cache: false,
		type: "PUT",
		data: formData
	}).done(function (data) {
		console.log(data);
		loading(false);
		getData();
		// location.reload();
	}).fail(function(){
		loading(false);
	});
}