var getData = function () {
	//후술
}

$(document).ready(function () {
	///api/v2/user/pretranslated/project/7/resource

	var resHtml = $(".resourceList").html();
	var prjHtml = $(".projectInformation").html();
	$(".resourceList").empty();
	$(".projectInformation").empty();
	getData = function () {
		loading(true);
		$.ajax({
			url: '/api/v2/user/pretranslated/project/' + project_id + '/resource',
			processData: false,
			contentType: false,
			dataType: "json",
			cache: false,
			type: 'GET'
		}).done(function (data) {
			var html = data.data.map(data => eval('`' + resHtml + '`')).join('');
			$(".resourceList").html(html);
			var html2 = eval('`' + prjHtml + '`');
			$(".projectInformation").html(html2);

			$("#tags").html(data.tags.split(',').map(function (tag) { return tag != "" ? `<span class="tagging">${tag}</span>` : "" }).join(''));


			$("#addTag").autocomplete({
				source: (function () {
					var xhr;
					return function (request, response) {
						if (xhr) {
							xhr.abort();
						}
						var url = "/api/v2/admin/pretranslated/project/" + project_id + "/tags";

						url += request.term.length < 1 ? "" : "/" + request.term

						xhr = $.ajax({
							url: url,
							data: {
							},
							dataType: 'json'
						}).done(function (data) {
							response(data);
						}).fail(function (xhr, ajaxOptions, thrownError) {
							response([]);
						}).always(function () {
						});

					}
				})(),
				minLength: 0,
				select: function (event, ui) {
					// log( "Selected: " + ui.item.value + " aka " + ui.item.id );
				},
				focus: function (e, ui) {
					console.log(this);
					return false;
				}
			});


			$("#inputPlatform").autocomplete({
				source: (function () {
					var xhr;
					return function (request, response) {
						if (xhr) {
							xhr.abort();
						}
						var url = "/api/v2/admin/pretranslated/project/" + project_id + "/platform";

						url += request.term.length < 1 ? "" : "/" + request.term

						xhr = $.ajax({
							url: url,
							data: {
							},
							dataType: 'json',
							success: function (data) {
								response(data.filter(function (elem) {
									return elem != null;
								}));
							},
							error: function () {
								response([]);
							}
						});
					}
				})(),
				minLength: 0,
				select: function (event, ui) {
					// log( "Selected: " + ui.item.value + " aka " + ui.item.id );
				},
				focus: function (e, ui) {
					console.log(this);
					return false;
				}
			});

		}).fail(function (xhr, ajaxOptions, thrownError) {
			console.log('error');
		}).always(function () {
			loading(false);
			// $.ajax({
			// 	url: '/api/v2/user/pretranslated/project',
			// 	processData: false,
			// 	contentType: false,
			// 	dataType: "json",
			// 	cache: false,
			// 	type: 'GET'
			// }).done(function (data) {
			// 	var html = data.data.filter(data => data.id == project_id).map(data => eval('`' + prjHtml + '`')).join('');
			// 	$(".projectInformation").html(html);
			// }).fail(function (xhr, ajaxOptions, thrownError) {
			// 	console.log('error');
			// }).always(function () {
			// loading(false);
			// });
		});
	}

	getData();


	var isPressed = false;
	$("body").on("focus", "#addTag, #inputPlatform", function (e) {
		$(this).keydown();
	});
	$("body").on("keydown", "#addTag", function (e) {
		if (e.which == 13 || e.which == 188) {
			e.preventDefault();
			if (isPressed) return;
			if ($(this).val().length > 0) {
				var searchString = $(this).val();
				isPressed = true;
				$.ajax({
					url: "/api/v2/admin/pretranslated/project/" + project_id + "/tags/" + searchString,
					data: {},
					dataType: 'json',
					method: 'get',
					processData: false,
					contentType: false,
					cache: false
				}).done(function (data) {
					console.log(data);
					var isExist = data.indexOf(searchString) >= 0 ? true : confirm("존재하지 않는 태그입니다. 새로 생성합니까?");
					isExist ? $.ajax({
						url: "/api/v2/admin/pretranslated/project/" + project_id + "/tags/" + searchString,
						dataType: 'html',
						method: 'post',
						processData: false,
						contentType: false,
						cache: false
					}).done(function (data) {
						$("#editProject").submit();
					}).fail(function (xhr, ajaxOptions, thrownError) {
						alert("실패");
					}).always(function () {
						isPressed = false;
					}) : "";

				}).fail(function (xhr, ajaxOptions, thrownError) {
					isPressed = false;
				}).always(function () {
				});
			}
			else {
				return;
			}
		}
	});

	$("body").on("change", "#editCover", function (e) {
		e.preventDefault();
		var formData = new FormData();
		var file = $(this)[0].files[0];
		formData.append("cover_photo", file, file.name);

		var url = "/api/v2/admin/pretranslated/project/" + project_id;
		var method = "PUT";

		//'/api/v2/admin/pretranslated/project/' + project_id + '/resource/' + resource_id + '/file/' + index
		loading(true);
		$.ajax({
			url: url,
			processData: false,
			contentType: false,
			dataType: "html",
			cache: false,
			type: method,
			data: formData
		}).done(function (data) {
			console.log(data);
			loading(false);
			getData();
		}).fail(function () {
			loading(false);
		});
	});

	$("body").on("submit", "#editProject", function (e) {
		e.preventDefault();
		var formData = new FormData($(this)[0]);
		var url = "/api/v2/admin/pretranslated/project/" + project_id;
		var method = "PUT";
		$.ajax({
			url: url,
			processData: false,
			contentType: false,
			dataType: "html",
			cache: false,
			type: method,
			data: formData
		}).done(function (data) {
			console.log(data);
			loading(false);
			getData();
		}).fail(function () {
			loading(false);
		});
	});

	$("body").on("click", "#deleteProject", function () {
		if (confirm("정말 삭제하겠습니까?")) {
			var url = "/api/v2/admin/pretranslated/project/" + project_id;
			var method = "delete";
			$.ajax({
				url: url,
				processData: false,
				contentType: false,
				dataType: "html",
				cache: false,
				type: method
			}).done(function (data) {
				console.log(data);
				history.back();
			});
		}
	});

	$("body").on("click", ".tagging", function () {
		var searchString = $(this).text();
		$.ajax({
			url: "/api/v2/admin/pretranslated/project/" + project_id + "/tags/" + searchString,
			data: {},
			dataType: 'html',
			method: 'delete',
			processData: false,
			contentType: false,
			cache: false
		}).done(function (data) {
			getData();
		}).fail(function (xhr, ajaxOptions, thrownError) {
			alert("실패");
		}).always(function () {
		});
	});

});

var newResource = function () {
	///api/v2/admin/pretranslated/project/(int: project_id)/resource

	var formData = new FormData();
	formData.append('target_language_id', '1');
	formData.append('theme', '');
	formData.append('description', '');
	formData.append('tone_id', '0');
	formData.append('read_permission_level', '0');
	formData.append('price', '0');
	formData.append('is_original', 'false');

	$.ajax({
		url: '/api/v2/admin/pretranslated/project/' + project_id + '/resource',
		processData: false,
		contentType: false,
		dataType: "json",
		cache: false,
		type: 'POST',
		data: formData
	}).done(function (data) {
		location.href = '/admin/pretranslated/' + project_id + '/' + data.resource_id;
	}).fail(function (xhr, ajaxOptions, thrownError) {
		console.log('error');
	}).always(function () {
		onLoading = false;
	});
}