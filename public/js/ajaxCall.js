/*******************************************
    
    전역변수

********************************************/
//get방식 url 받아오기. (ex - ?pid=2&rid=3에서 값만 받아오는 플러그인)
var QueryString = function () {
    // This function is anonymous, is executed immediately and 
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();


//어조 설정(톤)
var tone = ["", "원망", "긴급한", "애매한", "존경", "애원", "비판적인", "변명", "솔직한", "감정적인", "기분좋은"];
//format설정()
var format = ["전자우편", "편지", "에세이", "광고", "초대장", "안부", "출판물", "대본", "리서치", "레포트", "메뉴", "지원서", "설명서", "SNS", "기타"];
//subject설정(분야)
var subject = ["", "예술", "스포츠", "과학", "건강", "경제", "음악", "문학", "정치", "비즈니스", "종교", "쇼핑", "패션"];
//language설정(언어)
var lang = ["", "한국어", "영어(미국)/미국", "영어(영국)/영국", "북경어(중국)/중국본토", "광동어(중국)/중국(홍콩,마카오)", "태국어", "대만어(중국)", "일본어", "스페인어", "포르투갈어", "베트남", "독일어", "프랑스어"];
//language설정(언어)
var shortLang = ["", "한", "영", "영", "북", "광", "태", "대", "일", "스", "포", "베", "독", "프"];
//프로젝트 공유상태
var permission_level = ["", "무료", "유료"];
//프로젝트 권한보기설정
var preview_permission = ["미리보기불가", "미리보기권한", "다운로드권한"];
//url주소 받기
var location_info = location.pathname.split("/").pop();
//클레스안에 사용할 this변수
var objThis = this;
//서버 페이징 변수
var page = 1;
//유저 고유아이디
var user_id = 0;

//프로젝트 및 리소스 생성, 유저관리 팝업창

$("body").on("click", ".popup_btn", function () {
    $(".popup").addClass("visible");
});
$("body").on("click", ".popup_close", function () {
    $(".popup").removeClass("visible");
});

$("body").on("click", ".popup", function (e) {
    var $container = $(".popup_box");

    if ($(this).hasClass("visible") && !$container.is(e.target) && $container.has(e.target).length === 0) {
        $(this).removeClass("visible");
    }
});

$("body").on("keydown", function(e){
    if(e.keyCode === 27){
        $(".popup_close").click();
    }
});


//프로젝트 & 리소스 전체 선택
$("body").on("change", "input[id='checkAll']", function () {
    console.log($("input[id='checkAll']").is(":checked"));
    if ($(this).is(":checked")) {
        $("input[type='checkbox']").prop("checked", true);
    } else {
        $("input[type='checkbox']").prop("checked", false);
    }
});


/*******************************************
    
    ajax 시작

********************************************/
$(document).ready(function () {
    //ajax 및 API 모음(인스턴스 클래스 방식)
    var ajaxStart = new AjaxStart();
    //get방식 API
    switch (location_info) {
        case "main":
            //메인페이지
            ajaxStart.get_main();
            break;
        case "commis":
            //의뢰물관리
            ajaxStart.get_commis();
            break;
        case "userM":
            //권한관리
            ajaxStart.get_userM();
            break;
        case "requester":
            //의뢰인관리
            ajaxStart.get_userM_requester();
            break;
        case "translator":
            //번역가관리
            ajaxStart.get_userM_translator();
            break;
        case "admin":
            //관리자관리
            ajaxStart.get_userM_admin();
            break;
        /*case "projectG":
            //프로젝트 보기
            ajaxStart.get_project(); 
            break;*/
        case "modify":
            //프로젝트 수정
            ajaxStart.get_project_modify();
            break;
        case "projectG_resource":
            //프로젝트 리소스 보기
            ajaxStart.get_project_resource();
            break;
        case "view":
            //프로젝트 리소스 정보보기
            ajaxStart.get_resource_view();
            break;
        case "put":
            //프로젝트 리소스 정보수정
            ajaxStart.get_resource_put();
            break;
    }

    //유저관리통합
    if (location_info == "userM" || location_info == "requester" || location_info == "translator" || location_info == "admin") {
        location_info = "userM";
    }
    //KANGAROO통합
    if (location_info == "tagData" || location_info == "photoData") {
        location_info = "kangaroo";
    }
    //프로젝트통합
    if (location_info == "projectG" || location_info == "modify" || location_info == "projectG_resource" || location_info == "view" || location_info == "put") {
        location_info = "project";
    }
    //DB통합
    if (location_info == "DBexport" || location_info == "DBimport") {
        location_info = "DB";
    }

    //체인지 이벤트
    $("body").on("change", function (e) {
        this.option = {
            user_id: $(e.target).parents(".cursor").children().first().attr("value"),
            status: $(e.target).val(),
            target: e.target
        };
        switch (location_info) {
            case "commis":
                switch (e.target.name) {
                    case "status":
                        ajaxStart.put_commis(this.option); break;
                }
                break;
            case "userM":
                switch (e.target.name) {
                    case "permission_id":
                        ajaxStart.put_userM_per(this.option); break;
                    case "activity_id":
                        ajaxStart.put_userM_act(this.option); break;
                    case "lang":
                        ajaxStart.put_langChange(this.option); break;
                    case "powerAll":
                        ajaxStart.post_userM_adminAllCheck(this.option); break;
                    case "power":
                        ajaxStart.post_userM_adminCheck(this.option); break;
                }
                break;
            case "project":
                switch (e.target.className) {
                    case "upload_file":
                        ajaxStart.post_upload_file(); break;
                    case "file_modify":
                        ajaxStart.put_file_modify(this.option); break;
                    case "file_modify":
                        ajaxStart.delete_file_delete(this.option); break;
                    case "preview_permission":
                        ajaxStart.put_preview_permission(this.option); break;
                }
                break;
        }
    });
    //클릭 이벤트
    $("body").on("click", function (e) {
        this.option = {
            user_id: $(e.target).parents(".cursor").children().first().attr("value"),
            status: $(e.target).val(),
            target: e.target
        };
        switch (location_info) {
            case "userM":
                switch (e.target.className) {
                    case "info":
                        ajaxStart.get_userM_data(this.option); break;
                }
                break;
            case "project":
                switch (e.target.className) {
                    case "sel":
                        ajaxStart.project_move(this.option); break;
                    case "project_push":
                        ajaxStart.post_project_create(this.option); break;
                    case "projectG_modify":
                        ajaxStart.put_projectG_modify(this.option); break;
                    case "project_delete":
                        ajaxStart.delete_project(this.option); break;
                    case "resource_modify":
                        location.href = "/admin/projectG_resource/put?pid=" + QueryString.pid + "&rid=" + QueryString.rid; break;
                    case "contents_update":
                        ajaxStart.put_contents_update(); break;
                    case "file_delete":
                        ajaxStart.delete_resource_file(this.option); break;
                }
                break;
        }
    });
});

//AjaxStart클래스 생성(멤버변수)
function AjaxStart() {
    this.objOp = null;
    this.option = null;
}

//ajax옵션 설정 합치기
AjaxStart.prototype.ajaxConnect = function (option) {
    this.objOp = {
        url: "",
        type: "",
        dataType: "",
        contentType: false,
        processData: false,
        formData: "",
        selector: "",
        etc: "",
        target: ""
    }
    this.objOp = $.extend({}, this.objOp, option);
    //ajax실행 함수
    this.ajaxExecute(this.objOp);
}

/*******************************************
    
    ajax get방식

********************************************/
//메인페이지
AjaxStart.prototype.get_main = function () {
    //번역물 총 개수 API
    this.objOp = {
        url: "/api/v2/admin/stats/overview",
        type: "get",
        dataType: "",
        selector: "get_overview"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);

    //급한 번역물(5개) 보기 API
    this.objOp = {
        url: "/api/v2/admin/stats/aboutDeadline",
        type: "get",
        dataType: "",
        selector: "get_aboutDeadline"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//의뢰물관리 페이지
AjaxStart.prototype.get_commis = function () {
    //page의 전역변수는 다른 이벤트를 생성하여 받아와야 함.(페이징이 들어가는 곳은 전부 그렇게 해야한다고 생각합니다.(연범 의견))
    //의뢰물 정보 받아오기
    this.objOp = {
        url: "/api/v2/admin/requests",
        //페이지의 총 개수나 데이터의 총 개수가 생성되어 kangaroo의 페이징과 같다면 사용.
        // url: "/api/v2/admin/requests?page=" + page + "&itemPerPage=20", 
        type: "get",
        dataType: "json",
        selector: "get_commis"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//유저관리 페이지
AjaxStart.prototype.get_userM = function (option) {
    //모든 유저의 정보
    this.objOp = {
        // url: "/api/v2/admin/permissionControlUserLists?page=" + page + "&itemPerPage=20",
        url: "/api/v2/admin/permissionControlUserLists?page=" + page,
        type: "get",
        dataType: "json",
        selector: "get_userM",
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//의뢰인관리 페이지
AjaxStart.prototype.get_userM_requester = function () {
    //의뢰인 정보
    this.objOp = {
        // url: "/api/v2/admin/userLists?page=" + page + "&itemPerPage=20",
        url: "/api/v2/admin/userLists?page=" + page,
        type: "get",
        dataType: "",
        selector: "get_userM_requester"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//번역가관리 페이지
AjaxStart.prototype.get_userM_translator = function () {
    //번역가 정보
    this.objOp = {
        // url: "/api/v2/admin/translatorLists?page=" + page + "&itemPerPage=20",
        url: "/api/v2/admin/translatorLists?page=" + page,
        type: "get",
        dataType: "",
        selector: "get_userM_translator"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//관리자관리 페이지
AjaxStart.prototype.get_userM_admin = function () {
    //관리자 정보
    this.objOp = {
        // url: "/api/v2/admin/adminLists?page=" + page + "&itemPerPage=20",
        url: "/api/v2/admin/adminLists?page=" + page,
        type: "get",
        dataType: "",
        selector: "get_userM_admin"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//유저정보 가져오기
AjaxStart.prototype.get_userM_data = function (option) {
    //this변수
    var $this = $(option.target);
    //유저 이메일
    var user_email = $this.next().text();
    //유저정보 상세보기
    this.objOp = {
        url: "/api/v2/user/profile?user_email=" + user_email,
        type: "get",
        dataType: "json",
        target: $this,
        selector: "get_userM_data"
    };
    //유저정보 없애기
    $(".details td").remove();
    if (!$this.parent().next().hasClass("view")) {
        //클래스가 없을 경우 상세보기 생성
        //ajax옵션 설정하러 가기
        this.ajaxConnect(this.objOp);

    } else if ($this.parent().next().hasClass("view")) {
        //클래스가 있을 경우 상세보기 없어짐.
        $this.parent().next().removeClass("view");
        $this.parent().next().hide();
    }
}
//프로젝트관리 페이지
AjaxStart.prototype.get_project = function () {
    //프로젝트 정보
    this.objOp = {
        // url: "/api/v2/user/pretranslated/project?page=" + page + "&itemPerPage=20",
        url: "/api/v2/user/pretranslated/project?page=" + page,
        type: "get",
        dataType: "",
        selector: "get_project"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//프로젝트수정 페이지
AjaxStart.prototype.get_project_modify = function () {
    //프로젝트 정보 수정
    //개인적(연범)으로 프로파일API처럼 따로 하나의 정보에 대한 값만 받을 수 있으면 좋을 것 같다.
    this.objOp = {
        url: "/api/v2/user/pretranslated/project",
        type: "get",
        dataType: "",
        selector: "get_project_modify"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//프로젝트리소스관리 페이지
AjaxStart.prototype.get_project_resource = function () {
    //프로젝트 리소스 정보
    //아직 페이징하기 위한 옵션이 없음.
    this.objOp = {
        url: "/api/v2/user/pretranslated/project/" + QueryString.pid + "/resource",
        type: "get",
        dataType: "",
        selector: "get_project_resource"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//리스소정보보기 페이지
AjaxStart.prototype.get_resource_view = function () {
    //리소스 상세정보 보기
    this.objOp = {
        url: "/api/v2/user/pretranslated/project/" + QueryString.pid + "/resource",
        type: "get",
        dataType: "",
        selector: "get_resource_view"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}
//리소스정보수정 페이지
AjaxStart.prototype.get_resource_put = function () {
    //리소스 수정할 정보 보기
    this.objOp = {
        url: "/api/v2/user/pretranslated/project/" + QueryString.pid + "/resource",
        type: "get",
        dataType: "",
        selector: "get_resource_put"
    }
    //ajax옵션 설정하러 가기
    this.ajaxConnect(this.objOp);
}

/*******************************************
    
    ajax post방식

********************************************/
//관리자관리 페이지
AjaxStart.prototype.post_userM_adminAllCheck = function (option) {
    //each문 안에서도 사용할 수 있는 this변수 생성
    var objThis = this;
    //this변수
    var $this = $(option.target);
    //check된 함수 담는 변수
    var val = [];
    //관리자 ajax옵션
    this.objOp = {
        type: "POST",
        dataType: ""
    };
    if ($this.is(":checked")) {
        $.each($this.siblings("input[name='power']:not(:checked)"), function (index, item) {
            val.push(item.value);
        });
        $.each(val, function (index, item) {
            option.page = item;
            option.action = "grant";
            objThis.objOp.url = "/api/v2/admin/user/" + option.user_id + "/permission/page/" + option.page + "/" + option.action;
            //ajax값 넘기기
            objThis.ajaxConnect(objThis.objOp);
        })
        $this.siblings("input[name='power']").prop("checked", true);
    } else {
        $.each($this.siblings("input[name='power']"), function (index, item) {
            option.page = $this.siblings("input[name='power']").eq((index - 1)).attr("value");
            option.action = "revoke";
            objThis.objOp.url = "/api/v2/admin/user/" + option.user_id + "/permission/page/" + option.page + "/" + option.action;
            //ajax값 넘기기
            objThis.ajaxConnect(objThis.objOp);
        });
        $this.siblings("input[name='power']").prop("checked", false);
    }

}
//관리자관리 페이지
AjaxStart.prototype.post_userM_adminCheck = function (option) {
    var objThis = this;
    var $this = $(option.target);
    this.objOp = {
        type: "POST",
        dataType: "",
    };
    var page = option.status;
    var action = "";
    if ($this.is(":checked")) {
        action = "grant";
        this.objOp.url = "/api/v2/admin/user/" + option.user_id + "/permission/page/" + page + "/" + action
    } else {
        action = "revoke";
        this.objOp.url = "/api/v2/admin/user/" + option.user_id + "/permission/page/" + page + "/" + action
    }
    // this.objOp[0].url = "/api/v2/admin/requests/" + option.user_id + "/status/" + option.status;
    //ajax값 넘기기
    this.ajaxConnect(this.objOp);
}
//프로젝트 리소스로 이동
AjaxStart.prototype.project_move = function (option) {
    var $this = $(option.target);
    var pid = $this.prev().attr("value");
    if (QueryString.pid) {
        var rid = $this.prev().attr("value");
        pid = QueryString.pid;
        location.href = "/admin/projectG_resource/view?pid=" + pid + "&rid=" + rid;
    } else {
        location.href = "/admin/projectG_resource?pid=" + pid;
    }

}
//프로젝트 전체선택
AjaxStart.prototype.post_project_create = function (option) {
    var $this = option.target;
    this.objOp = {
        url: "/api/v2/admin/pretranslated/project",
        type: "POST",
        dataType: "JSON",
        selector: "reload"
    };
    if ($this.id == "projectG_create") {
        var original_lang_id = $("select[name='original_lang']").val();
        var author = $("#author").val();
        var subject_id = $("select[name='subject']").val();
        var format_id = $("select[name='format']").val();
        var cover_photo = $("input[id='cover']")[0].files[0];
        var cover_name = $("input.cover").val();
        var data = new FormData();
        data.append("original_lang_id", original_lang_id);
        data.append("format_id", format_id);
        data.append("subject_id", subject_id);
        data.append("author", author);
        data.append("cover_photo", cover_photo, cover_name);
        this.objOp.formData = data;
        this.ajaxConnect(this.objOp);
    } else if ($this.id == "projectG_resource_create") {
        this.objOp.url = "/api/v2/admin/pretranslated/project/" + QueryString.pid + "/resource";
        var target_language_id = $("select[name='original_lang']").val()
        var theme = $("#input_resource_title").val();
        var description = $("#description").val();
        var tone_id = $("select[name='tone_id']").val();
        var read_permission_level = $("select[name='read_permission_level']").val();
        var price = $("input[name='price']").val();
        var is_original = $("input[name='is_original']").prop("checked");
        var data = new FormData();
        data.append("target_language_id", target_language_id);
        data.append("theme", theme);
        data.append("description", description);
        data.append("tone_id", tone_id);
        data.append("read_permission_level", read_permission_level);
        data.append("price", price);
        data.append("is_original", is_original);
        this.objOp.formData = data;
        this.ajaxConnect(this.objOp);
    }
}
//리소스파일 업로드
AjaxStart.prototype.post_upload_file = function () {
    var files = $("input[type='file']")[0].files;
    var data = new FormData();
    data.append("preview_permission", 0);
    $.each(files, function (index, item) {
        data.append("file_list[]", item, item.name);
    });
    this.objOp = {
        url: "/api/v2/admin/pretranslated/project/" + QueryString.pid + "/resource/" + QueryString.rid + "/file",
        type: "POST",
        dataType: "",
        selector: "post_upload_file"
    };
    this.objOp.formData = data;
    this.ajaxConnect(this.objOp);
}
/*******************************************
    
    ajax put방식

********************************************/
//의뢰물관리 페이지
AjaxStart.prototype.put_commis = function (option) {
    this.objOp = {
        url: "/api/v2/admin/requests/" + option.user_id + "/status/" + option.status,
        type: "PUT",
        dataType: "",
        selector: "put_commis"
    };
    //ajax값 넘기기
    this.ajaxConnect(this.objOp);

}
//유저구분관리 페이지
AjaxStart.prototype.put_userM_per = function (option) {
    this.objOp = {
        url: "/api/v2/admin/user/" + option.user_id + "/permission/" + option.status,
        type: "PUT",
        selector: "put_userM_per"
    };
    //ajax값 넘기기
    this.ajaxConnect(this.objOp);
}
//활동관리 페이지
AjaxStart.prototype.put_userM_act = function (option) {
    var $this = $(option.target);
    if ($this.attr("class") == "activity") {
        user_id = $this.parents("#modify_info").find("#user_mail").attr("value");
    } else {
        user_id = $this.parents(".cursor").children().first().attr("value");
    }
    this.objOp = {
        url: "/api/v2/admin/user/" + user_id + "/activity/" + option.status,
        type: "PUT",
        dataType: "",
    };
    //ajax값 넘기기
    this.ajaxConnect(this.objOp);
}
//유저정보관리 페이지
AjaxStart.prototype.put_langChange = function (option) {
    var $this = $(option.target);
    console.log($this.value);
    this.objOp = {
        dataType: "",
        target: ""
    }
    user_id = $this.parents("div#modify_info").find("span#user_mail").attr("value");
    console.log(user_id)
    if ($this.is(":checked")) {
        var data = new FormData();
        data.append("language_id", $this.val());
        //번역 언어 추가
        this.objOp.url = "/api/v2/admin/user/" + user_id + "/assignLanguage";
        this.objOp.type = "post";
        this.objOp.formData = data;
        this.objOp.selector = "";
    } else if (!$this.is(":checked")) {
        //번역 언어 삭세
        this.objOp.type = "delete";
        this.objOp.url = "/api/v2/admin/user/" + user_id + "/deassignLanguage/" + option.status;
        this.objOp.selector = "";
    }
    //ajax값 넘기기
    console.log(this.objOp)
    this.ajaxConnect(this.objOp);
}
//프로젝트정보 수정
AjaxStart.prototype.put_projectG_modify = function () {
    var original_lang = $("select[name='original_lang']").val();
    var format = $("select[name='format']").val();
    var subject = $("select[name='subject']").val();
    var author = $("#author").val();
    var cover = $("#cover")[0].files[0];
    var cover_name = $("input[name='cover']").val();
    var data = new FormData();
    data.append("original_lang_id", original_lang);
    data.append("format_id", format);
    data.append("subject_id", subject);
    data.append("author", author);
    data.append("cover_photo", cover, cover_name);
    this.objOp = {
        url: "/api/v2/admin/pretranslated/project/" + QueryString.pid,
        type: "put",
        dataType: "",
        formData: data,
        selector: "put_projectG_modify"
    }
    this.ajaxConnect(this.objOp);
}
//리소스정보 수정
AjaxStart.prototype.put_contents_update = function () {
    this.objOp = {
        url: "/api/v2/admin/pretranslated/project/" + QueryString.pid + "/resource/" + QueryString.rid,
        type: "PUT",
        dataType: "",
    };
    var target_language_id = $("select[name='target_language_id']").val();
    var theme = $("input[name='theme']").val();
    var description = $("input[name='description']").val();
    var tone_id = $("select[name='tone_id']").val();
    var read_permission_level = $("select[name='read_permission_level']").val();
    var price = $("input[name='price']").val();
    var data = new FormData();
    data.append("target_language_id", target_language_id);
    data.append("theme", theme);
    data.append("description", description);
    data.append("tone_id", tone_id);
    data.append("read_permission_level", read_permission_level);
    data.append("price", price);
    this.objOp.formData = data;
    this.ajaxConnect(this.objOp);
}
//리소스파일 수정
AjaxStart.prototype.put_file_modify = function (option) {
    var $this = option.target;
    var file_id = $($this).parents("tr").attr("value");
    var preview_permission = $($this).parents('tr').find("select").val();
    var files = $($this)[0].files[0];
    var data = new FormData();
    data.append("preview_permission", preview_permission);
    data.append("file", files, files.name);
    this.objOp = {
        url: "/api/v2/admin/pretranslated/project/" + QueryString.pid + "/resource/" + QueryString.rid + "/file/" + file_id,
        type: "PUT",
        dataType: "",
        formData: data,
        selector: "reload"
    };
    // this.objOp.formData = data;
    this.ajaxConnect(this.objOp);
}
//리소스파일 권한수정
AjaxStart.prototype.put_preview_permission = function (option) {
    var $this = option.target;
    var file_id = $($this).parents("tr").attr("value");
    var preview_permission = $($this).parent().find("select").val();
    // var files = $(this)[0].files;

    var data = new FormData();
    this.objOp = {
        url: "/api/v2/admin/pretranslated/project/" + QueryString.pid + "/resource/" + QueryString.rid + "/file/" + file_id,
        type: "PUT",
        dataType: "",
    };
    data.append("preview_permission", preview_permission);
    this.objOp.formData = data;
    this.ajaxConnect(this.objOp);
}
/*******************************************
    
    ajax delete방식

********************************************/
//프로젝트 삭제
AjaxStart.prototype.delete_project = function (option) {
    var $this = option.target;
    var objThis = this;
    this.objOp = {
        url: "",
        type: "DELETE",
        dataType: "",
        selector: "reload"
    };
    if ($this.id == "projectG_delete") {
        $.each($("input[name^='choose']:checked"), function (index, item) {
            var project_id = $(item).parents(".cursor").children().first().attr("value");
            objThis.objOp.url = "/api/v2/admin/pretranslated/project/" + project_id;
            objThis.ajaxConnect(objThis.objOp);
        });
    } else if ($this.id == "projectG_resource_delete") {
        $.each($("input[name^='choose']:checked"), function (index, item) {
            var resource_id = $(item).parents(".cursor").children().first().attr("value");
            objThis.objOp.url = "/api/v2/admin/pretranslated/project/" + QueryString.pid + "/resource/" + resource_id;
            objThis.ajaxConnect(objThis.objOp);
        });
    }
}
AjaxStart.prototype.delete_resource_file = function (option) {
    var objThis = this;
    var checked = $("input[name='resource_file']:checked");
    this.objOp = {
        type: "DELETE",
        dataType: "",
        selector: "reload"
    };
    $.each(checked, function (index, item) {
        var file_id = $(item).parents(".file_view").attr("value");
        objThis.objOp.url = "/api/v2/admin/pretranslated/project/" + QueryString.pid + "/resource/" + QueryString.rid + "/file/" + file_id;
        objThis.ajaxConnect(objThis.objOp);
    });
}




/*******************************************
    
    ajax 실행구문

********************************************/
//ajax실행
AjaxStart.prototype.ajaxExecute = function (option) {
    $.ajax({
        url: option.url,
        type: option.type,
        dataType: option.dataType,
        contentType: option.contentType,
        processData: option.processData,
        data: option.formData
    }).done(function (data) {
        console.log("성공");
        //get방식
        // console.log(option.selector)
        switch (option.selector) {
            case "get_overview":
                get_overview(data);
                break;
            case "get_aboutDeadline":
                get_aboutDeadline(data);
                break;
            case "get_commis":
                get_commis(data);
                break;
            case "get_userM":
                get_userM(data);
                break;
            case "get_userM_requester":
                get_userM_requester(data);
                break;
            case "get_userM_translator":
                get_userM_translator(data);
                break;
            case "get_userM_admin":
                get_userM_admin(data);
                break;
            case "get_userM_data":
                get_userM_data(data, option);
                break;
            case "get_project":
                get_project(data);
                break;
            case "get_project_modify":
                get_project_modify(data);
                break;
            case "get_project_resource":
                get_project_resource(data);
                break;
            case "get_resource_view":
                get_resource_view(data);
                break;
            case "get_resource_put":
                get_resource_put(data);
                break;
            case "put_projectG_modify":
                location.href = "/admin/projectG";
                break;
            case "post_upload_file":
                location.reload();
                break;
            case "reload":
                location.reload();
                break;
        }
    }).fail(function (err) {
        console.log(err)
        // switch(option.selector) {
        //     case "":
        //         login(); break;
        // }
    });
}