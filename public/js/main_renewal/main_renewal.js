// var requestedData = {};
// var isAuthInProgress = false;
// var token = "";
var count_span;
var deadline;
var langs = ["한국어", "영어(미국) / 미국", "영어(영국) / 영국", "북경어(중국) / 중국본토", "광동어(중국) / 중국(홍콩, 마카오)", "대만어(중국)", "일본어", "스페인어", "프랑스어", "독일어", "이탈리아어", "태국어", "베트남어"];
var lang7 = ["한국어", "영어(미국) / 미국", "영어(영국) / 영국", "북경어(중국) / 중국본토", "광동어(중국) / 중국(홍콩, 마카오)", "대만어(중국)", "일본어"];
var lang8 = ["한국어", "스페인어", "프랑스어"];
var lang9 = ["한국어", "독일어", "이탈리아어", "베트남어"];
var lang10 = ["한국어", "태국어", "아랍어", "기타"];

var locationQueryString;
var add_text_val = 70;
var basic_word_num = 220;
var basic_price;

$(document).ready(function () {

    locationQueryString = {};
    location.search.substr(1).split("&").forEach(function (pair) {
        if (pair === "") return;
        var parts = pair.split("=");
        locationQueryString[parts[0]] = parts[1] &&
            decodeURIComponent(parts[1].replace(/\+/g, " "));
    });

    if (!(locationQueryString.type > 0 && locationQueryString.type <= 10)) {
        locationQueryString.type = 1;
    }

    if (locationQueryString.type == 1 || locationQueryString.type == 3 || locationQueryString.type == 4) {

        option_add(langs, true, $("#original_language"), $(".target_language"));

    } else if (locationQueryString.type >= 7 && locationQueryString.type <= 10) {
        $(".div70 .inside_doc").hide();
        $(".div30 .inside_doc").hide();

        if (locationQueryString.type == 7) {

            option_add(lang7, false, $("#original_language"), $(".target_language"));

        } else if (locationQueryString.type == 8) {

            option_add(lang8, false, $("#original_language"), $(".target_language"));
        } else if (locationQueryString.type == 9) {

            option_add(lang9, false, $("#original_language"), $(".target_language"));
        } else {

            option_add(lang10, false, $("#original_language"), $(".target_language"));
        }

        $("#original_language").find("option").eq(1).prop("selected","selected");
        $(".target_language").find("option").eq(0).prop("selected","selected");

    } else {

        option_add(langs, false, $("#original_language"), $(".target_language"));
    }


    var flag = false;

    $("body").on("click", ".nav-wrapper li", function () {
        if (!flag) {
            flag = true;
            setTimeout(function () {
                flag = false;
            }, 260);
            $(".nav-wrapper").toggleClass("invisible");
            $(this).addClass("active");
            $(this).siblings("li").removeClass("active");
        }
        return false;
    });

    otherClick();

    $("body").on("click", ".fixed_header .button", function () {
        $(this).siblings(".nav-wrapper").toggleClass("invisible");
    });

    $(document).on("scroll", onScroll);

    //smoothscroll
    $("body").on("click", "#menu a[href^='#']", function (e) {
        e.preventDefault();
        // $(document).off("scroll");

        $("a").each(function () {
            $(this).removeClass("active");
        })
        $(this).addClass("active");

        var target = this.hash,
            menu = target;
        $target = $(target);
        $("html, body").stop().animate({
            "scrollTop": $target.offset().top - 10
        }, 500, "swing", function () {
            window.location.hash = target;
            // $(document).on("scroll", onScroll());
        });
    });

    var arr = $(".slide-container");

    var getCurrentSlide = function () {
        var index = $("input[name=radio-btn]").index($("input[name=radio-btn]:checked"));
        return $($(".slide-container")[index]);
    }

    // var current = 0;
    function bgChange() {
        if (arr.length > 0) {
            // arr.removeClass("red");
            // arr.eq(current).find(".nav .next").click();

            // current = (current + 1) % arr.length;

            getCurrentSlide().find(".nav .next").click();
        }
    }
    var autoslide = setInterval(bgChange, 3000);

    $("body").on("mouseenter", ".form-request .container", function () {
        clearInterval(autoslide);
    });
    $("body").on("mouseleave", ".form-request .container", function () {
        autoslide = setInterval(bgChange, 3000);
    });

    var startCoords = 0;
    var endCoords = 0;
    // var tempY = 0;
    $(".slides").bind('touchstart', function (e) {
        // e.preventDefault();
        startCoords = endCoords = e.originalEvent.targetTouches[0].pageX;

        // tempY = e.originalEvent.targetTouches[0].pageY;
    });
    $(".slides").bind('touchmove', function (e) {
        clearInterval(autoslide);
        autoslide = setInterval(bgChange, 3000);

        endCoords = e.originalEvent.targetTouches[0].pageX;

        if (startCoords - endCoords > 60) {
            e.preventDefault();
        }
        if (endCoords - startCoords > 60) {
            e.preventDefault();
        }

    });
    $(".slides").bind('touchend', function (e) {
        e.preventDefault();
        // setTimeout(function () {
        if (startCoords - endCoords > 60) {
            getCurrentSlide().find(".nav .next").click();
        }
        if (endCoords - startCoords > 60) {
            getCurrentSlide().find(".nav .prev").click();
        }
        // }, 2000);

    });

    var resizeText = function () {
        // Standard height, for which the body font size is correct
        var preferredFontSize = 100; // %
        var preferredSize = 1024 * 768;

        var currentSize = $(window).width() * $(window).height();
        var scalePercentage = Math.sqrt(currentSize) / Math.sqrt(preferredSize);
        var newFontSize = preferredFontSize * scalePercentage;
        $("body").css("font-size", newFontSize + '%');
    };

    /*$(window).bind('resize', function() {
        resizeText();
    }).trigger('resize');*/

    if (locationQueryString.type == 2) {
        basic_price = 80000;
    } else {
        basic_price = 18000;
    }

    $(".total span").html(basic_price);

    var sub_text = "<sub>페이지 당 </sub>";

    $("body").on("click", "input[type='radio']", function () {

        // radio_val = $(this).attr("value");
        label_text = $(this).next("label").text();

        $(".slide div.web_img").css("background-image", "url(/img/IMG_070" + locationQueryString.type + ".jpg)");

        $(".slide div.mobile_img").css("background-image", "url(/img/IMG_070" + locationQueryString.type + "_mobile.jpg)");

        $(".div30_inside").find(".subject").text(label_text);
    });

    
    if(locationQueryString.type > 6){
        
        $(".inside_right #subject7").click();
        
    } else {
        
        $(".inside_right #subject" + locationQueryString.type).click();
        $(".inside_right label[for=subject" + locationQueryString.type + "]").siblings("label").css("display", "none");
    }



    $("body").on("change", "#original_language", function () {

        var original_language_val = parseInt($("#original_language option:selected").attr("lang_val"));
        var original_lang_val = $("#original_language option:selected").val();
        var target_language_val = parseInt($(".target_language option:selected").attr("lang_val"));
        var target_lang_val = $(".target_language option:selected").val();
        var count_word = parseInt($(".count_word").html());
        var count_text = parseInt($(".count_text").html());

        if (original_language_val != 0) {
            $(".target_language").find("option[lang_val='0']").attr("selected", "selected");
            $(".target_language").attr("disabled", true);
        } else {
            $(".target_language").attr("disabled", false);
        }

        if (locationQueryString.type == 2) {
            basic_price = 80000;

        } else {

            if (target_language_val == 0) {

                $(".text_lang").addClass("invisible");
                $(".word_lang").removeClass("invisible");

                if(original_language_val == 0){
                    basic_price = 0;

                } else if (original_lang_val.indexOf("영어") > -1) {
                    add_text_val = 60;
                    basic_price = 15000;
                    basic_word_num = 250;

                    word_price(basic_price, basic_word_num, count_word, add_text_val);

                } else if (original_lang_val.indexOf("스페인") > -1 || original_lang_val.indexOf("프랑스") > -1) {
                    add_text_val = 100;
                    basic_price = 25000;
                    basic_word_num = 220;

                    word_price(basic_price, basic_word_num, count_word, add_text_val);

                } else if (original_lang_val.indexOf("중국") > -1 || original_lang_val.indexOf("일본") > -1) {
                    $(".text_lang").removeClass("invisible");
                    $(".word_lang").addClass("invisible");

                    add_text_val = 20;
                    basic_price = 15000;
                    basic_word_num = 650;

                    word_price(basic_price, basic_word_num, count_text, add_text_val);
                } else {

                    if(original_lang_val.indexOf("기타") > -1){
                        $(".etc_lang").removeClass("invisible");
                    } else {
                        $(".etc_lang").addClass("invisible");
                    }

                    add_text_val = 120;
                    basic_price = 30000;
                    basic_word_num = 220;
                    
                    word_price(basic_price, basic_word_num, count_word, add_text_val);
                }
            }
        }


        var original_lang = $(this).find("option[value='" + $(this).val() + "']").text();

        $(".div30_inside .original_lang").text(original_lang);

    });

    $("#original_language").change();

    $("body").on("change", ".target_language", function () {
        var count_word = parseInt($(".count_word").html());
        var count_text = parseInt($(".count_text").html());

        var original_language_val = parseInt($("#original_language option:selected").attr("lang_val"));
        var original_lang_val = $("#original_language option:selected").val();
        var target_language_val = parseInt($(this).find("option:selected").attr("lang_val"));
        var target_lang_val = $(this).find("option:selected").val();

        if (target_language_val != 0) {
            $("#original_language").find("option[lang_val='0']").attr("selected", "selected").trigger("click");
            $("#original_language").attr("disabled", true);
        } else {
            $("#original_language").attr("disabled", false);
        }

        if (locationQueryString.type == 2) {
            basic_price = 80000;

        } else {

            if (original_language_val == 0) {

                basic_word_num = 220;

                if(target_language_val == 0){
                    basic_price = 0;

                } else if (target_lang_val.indexOf("영어") > -1 || target_lang_val.indexOf("중국") > -1 || target_lang_val.indexOf("일본") > -1) {
                    add_text_val = 70;
                    basic_price = 18000;

                    word_price(basic_price, basic_word_num, count_word, add_text_val);

                } else if (target_lang_val.indexOf("스페인") > -1 || target_lang_val.indexOf("프랑스") > -1) {
                    add_text_val = 120;
                    basic_price = 30000;

                    word_price(basic_price, basic_word_num, count_word, add_text_val);

                } else {

                    if(target_lang_val.indexOf("기타") > -1){
                        $(".etc_lang").removeClass("invisible");
                    } else {
                        $(".etc_lang").addClass("invisible");
                    }

                    add_text_val = 140;
                    basic_price = 35000;
                    
                    word_price(basic_price, basic_word_num, count_word, add_text_val);
                }
                
            }
        }

        var target_lang = $(this).find("option[value='" + $(this).val() + "']").text();

        $(".div30_inside .target_lang").text(target_lang);
    });

    $(".target_language").change();

    $("body").on("click", "#form-request .div70 li", function () {
        $(this).toggleClass("select");
        $(this).siblings().removeClass("select");
        $(".total sub").remove();
        var original_val = $("#original_language option:selected").attr("lang_val");

        var wordCount = original_val >= 3 && original_val <= 6 ? parseInt($(".count_text").text()) : parseInt($(".count_word").text());
        var totalSpan = $(".total_price .total span");
        var selected_li = $(".step2_btn li.select span:eq(0)").text();
        var deadline = $(this).find(".deadline").text();


        $(".urgency_rate").html(selected_li + "<sub> " + deadline + "</sub>");

        if (locationQueryString.type == 2) {
            if ($("li.select").length < 1) {
                totalSpan.html(parseInt(parseFloat(basic_price)));
            } else {

                totalSpan.html(parseInt(parseFloat(basic_price) * parseFloat($(this).attr("multi"))));
            }
        } else {

            if (wordCount <= basic_word_num) {
                totalSpan.html(parseInt(parseFloat(basic_price) * parseFloat($(this).attr("multi"))));
            } else if ($("li.select").length < 1) {
                totalSpan.html(parseInt((parseFloat(basic_price) + (wordCount - basic_word_num) * parseInt(add_text_val))));
            } else {
                /*console.log(
                    parseInt((parseFloat(basic_price)+ (wordCount-220)*70 ) * parseFloat($(this).attr("multi")))                
                );*/
                totalSpan.html(parseInt((parseFloat(basic_price) + (wordCount - basic_word_num) * parseInt(add_text_val)) * parseFloat($(this).attr("multi"))));
            }
        }

    });



    var main_step = 0;

    $("body").on("click", ".next_btn", function () {

        if (main_step == 0) {
            if ($(".request_text textarea").val() == "" && file_name() == "") {
                alert("의뢰문서 또는 텍스트를 채워주세요.");
                return;
            }

            if ($("#original_language option:selected").attr("lang_val") == $(".target_language option:selected").attr("lang_val")) {

                alert("같은 언어로 의뢰하실 수 없습니다.");
                return;
            }

            if(($("#original_language option:selected").val()).indexOf("기타") > -1 || ($(".target_language option:selected").val()).indexOf("기타") > -1 ){

                if($(".etc_lang input").val()==""){

                    alert("기타 언어 선택 시 의뢰할 언어를 적어주세요.");
                    return;
                }
            }

        } else if (main_step == 1) {
            if ($(".step2_btn li.select").length < 1) {
                alert("긴급도를 선택해주세요.");
                return;
            }

            $("select").attr("disabled", false);
        } else if (main_step == 2) {
            return;
        }

        main_step++;
        if (main_step >= 2) {
            main_step = 2;
            $(this).addClass("invisible");
            $(".submit_check").css("display", "block");

        } else {
            $(this).removeClass("invisible");
        }

        if (main_step == 1) {

            var Now = new Date();
            var NowTime = Now.format("yyyy-MM-dd a/p hh:mm");
            /*var NowTime = Now.getFullYear();
            NowTime += ' ' + (Now.getMonth() + 1) ;
            NowTime += ' ' + Now.getDate();
            NowTime += ' ' + Now.getHours();
            NowTime += ':' + Now.getMinutes();*/
            // NowTime += ':' + Now.getSeconds();

            $(".inside_header .now_date").text("( 현재 " + NowTime + " 입니다. )");

            Now.setDate(Now.getDate() + 5);
            NowTime = Now.format("yyyy-MM-dd");

            $(".step2_btn li").eq(0).find(".deadline").text(NowTime + " 24:00까지");

            Now.setDate(Now.getDate() - 3);
            NowTime = Now.format("yyyy-MM-dd");
            $(".step2_btn li").eq(1).find(".deadline").text(NowTime + " 24:00까지");

            Now.setDate(Now.getDate() - 1);
            NowTime = Now.format("yyyy-MM-dd");
            $(".step2_btn li").eq(2).find(".deadline").text(NowTime + " 24:00까지");

        }
        $(".prev_btn").removeClass("invisible");
        $(".main_step").addClass("invisible");
        $(".main_step:eq(" + main_step + ")").removeClass("invisible");

    });

    $("body").on("click", ".prev_btn", function () {
        main_step--;
        var wordCount = parseInt($(".count_word").text());

        if (main_step <= 0) {
            main_step = 0;
            $(this).addClass("invisible");
        } else {
            $(this).removeClass("invisible");
        }
        $(".submit_check").css("display", "none");
        $("#coupon").val("");
        $("#coupon").attr("readonly", false);
        $(".next_btn").removeClass("invisible");
        $(".main_step").addClass("invisible");
        $(".main_step:eq(" + main_step + ")").removeClass("invisible");

        if ($(".step2_btn li.select").length >= 1) {
            $(".step2_btn li.select").removeClass("select");

            if (locationQueryString.type == 2) {
                $(".total span").html(parseInt(parseFloat(basic_price)));
            } else {

                if (wordCount <= basic_word_num) {
                    $(".total span").html(parseInt(parseFloat(basic_price) * 1));
                } else {
                    $(".total span").html(parseInt((parseFloat(basic_price) + (wordCount - basic_word_num) * parseInt(add_text_val))) * 1);
                }
            }
        }
    });

    var file_name = function () {
        var file = $('#request_file');
        var lg = file[0].files.length;
        var items = file[0].files;
        var fileName = "";
        if (lg > 0) {
            for (var i = 0; i < lg; i++) {
                fileName = items[i].name;
                $(".div30_inside .file_name").text(fileName);
                $(".file_delete_btn").css("display", "inline-block");
                $(".request_text textarea").attr("readonly", true);
                $(".file_wrap").removeClass("invisible");
            }
        } else {
            fileName = "";
            $(".file_delete_btn").css("display", "none");
            $(".request_text textarea").attr("readonly", false);
        }
        return fileName;
    }

    $("body").on("click", "button.file_delete_btn", function () {
        $("#request_file").val("");
        $(this).css("display", "none");
        $(".div30_inside .file_name").text("");
        $(".request_text textarea").attr("readonly", false);
        // console.log("삭제");
    });


    $("body").on("change", "#request_file", file_name);

    /*// word count
        $("#text").change(counter);
        $("#text").keydown(counter);
        $("#text").keypress(counter);
        $("#text").keyup(counter);
        $("#text").blur(counter);
        $("#text").focus(counter);
    
        $("body").on("click", ".text_delete", function () {
            TextReset();
        });
    
        $("body").on("click", ".text_copy", function () {
            copyToClipboard($('#text'));
        });*/

    // 스크롤 이벤트
    $(document).on("scroll", function () {
        // console.log(window.pageYOffset);
        if (window.pageYOffset > 80) {
            $(".fixed_header").removeClass("invisible");
            $(".form-header #menu li").removeClass("active");
        } else {
            $(".fixed_header").addClass("invisible");
            $("nav #menu li").removeClass("active");
        }
        if (window.pageYOffset == 0) {
            $(".fixed_header .nav-wrapper").addClass("invisible");
        }

    });
    $(document).scroll();

    // $("body").on("click", ".form-header-logo img", function () {
    //     location.href = "/product?type=" + locationQueryString.type;
    // });

    $("body").on("click", ".fixed_header-logo, .form-header-logo img", function () {
        location.href = "/product?type=" + locationQueryString.type;
    });


    // menu scroll
    $("body").on("click", ".free_request", function (e) {
        e.preventDefault();
        var targetOffset = $(".form-request-inside").offset().top - 75;
        $('html,body').animate({
            scrollTop: targetOffset
        }, 500);
        $(".form-request-inside").effect("highlight", {}, 850);
    });

    $(document).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $(".btn_top").removeClass("invisible");
        } else {
            $(".btn_top").addClass("invisible");
        }
    });
    $(document).scroll();

    // go to top
    $("body").on("click", ".btn_top", function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $("body").on("click", "#coupon_btn", function () {

        var formData = new FormData();
        var coupon = $("#coupon").val();
        var user_email = "ciceron@ciceron.me";
        if (coupon == "") {
            alert("쿠폰 번호를 입력해주세요.");
            return;
        }
        formData.append("coupon", coupon);
        formData.append("user_email", user_email);

        $.ajax({
            url: '/api/v2/user/coupon/check',
            contentType: false,
            processData: false,
            data: formData,
            type: 'post'
        }).done(function (data) {

            // var coupon = $("#coupon").val().trim();
            // var user_email = $("#email").val().trim();
            var price = parseInt($(".total span").text());
            var change_price = "";

            // $.each(data.data, function (k, val) {
            // console.log(val.text);

            change_price = "<sub>" + price + "</sub>" + "<span>" + ((price - data.benefitPoint) > 0 ? (price - data.benefitPoint) : "0") + "</span>";

            // if (val.text == coupon) {

            var r = confirm("쿠폰을 사용하시겠습니까?");

            $(".total span").html("");
            if (r == true) {
                $("#coupon").attr("readonly", true);
                $(".total").html(change_price);
            } else {
                $(".total").text(price);
            }
            // } else 
            // else {

            // }

        }).fail(function (request, status, error) {
            alert("사용할 수 없는 쿠폰입니다.");
            console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
        });

        return false;
    });


    $("body").on("submit", "#form-request", function (e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);

        var request_text = $(".request_text textarea").val();
        /*var original_language = $(".original_language option:checked").text();
        var target_language = $(".target_language option:checked").text();*/
        var original_lang_val = $("#original_language option:selected").val();
        var target_lang_val = $(".target_language option:selected").val();
        var count_word = $(".count_word").text();
        var blank_word = $(".blank_count").text();
        var deadline_date = $(".urgency_rate").text();
        var coupon = $("#coupon").val();
        var total_price = $(".total_price .total span").text();
        var etc_lang_txt = $(".etc_lang input").val();

        var main_text = "단어수",
            sub_text = "단어";

        // for (var i = 3; i <= 6; i++) {
        //     if (i == original_language_val) {
        //         main_text = "글자수";
        //         sub_text = "자";
        //     }
        // }

        if(original_lang_val.indexOf("일본") > -1 || original_lang_val.indexOf("중국") > -1){
            main_text = "글자수";
            sub_text = "자";
        }

        var inform = request_text + "<br /><br/>" + main_text + " : " + count_word + sub_text + " (공백포함: " + blank_word + " " + sub_text + ")" + "<br/><br/>" + "예상금액 : " + total_price + "원";

        if(original_lang_val.indexOf("기타") > -1 || target_lang_val.indexOf("기타") > -1){
            inform += "<br/><br/>의뢰언어 : " + etc_lang_txt;
        }
        // var count_words = count_word + "(공백포함: " + blank_word + " 단어)";

        formData.append("message", inform);
        formData.append("deadline_date", deadline_date);

        if ($("#coupon").attr("readonly") == "readonly") {

            formData.append("coupon", coupon);
        } else if (coupon != "") {
            alert("쿠폰 적용을 눌러주세요.");
            return;
        }

        $.ajax({
            url: '/api/v2/user/sendSimpleRequest',
            data: formData,
            contentType: false,
            processData: false,
            cache: false,
            beforeSend: function () {
                loading(true);
            },
            // dataType: "JSON",
            type: 'POST'
        }).done(function (data) {
            // alert("번역 의뢰 성공");
            // location.reload();
            $(".request_popup_wrap").removeClass("invisible");
            $("body").css("overflow", "hidden");

        }).fail(function (request, status, error) {
            console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
        }).always(function () {
            loading(false);

        });
        return false;
    });

    var prevent_scroll = function (e) {
        e.preventDefault();
        e.stopPropagation();

        return false;
    }

    $("body").on("scroll touchmove mousewheel", ".request_popup_wrap", prevent_scroll);

    $("body").on("click", ".request_popup button", function () {
        location.reload();
    });

    $("body").on("click", "button.text_counter", function (e) {
        e.preventDefault();
        $(".text_counter_container").removeClass("invisible");
        $("body").css("overflow", "hidden");
        $("#text").val("");
        $('#wordCount').html(" " + 0 + " ");
        $('#totalChars').html(" " + 0 + " ");
        $('#charCountNoSpace').html(" " + 0 + " ");
    });

    $("body").on("click", ".text_counter_popup .close", function (e) {
        e.preventDefault();
        var wordCountTrim = $("#wordCount").text().trim();
        var charCountNoSpaceTrim = $("#charCountNoSpace").text().trim();
        $("body").css("overflow", "visible");
        $(".text_counter_container").addClass("invisible");
        count_span = $(".count_span").text().trim();

        var testCount_confirm = confirm(count_span + '를 입력하시겠습니까?');
        if (testCount_confirm) {
            if (count_span != "글자수") {
                $("input[name='num']").val(wordCountTrim);
            } else {
                $("input[name='num']").val(charCountNoSpaceTrim);
            };
        }
        return false;
    });
    $("body").on("click", ".text_counter_popup .text_input", function (e) {
        e.preventDefault();
        var wordCountTrim = $("#wordCount").text().trim();
        var charCountNoSpaceTrim = $("#charCountNoSpace").text().trim();
        $("body").css("overflow", "visible");
        $(".text_counter_container").addClass("invisible");
        count_span = $(".count_span").text().trim();

        var testCount_confirm = confirm(count_span + '를 입력하시겠습니까?');
        if (testCount_confirm) {
            if (count_span != "글자수") {
                $("input[name='num']").val(wordCountTrim);
            } else {
                $("input[name='num']").val(charCountNoSpaceTrim);
            };
        }
        return false;
    });

    $("body").on("keyup", function (e) {
        if ($(".text_counter_container").css("display") == "none") {
            // console.log("esc 방지");
        } else {
            if (e.keyCode == 27) {
                $(".text_counter_popup .close").click();
            }
        }
    });

    var currentTime = new Date();
    var dp_input, dp_inst;
    // currentTime.setDate(currentTime.getDate()+14);
    $(".input-deadline").datepicker({
        // inline: true,
        showOtherMonths: true,
        dateFormat: "yy-mm-dd",
        minDate: currentTime,
        defaultDate: 14,
        beforeShow: function (input, inst) {
            dp_input = input;
            dp_inst = inst;
            setTimeout(function () {
                dp_inst.dpDiv.css({
                    width: $(dp_input).outerWidth()
                });
                // inst.dpDiv.addClass("ll-skin-nigran");
            }, 0);
        },
        onChangeMonthYear: function (year, month, widget) {
            setTimeout(function () {
                dp_inst.dpDiv.css({
                    width: $(dp_input).outerWidth()
                });
                // inst.dpDiv.addClass("ll-skin-nigran");
            }, 0);
        }
    });

    $("#ui-datepicker-div").addClass("ll-skin-nigran");

});

// language function
var option_add = function (arr, one_lang, select_name1, select_name2) {
    if (one_lang) {
        select_name1.append("<option value='" + arr[0] + "' lang_val='0'>" + arr[0] + "</option>");
        select_name2.append("<option value='" + arr[1] + "' lang_val='1'>" + arr[1] + "</option>")
    } else {

        $.each(arr, function (i, val) {
            select_name1.append("<option value='" + arr[i] + "' lang_val='" + i + "'>" + arr[i] + "</option>");
            select_name2.append("<option value='" + arr[i] + "' lang_val='" + i + "'>" + arr[i] + "</option>");
        });

    }

    select_name1.find("option").eq(0).prop("selected", "selected");
    select_name2.find("option").eq(1).prop("selected", "selected");
}

// textarea function
textarea_focus = function (isDiv) {
    var input_file = $(".request_text .file_wrap");
    if ($("#request_file").val() != "") {
        input_file.removeClass("invisible");
    } else {
        input_file.addClass("invisible");
    }
    if (isDiv) {
        $(".request_text textarea").focus();
    }
}
textarea_blur = function () {
    var input_file = $(".request_text .file_wrap");
    input_file.removeClass("invisible");

    if ($(".request_text textarea").val() != "") {
        input_file.addClass("invisible");
        $(".file_name").text("TEXT");
    } else {
        $(".file_name").text("");
    }
}

var price_count = function (basic_p, is_apo) {

    var price2;

    if (is_apo) {

        price2 = 80000;

        return price2;

    } else {

        price2 = basic_p;

        return price2;
    }
};

var word_price = function (price2, basicWordNum, wordnum, multi_val) {

    if (!basicWordNum || wordnum <= basicWordNum) {
        // return price2;
        $(".total span").html(price2);

    } else {
        price2 = price2 + (wordnum - basicWordNum) * parseInt(multi_val);
        // return price2;
        $(".total span").html(price2);

    }
}


counter = function () {
    var value = $(".request_text textarea").val();
    var total_span = $(".total span");
    var total_span_int = parseInt(total_span);

    var regex = /\s+/gi;
    var regex2 = value.match(/\S+/g);
    var wordCountVal = value.split("");
    var space_num = 0;

    if (value.length <= 0) {
        $(".blank_count, .blank_text").html(0);
        $('.count_word, .count_text').html(" " + 0 + " ");
        return;
    }

    $.each(wordCountVal, function (index, val) {
        if (val == " ") {
            space_num++;
        }
    });

    var wordCount = value.replace(regex, " ").split(" ").length;
    // var wordCountNoSpace = value.trim().replace(regex, " ").split(" ").length;
    var wordCountNoSpace = regex2 ? regex2.length : 0;
    var totalChars = value.replace(/\./g, "").length;
    var charCountNoSpace = value.replace(regex, "").replace(/\./g, "").length;

    $(".blank_count").html(wordCountNoSpace + space_num);
    $(".count_word").html(" " + wordCountNoSpace + " ");

    $(".blank_text").html(value.length);
    $(".count_text").html(charCountNoSpace);

    if (locationQueryString.type == 2) {
        // total_span.html(word_price(price_count(basic_price, true)), false, wordCountNoSpace, add_text_val);
        word_price(price_count(basic_price, true), false, wordCountNoSpace, add_text_val);
    } else {
        // total_span.html(word_price(price_count(basic_price, false)), basic_word_num, wordCountNoSpace, add_text_val);
        word_price(price_count(basic_price, false), basic_word_num, wordCountNoSpace, add_text_val);
    }

    /*$("#count_word").html(" " + totalChars + " ");
    $("#charCountNoSpace").html(" " + charCountNoSpace + " ");*/
};

var text_area = $(".request_text textarea");

$(text_area).change(counter);
$(text_area).keydown(counter);
$(text_area).keypress(counter);
$(text_area).keyup(counter);
$(text_area).blur(counter);
$(text_area).focus(counter);

var loading = function (isStart) {
    if (isStart == false) {
        $(".global_loading").remove();
    } else {
        $('body').prepend('<div class="global_loading">' +
            '<div class="mask-loading">' +
            '<div class="spinner">' +
            '<div class="double-bounce1"></div>' +
            '<div class="double-bounce2"></div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );
    }
}

// scroll menu event
onScroll = function (event) {
    var scrollPos = $(document).scrollTop();
    $('#menu a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        /*console.log("top: " + refElement.position().top);
        console.log("scroll: " + scrollPos);*/
        if (refElement.position().top - 451 <= scrollPos && refElement.position().top + refElement.height() - 351 > scrollPos) {
            $('#menu ul li a').removeClass("active");
            currLink.addClass("active");
        } else {
            currLink.removeClass("active");
        }
    });
}

if_mobile = function () {
    var windowWidth = $(window).width();
    if (windowWidth <= 640) {
        //창 가로 크기가 640 미만일 경우         
        // $(".nav-wrapper").addClass("invisible");
        // otherClick();
    } else {
        // $(".nav-wrapper").removeClass("invisible");
    }
}

otherClick = function () {
    $("body").on("click", function (e) {
        // alert("fdsa");
        var $container = $(".fixed_header, .form-header, .nav .next");
        // console.log(!$container.is(e.target));
        // console.log($container.has(e.target).length);

        // if the target of the click isn't the container nor a descendant of the container
        // console.log("fdsa");
        if (!$container.is(e.target) && $container.has(e.target).length === 0) {
            $(".nav-wrapper").addClass("invisible");
            // $(".form-header .nav-wrapper").addClass("invisible");
        } else {
            // $(".nav-wrapper").removeClass("invisible");
        }
    });
}

Date.prototype.format = function (f) {
    if (!this.valueOf()) return " ";

    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;

    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function ($1) {
        switch ($1) {
            case "yyyy":
                return d.getFullYear();
            case "yy":
                return (d.getFullYear() % 1000).zf(2);
            case "MM":
                return (d.getMonth() + 1).zf(2);
            case "dd":
                return d.getDate().zf(2);
            case "E":
                return weekName[d.getDay()];
            case "HH":
                return d.getHours().zf(2);
            case "hh":
                return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm":
                return d.getMinutes().zf(2);
            case "ss":
                return d.getSeconds().zf(2);
            case "a/p":
                return d.getHours() < 12 ? "오전" : "오후";
            default:
                return $1;
        }
    });
};

String.prototype.string = function (len) {
    var s = '',
        i = 0;
    while (i++ < len) {
        s += this;
    }
    return s;
};
String.prototype.zf = function (len) {
    return "0".string(len - this.length) + this;
};
Number.prototype.zf = function (len) {
    return this.toString().zf(len);
};