var fileList = [];
var previewTextList = [];
var requestComplete = false;


$(document).ready(function(){

     function isValidURL(s) {    
        var regexp = /(((ftp|http|https):\/\/)|www)(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(s);    
     }

    // alert(document.defaultCharset);
    // alert(escapeHtml("<br />").length)

    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    
    $('form').on('click mousedown keyup', function(){
        window.onbeforeunload = function () {
        //   alert('You are trying to leave.');
            if(!requestComplete){
                return false;
            }
        }
        $('form').off('click mousedown keyup');
    });
    
    
    for( i = 0; i < 15; i++){
        $("#selectFormat").append('<option value="' + i + '" data-imagesrc="/static/img/format/' + i +'.png" data-description="">' + i18n.getI18n('format_' + i) + '</option>');
    }
    
    for(i = 0; i < 12; i++){
        $("#selectField").append('<option value="' + i + '" data-imagesrc="/static/img/subject/' + i +'.png" data-description="">' + i18n.getI18n('subject_' + i) + '</option>');
    }
    
    var currentTime = new Date();
    currentTime.setDate(currentTime.getDate()+14);
    $( ".datepicker" ).datepicker({
        inline: true,
        showOtherMonths: true,
        minDate: currentTime
    });
    
    var $preScrollHeight = 0;
    $("#request_text").on('keyup keydown change',function(){

        if($("input[name=request_originalLang]").val()=='0'||$("input[name=request_targetLang]").val()=='0'){
            $("#selectLanguage").helper(i18n.getI18n("request_please_select_the_language"));
            return;
        }

        if(isValidURL($(this).val())) {
            $('.isUrl').show();
        }
        else{
            $('.isUrl').hide();
        }
        
        var $nowScroll = $(document).scrollTop();
        $(this).css('height', 'auto' );
        $(this).height( this.scrollHeight );
        $(document).scrollTop($nowScroll + this.scrollHeight - $preScrollHeight);
        $preScrollHeight = this.scrollHeight;
        //$(this).val().length
        $(".characterCount").text(i18n.getI18n("request_letters",{request_words: $(this).val().length}));
        // setPrice();
    });
    
    $("#request_text").keydown();

    $("#priceBar").on('input change', function(){
        $("#nowPrice").val((parseFloat($(this).val())).toFixed(2));
        $("#localPrice").text(changeMoney($("#nowPrice").val()));
        // setPrice();
    });
    
    $("#nowPrice").on('input change', function(){
        $("#priceBar").val((parseFloat($(this).val())).toFixed(2));
        $("#localPrice").text(changeMoney($("#nowPrice").val()));
        // setPrice();
    });
    
    
    $('#selectLanguageFrom').ddslick({
        width: 120,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    $('#selectLanguageTo').ddslick({
        width: 120,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    
    $('#selectFormat').ddslick({
        height: 300,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });
    
    $('#selectField').ddslick({
        height: 300,
        onSelected: function(selectedData){
            //callback function: do something with selectedData;
        }   
    });

    $('#request_i18nFileFormat').ddslick({
        onSelected: function(selectedData){
            if(selectedData.selectedIndex == 3 || selectedData.selectedIndex == 4){
                $("#request_i18nLangKey").attr("disabled", false);
                
            }
            else{
                $("#request_i18nLangKey").attr("disabled", true);
            }
        }
    })

    $('#request_i18nFileFormat, #request_i18nFileFormat .dd-select').css('width', '100%');
    
    $(".content_select").click(function(){
        $(".content_select").removeClass("selected");
        $(this).addClass("selected");
        
        $(".request_isType").attr("value", "0");
        $("#request_is" + $(this).attr("value")).attr("value", "1");
        $(".content_Type").hide();
        $("#content_" + $(this).attr("value")).show();
        
        if($(this).attr("value") == "I18n"){
            $(".hideWhenI18n").hide();
        }
        else{
            $(".hideWhenI18n").show();
        }
        // setPrice();
    });


    var showDrag = false;
    var timeout = -1;
    var showDrag2 = false;
    var timeout2 = -1;

    $('.file_add').on('dragover dragenter', function(e){
        e.preventDefault();
        showDrag2 = true; 
        $('.file_add').addClass('hover');
    });

    $('.file_add').on('dragleave dragend', function(){
        
        showDrag2 = false; 
        clearTimeout( timeout2 );
        timeout2 = setTimeout( function(){
            if( !showDrag2 ){ $('.file_add').removeClass('hover'); }
        }, 70 );
    });

    $('.file_add').on('drop', function(e){
        e.stopPropagation();
        e.preventDefault();
        $('.file_add').removeClass('drag');
        $('.file_add').removeClass('hover');

        var dt = e.dataTransfer || (e.originalEvent && e.originalEvent.dataTransfer);
        var files = e.target.files || (dt && dt.files);
        addFiles(files);
        // alert(e.dataTransfer.files[0].name);
        // console.log(e.dataTransfer.files[0]);
        return false;
    });

    $(this).on('dragover dragenter', function(e){
        e.preventDefault();
        showDrag = true; 
        $('.file_add').addClass('drag');
        
    });

    $(this).on('dragleave dragend', function(){
        
        showDrag = false; 
        clearTimeout( timeout );
        timeout = setTimeout( function(){
            if( !showDrag ){ $('.file_add').removeClass('drag'); }
        }, 70 );
    });

    $(this).on('drop', function(e){
        e.preventDefault();
        $('.file_add').removeClass('drag');
        $('.file_add').removeClass('hover');
        return false;
    });



    $("#request_file").bind('change',function(e){
        addFiles(this.files);
    });


    function previewImage(file, fileDiv){

        var $previewText = "";

        $(fileDiv).find('.file_preview').text("");
        $(fileDiv).find('.file_preview').append('<canvas class="canvas" style="width: 100%; height: auto;"></canvas><span class="preview_text"></span>');
        var c = $(fileDiv).find('.file_preview .canvas').get(0);
        var ctx = c.getContext("2d");
        c.width = c.width;
        
        var reader = new FileReader();
        reader.onload = function (e) {

            // $('.content_photo_preview').attr('src', e.target.result);
            
            var img = new Image();
            img.src = e.target.result;
            
            var formData = new FormData();
            formData.append("data",file);
            $.ajax({
                url: 'https://api.projectoxford.ai/vision/v1.0/ocr?language=unk&detectOrientation=true',
                beforeSend: function (request)
                {
                    request.setRequestHeader("Ocp-Apim-Subscription-Key", "550f774dcb774819884b4fafdd3f6683");
                },
                data: formData,
                processData: false,
                contentType: false,
                dataType: "JSON",
                type: 'POST',
                error: function(xhr, ajaxOptions, thrownError){
                    // if (xhr.status == 417) {
                    //     alert("올바르지 않은 이메일 주소입니다.");
                    // }
                    // else if(xhr.status == 412){
                    //     alert("이미 존재하는 이메일 주소입니다.");
                    // }
                    var data = JSON.parse(xhr.responseText);
                    alert("사진 분석을 실패하였습니다.\n" + data.message);
                }
            }).done(function(data){
                // $previewText = "";
            
                c.width = img.width;
                c.height = img.height;
                
                
                ctx.drawImage(img, 0,0);
                // ctx.rotate(-(data.textAngle * Math.PI / 180));
                ctx.translate(img.width / 2 , img.height / 2);
                ctx.rotate((data.textAngle * Math.PI / 180));
                ctx.translate(-img.width / 2 , -img.height / 2);
                
                // ctx.translate(img.width / 2, img.height / 2);
                
                // ctx.translate(-img.width / 2 , -img.height / 2);
                // ctx.translate(-img.width / 2 , -img.height / 2);
                
                $(data.regions).each(function(i, regions){
                    $(regions.lines).each(function(j, line){
                        $(line.words).each(function(k, word){
                            $previewText = $previewText + " " + word.text;
                            
                            $box = word.boundingBox.split(',');
                            // ctx.lineWidth="6";
                            ctx.fillStyle="red";
                            ctx.globalAlpha=0.5;
                            ctx.fillRect($box[0],$box[1],$box[2],$box[3]);
                        });
                        $previewText = $previewText + '\n';
                        
                    });
                }); 
                // ctx.rotate((data.textAngle * Math.PI / 180));
                ctx.stroke();
                
                $previewText = $.trim($previewText);
                previewTextList[fileList.indexOf(file)] = $previewText;
                
                //$(".content_photo_preview_text").text($previewText);
                $(fileDiv).find('.file_preview .preview_text').html("분석 결과 약 " + $previewText.length + " 글자(공백 포함)로 파악되었습니다.<br />만약 올바르지 않다고 판단된다면 직접 가격을 설정할 수 있습니다.<br />사진이 작거나 화질이 좋지 않을 경우, 손글씨일 경우, 글이 여러 방향에서 써진 경우 분석이 어렵습니다. 문서를 스캔한 파일의 경우 최상의 결과를 얻을 수 있습니다.");
                if($previewText == "")$(fileDiv).find('.file_preview .preview_text').html($(".content_photo_preview_text").html() + "<br />텍스트가 발견되지 않았습니다.");
                // $(".characterCount").text("약 " + $previewText.length + " 글자");
                // // setPrice($previewText);
                // $(".loading").hide();
                
            });
            
            // $("#frmUser_profilePic").submit();
        }

        reader.readAsDataURL(file);
    }

    function previewEtc(file, fileDiv){

        // alert(fileList.indexOf(file));
        var $previewText = "";

        $(fileDiv).find('.file_preview').text("");
        $(fileDiv).find('.file_preview').append('<span class="preview_text"></span>');
        var encoding = "";
        var reader = new FileReader();
            reader.onload = function (e) {
                // alert(JSON.stringify(jschardet.detect(e.target.result)));
                // alert(decodeURIComponent(encodeURIComponent(e.target.result)))
                if(encoding == "" && jschardet.detect(e.target.result).encoding != "UTF-8"){
                    encoding = jschardet.detect(e.target.result).encoding;
                    reader.readAsText(file, settings['lang_'+$("input[name=request_originalLang]").val()+'_charset']);
                    $previewText = e.target.result;
                    previewTextList[fileList.indexOf(file)] = $previewText;
                    // var rr = new TextStreamReader();
                    // alert(encoding);
                    return;
                }
                $(fileDiv).find('.file_preview .preview_text').html('<pre>' + escapeHtml(e.target.result) + '</pre>');
            };
        reader.readAsText(file);
    }

    function previewJson(file, fileDiv){
    

        $(fileDiv).find('.file_preview').text("");
        $(fileDiv).find('.file_preview').append('<span class="preview_text"></span>');
        
        var $previewText = "";

        var reader = new FileReader();
            reader.onload = function(e){
                var data = JSON.parse(e.target.result);
                $previewText = "";
                
                // var splitByLine = (JSON.stringify(data, null, 4)).split('\n');
                // $.each(splitByLine, function(i, item){
                //     var s = item.split(': "');
                //     if(s.length > 1){
                //         var t = s[1].split('"');
                //         var u = "";
                        
                //         $.each(t, function(j, item2){
                //             if(j != t.length - 1){
                //                 u = u + item2 + '"';
                //             }
                //         });

                //         u.slice(0, -1);
                //         alert(u);
                //     }
                // });

                $(fileDiv).find('.file_preview .preview_text').html('<pre><code class="json">' + JSON.stringify(data, null, 4) + '</code></pre>');
                //<code class="html">
                hljs.highlightBlock($(fileDiv).find('.file_preview .preview_text code').get(0));
                $.each($(fileDiv).find('.file_preview .preview_text code .hljs-string'), function(i, item){
                    $previewText = $previewText + $(item).text() + " ";
                    // alert($(item).text());
                });
                previewTextList[fileList.indexOf(file)] = $.trim($previewText);
            };
        reader.readAsText(file);
    }
    
    function previewXml(file, fileDiv){
    

        $(fileDiv).find('.file_preview').text("");
        $(fileDiv).find('.file_preview').append('<span class="preview_text"></span>');
        
        var $previewText = "";

        var reader = new FileReader();
            reader.onload = function(e){
                var data = e.target.result;
                $previewText = e.target.result;
                $(fileDiv).find('.file_preview .preview_text').html('<pre><code class="xml">' + escapeHtml(data) + '</code></pre>');
                //<code class="html">
                hljs.highlightBlock($(fileDiv).find('.file_preview .preview_text code').get(0));
                previewTextList[fileList.indexOf(file)] = $previewText;
            };
        reader.readAsText(file);
    }

    function previewDocx(file, fileDiv){

    }


    function addFiles(files){
        
        if($("input[name=request_originalLang]").val()=='0'){
            $("#selectLanguage").helper(i18n.getI18n("request_please_select_the_language"));
            return;
        }

        for(var i = 0; i < files.length; i++){
            
            var fIndex = fileList.push(files[i]);
            previewTextList.push('');
            fIndex -= 1;
            $(".file_list").append('<div class="file">'+
                                        '<img src="/static/img/ICONS_CICERON-03.png" class="file_icon" />'+
                                        '<span class="file_name" title="'+files[i].name+'(' +files[i].type+')">'+files[i].name+'</span>'+
                                        '<span class="file_delete"><i class="icon ion-ios-minus-outline fa-2x"></i></span>'+
                                        '<div class="file_preview">미리보기 없음</div>'+
                                    '</div>');

            var fDiv = $(".file_list .file")[fIndex];

            $(".file_delete").off('click').on('click', function(){
                fileList.splice($(this).parent().index(), 1);
                previewTextList.splice($(this).parent().index(), 1);
                $(this).parent().remove();
            });

            if(files[i].type.toLowerCase().lastIndexOf('image', 0)===0){
                previewImage(files[i], fDiv);
            }
            else if(files[i].name.split('.').pop().toLowerCase() == 'json'){
                previewJson(files[i], fDiv);
            }
            else if(files[i].name.split('.').pop().toLowerCase() == 'xml'){
                previewXml(files[i], fDiv);
            }
            else if(!files[i].type || files[i].type == '' || files[i].type.toLowerCase().lastIndexOf('text', 0)===0){
                previewEtc(files[i], fDiv);
            }
            
            // setPrice();
        }
    }

    
    $("#request_i18nFileBinary").bind('change', function(e) {
        analyzeI18n(this.files[0]);
    })

    function analyzeI18n($file){
        $(".loading").show();
        var formData = new FormData();
        formData.append("file_binary",$file, "temp");
        formData.append("file_format", $("input[name=request_i18nFileFormat]").val())
        formData.append("file_langKey", $("#request_i18nLangKey").val())
        $.ajax({
            url: '/api/tool/i18n_test_parsing',
            data: formData,
            processData: false,
            contentType: false,
            dataType: "JSON",
            type: 'POST',
            error: function(xhr, ajaxOptions, thrownError){
                // if (xhr.status == 417) {
                //     alert("올바르지 않은 이메일 주소입니다.");
                // }
                // else if(xhr.status == 412){
                //     alert("이미 존재하는 이메일 주소입니다.");
                // }
                // var data = JSON.parse(xhr.responseText);
                // alert(data.message);
                alert("File analysis failed.\nPlease make sure you have selected the correct platform and language key.");
                $(".loading").hide();
            }
        }).done(function(data){
            console.log(data);
            data = data.data;
            $(".file_i18n").empty();
            $.each(data, function(key, value){
                $(".file_i18n").append(
                    '<div class="file_i18n_line">'
                    + '<div class="file_i18n_line_name">' + key + '</div>'
                    + '<div class="file_i18n_line_content">' + value + '</div>'
                    + '</div>'
                );
                
            });
            $(".loading").hide();
            
        });

        // $(".loading").hide();

    }


    $("#request_photo").bind('change',function(e){
        // for(var i = 0; i < this.files.length; i++){
        //     fileList.push(this.files[i]);
        // }


        if (this.files && this.files[0]) {
            
            $(".loading").show();
            
            var c = document.getElementById('canvas_test');
            var ctx = c.getContext("2d");
            c.width = c.width;
            
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                
                // $('.content_photo_preview').attr('src', e.target.result);
                
                var img = new Image();
                img.src = e.target.result;
                
                
                
                
                var formData = new FormData();
                formData.append("data",file);
                $.ajax({
                    url: 'https://api.projectoxford.ai/vision/v1.0/ocr?language=unk&detectOrientation=true',
                    beforeSend: function (request)
                    {
                        request.setRequestHeader("Ocp-Apim-Subscription-Key", "550f774dcb774819884b4fafdd3f6683");
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "JSON",
                    type: 'POST',
                    error: function(xhr, ajaxOptions, thrownError){
                        // if (xhr.status == 417) {
                        //     alert("올바르지 않은 이메일 주소입니다.");
                        // }
                        // else if(xhr.status == 412){
                        //     alert("이미 존재하는 이메일 주소입니다.");
                        // }
                        var data = JSON.parse(xhr.responseText);
                        alert("사진 분석을 실패하였습니다.\n" + data.message);
                    }
                }).done(function(data){
                    
                    $previewText = "";
                
                    c.width = img.width;
                    c.height = img.height;
                    
                    
                    ctx.drawImage(img, 0,0);
                    // ctx.rotate(-(data.textAngle * Math.PI / 180));
                    ctx.translate(img.width / 2 , img.height / 2);
                    ctx.rotate((data.textAngle * Math.PI / 180));
                    ctx.translate(-img.width / 2 , -img.height / 2);
                    
                    // ctx.translate(img.width / 2, img.height / 2);
                    
                    
                    
                    // ctx.translate(-img.width / 2 , -img.height / 2);
                    // ctx.translate(-img.width / 2 , -img.height / 2);
                    
                    $(data.regions).each(function(i, regions){
                        $(regions.lines).each(function(j, line){
                            $(line.words).each(function(k, word){
                                $previewText = $previewText + " " + word.text;
                                
                                $box = word.boundingBox.split(',');
                                // ctx.lineWidth="6";
                                ctx.fillStyle="red";
                                ctx.globalAlpha=0.5;
                                ctx.fillRect($box[0],$box[1],$box[2],$box[3]);
                            });
                            $previewText = $previewText + '\n';
                            
                        });
                    }); 
                    // ctx.rotate((data.textAngle * Math.PI / 180));
                    ctx.stroke();
                    
                    $previewText = $.trim($previewText);
                    
                    
                    //$(".content_photo_preview_text").text($previewText);
                    $(".content_photo_preview_text").html("분석 결과 약 " + $previewText.length + " 글자(공백 포함)로 파악되었습니다.<br />만약 올바르지 않다고 판단된다면 직접 가격을 설정할 수 있습니다.<br />사진이 작거나 화질이 좋지 않을 경우, 손글씨일 경우, 글이 여러 방향에서 써진 경우 분석이 어렵습니다. 문서를 스캔한 파일의 경우 최상의 결과를 얻을 수 있습니다.");
                    if($previewText == "")$(".content_photo_preview_text").html($(".content_photo_preview_text").html() + "<br />텍스트가 발견되지 않았습니다.");
                    $(".characterCount").text("약 " + $previewText.length + " 글자");
                    // setPrice($previewText);
                    $(".loading").hide();
                    
                });
                
                // $("#frmUser_profilePic").submit();
            }

            reader.readAsDataURL(this.files[0]);
        }
        
        
    });
    
    $('.show_setPrice').click(function(){
        
        if($("input[name=request_originalLang]").val()=='0'||$("input[name=request_targetLang]").val()=='0'){
            $("#selectLanguage").helper(i18n.getI18n("request_please_select_the_language"));
            return;
        }
        if($("#context_textArea").val().length<10){
            $("#context_textArea").helper(i18n.getI18n("request_please_write_context"));
            // alert($("#context_textArea").zIndex());
            return;
        }
        
        if(!$userInfomation.user_isTranslator){
            setPrice();
        }

        $('#popup_setPrice').show();
    });

    $('#popup_setPrice .dialog_close').click(function(){
        $('#popup_setPrice').hide();
    });

    $("#frmRequest").submit(function(e){
        e.preventDefault();
        startLoading();


        $('#popup_setPrice').hide();
        


        $("#request_clientId").val($userInfomation.user_email);
        var nowDate = new Date();
        $("#request_deltaFromDue").val(parseInt(($.datepicker.formatDate( "@", $("#deadlineDate").datepicker('getDate')) - nowDate.getTime())/1000) + 86400);
        
        

        if($('#request_isText').val() == '0'){
            $('#request_text').removeAttr('name');
        }
        if($('#request_isPhoto').val() == '0'){
            $('#request_photo').removeAttr('name');
        }


        var form = $(this)[0];
        var formData = new FormData(form);




        if($('#request_isFile').val() == '1'){
            zipModel.addFiles(fileList, function() {
            }, function(file) {
                //압축 중
                // var li = document.createElement("li");
                // zipProgress.value = 0;
                // zipProgress.max = 0;
                // li.textContent = file.name;
                // li.appendChild(zipProgress);
                // fileList.appendChild(li);
            }, function(current, total) {
                //압축 중
                // zipProgress.value = current;
                // zipProgress.max = total;
            }, function() {
                //압축 끝
                // if (zipProgress.parentNode)
                //     zipProgress.parentNode.removeChild(zipProgress);
                // fileInput.value = "";
                // fileInput.disabled = false;
                zipModel.getBlob(function(blob) {
                    formData.append('request_file', blob, "temp.zip");
                    popupPayment(formData);
                });
            });
        }
        else{
            popupPayment(formData);
            
        }
         
    });

    function popupPayment(formData){
        
        $.ajax({
            url: '/api/requests',
            data: formData,
            processData: false,
            contentType: false,
            dataType: "JSON",
            type: 'POST',
            error: function(xhr, ajaxOptions, thrownError){
                // if (xhr.status == 417) {
                //     alert("올바르지 않은 이메일 주소입니다.");
                // }
                // else if(xhr.status == 412){
                //     alert("이미 존재하는 이메일 주소입니다.");
                // }
                endLoading();
                alert("의뢰 실패");
            }
        }).done(function(data){
            endLoading();
            
            $("body").prepend('<div class="popup popup_payment"> <div class="popup_background"></div>' +
            '<div class="payment">' +
            '<div class="div_1-2" style="height: 100%; position: relative; overflow-y: visible;"> ' +
            '<div class="div_1-2_1" style="position: relative;"> ' +
            '<div id="payment_select">' +
            '<div class="payment_select" value="0"> ' +
            '<span i18n="request_payment_useCoupon">쿠폰 사용</span>' +
            '<span class="payment_select_now" id="payment_select_coupon">0</span> ' +
            '</div> ' +
            '<div class="payment_select selected" value="1">' +
            '<span i18n="request_payment_usePoint">포인트 사용</span>' +
            '<span class="payment_select_now" id="payment_select_point">0</span> ' +
            '</div>' +
            '<div class="payment_select" value="2"> ' +
            '<span i18n="request_payment_choosePaymentPlatform">결제수단 선택</span>' +
            '<span class="payment_select_now" id="payment_select_payment">-</span>' +
            '</div> ' +
            '<div class="payment_sum"> ' +
            '<span i18n="request_payment_requestPrice">의뢰 금액</span>($<span id="payment_sum_price"></span>) - <span i18n="request_payment_couponPrice">쿠폰</span>($<span id="payment_sum_coupon"></span>) - <span i18n="request_payment_pointPrice">포인트</span>($<span id="payment_sum_point"></span>)<br /> ' +
            '<span class="payment_select_now" style="font-weight: bold;"><span i18n="request_payment_finalPrice">결제 금액</span>: $<span id="payment_sum_amount"></span></span> ' +
            '</div>' +
            '</div>' +
            '</div> ' +
            '<div class="div_1-2_2" style="position: relative;">' + 
            '<div id="payment_tab"> ' +
            '<div class="payment_tab" id="payment_tab0">' +
            '<span class="payment_tab_title" i18n="request_payment_writeCoupon"> 쿠폰번호 입력: </span>' +
            '<div class="payment_tab_content"> ' +
            '<input type="text" placeholder="none" id="payment_usingCoupon"/> </div>' +
            '</div>' +
            '<div class="payment_tab selected" id="payment_tab1">' +
            '<span class="payment_tab_title"> <span i18n="request_payment_myPoint">현재 내 포인트:</span> <span id="payment_myPoint"></span> </span>' +
            '<br /> ' +
            '<span class="payment_tab_title" i18n="request_payment_usingPoint"> 사용할 포인트: </span>' +
            '<div class="payment_tab_content">' +
            '<input type="number" step="0.01" min="0" max="100" value="0" id="payment_usingPoint"/> ' +
            '</div>' +
            '</div> ' +
            '<div class="payment_tab" id="payment_tab2"> ' +
            '<span class="payment_tab_title" i18n="request_payment_choosePaymentPlatformAsk"> 결제 수단을 선택해주세요. </span> ' +
            '<div class="payment_tab_content">' +
            '<div class="payment_buttons"> ' +
            '<button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_paypal">' +
            '<img src="/static/img/payment/paypal.png" style="width: 150px;"/>' +
            '</button>' +
            '<button type="button" style="border: 1px solid #ddd; display: none;" class="payment_button" id="pay_alipay">' +
            '<img src="/static/img/payment/alipay.png" style="width: 150px;"/>' +
            '</button> ' +
            '</div> ' +
            '<span i18n="request_payment_choosePaymentPlatformOr"></span>' +
            '<div class="payment_buttons"> ' +
            '<button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_iamport" i18n="request_payment_CreditCard">' +
            '카드번호 입력' +
            '</button>' +
            ' </div> ' +
            '<div class="payment_tab_content" id="payment_iamport" style="display: none;"> ' +
            '카드번호:' +
            '<input type="text" maxlength="4" size="4" id="payment_card1">' +
            '- <input type="text" maxlength="4" size="4" id="payment_card2">' +
            '- <input type="password" maxlength="4" size="4" id="payment_card3">' +
            '- <input type="password" maxlength="4" size="4" id="payment_card4"> ' +
            '<br /> ' +
            '유효기간: ' +
            '<input type="text" maxlength="2" size="2" id="payment_month" placeholder="00">' +
            '월 ' +
            '<input type="text" maxlength="4" size="4" id="payment_year"> placeholder="0000"' +
            '년' +
            '<br /> ' +
            '생년월일(6글자) 또는 사업자등록번호(10글자): ' +
            '<input type="text" maxlength="10" size="10" id="payment_birth" placeholder="000000">' +
            '<br />' +
            '비밀번호 앞 두 자리:' +
            '<input type="password" maxlength="2" size="2" id="payment_pwd">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<button class="payment_submit" value=""> 적용하기 </button>' +
            '<button class="payment_close" i18n="request_payment_cancel"> 취소 </button> ' +
            '</div> ' +
            '</div>');


            $("#payment_sum_coupon").text("0");
            $("#payment_sum_point").text("0");
            $("#payment_sum_amount").text($("#nowPrice").val());
            
            $(".payment_submit").attr("value", data.request_id);
            
            $("#payment_sum_price").text($("#nowPrice").val());
            
            $("#payment_myPoint").text($userInfomation.user_point);
            
            if(parseFloat($("#nowPrice").val()) < parseFloat($userInfomation.user_point)){
                $("#payment_usingPoint").attr("max",$("#nowPrice").val());
                
            }else{
                $("#payment_usingPoint").attr("max",$userInfomation.user_point);
            }
            
            $("#payment_usingPoint").change(function(){
                if(parseFloat( $("#payment_usingPoint").val()) > parseFloat($("#payment_usingPoint").attr("max"))){
                    $("#payment_usingPoint").val( parseFloat($("#payment_usingPoint").attr("max")));
                }
            });
            
            $("#payment_usingCoupon").change(function(){
                $("#payment_sum_coupon").text("0");
                $("#payment_select_coupon").text("0");
            })
            
            $(".payment_submit").click(function(){
                if($(".payment_select.selected").attr("value") == "0"){
                    if($("#payment_usingCoupon").val() == ""){
                        
                    }
                    else{
                    }
                    var f = new FormData();
                    f.append("promoCode", $("#payment_usingCoupon").val());
                    startLoading();
                    $.ajax({
                        url: '/api/user/requests/'+$(this).attr("value")+'/payment/checkPromoCode',
                        data: f,
                        processData: false,
                        contentType: false,
                        dataType: "JSON",
                        type: 'POST',
                        error: function(xhr, ajaxOptions, thrownError){
                            endLoading();
                            // if (xhr.status == 417) {
                            //     alert("올바르지 않은 이메일 주소입니다.");
                            // }
                            // else if(xhr.status == 412){
                            //     alert("이미 존재하는 이메일 주소입니다.");
                            // }
                            alert("Invalid coupon.");
                            $("#payment_sum_coupon").text("0");
                            $("#payment_select_coupon").text("0");
                            $("#payment_sum_amount").text(parseFloat($("#payment_sum_price").text())-parseFloat($("#payment_sum_coupon").text())-parseFloat($("#payment_sum_point").text()));
                            if(parseFloat($("#payment_sum_amount").text()) <= 0){
                                $("#payment_select_payment").text(i18n.getI18n("request_payment_onlyPoint"));
                                $("#payment_sum_amount").text(0);
                            }

                        }
                    }).done(function(data){
                        endLoading();
                        $("#payment_sum_coupon").text(data.point);
                        $("#payment_select_coupon").text(data.point);
                        
                        $("#payment_sum_amount").text(parseFloat($("#payment_sum_price").text())-parseFloat($("#payment_sum_coupon").text())-parseFloat($("#payment_sum_point").text()));
                        if(parseFloat($("#payment_sum_amount").text()) <= 0){
                            $("#payment_select_payment").text(i18n.getI18n("request_payment_onlyPoint"));
                            $("#payment_sum_amount").text(0);
                        }

                    });


                }else if($(".payment_select.selected").attr("value") == "1"){
                    $("#payment_select_point").text($("#payment_usingPoint").val());
                    $("#payment_sum_point").text($("#payment_usingPoint").val());
                    
                }else if($(".payment_select.selected").attr("value") == "2"){
                    //$(".payment_button.selected")
                    
                    if(parseFloat($("#payment_sum_price").text()) < 2.5 && parseFloat($("#payment_sum_price").text()) > 0){
                        alert(i18n.getI18n("request_payment_sorry"));
                        return;
                    }

                    formData = new FormData();
                    
                    
                    if($("#payment_select_point").text() == "-") {
                        alert("사용할 포인트를 먼저 적용해주세요.");
                        return;
                    }
                    
                    if($("#payment_select_payment").text() == "-") {
                        alert(i18n.getI18n("request_payment_error_platform"));
                        return;
                    }
                    else if($("#payment_select_payment").text() == "PayPal"){
                        formData.append("payment_platform", "paypal");             
                    }
                    else if($("#payment_select_payment").text() == "Alipay"){
                        formData.append("payment_platform", "alipay");             
                    }
                    else if($("#payment_select_payment").text() == i18n.getI18n("request_payment_CreditCard")){
                        formData.append("payment_platform", "iamport");
                        formData.append("card_number", $("#payment_card1").val() + "-" + $("#payment_card2").val() + "-" + $("#payment_card3").val() + "-" + $("#payment_card4").val());
                        formData.append("expiry", $("#payment_year").val() + "-" + $("#payment_month").val());
                        formData.append("birth", $("#payment_birth").val());
                        formData.append("pwd_2digit", $("#payment_pwd").val());
                    }
                    else if($("#payment_select_payment").text() == i18n.getI18n("request_payment_onlyPoint")){
                        formData.append("payment_platform", "point_only");             
                    }
                    
                    formData.append("payment_amount", parseFloat($("#payment_sum_price").text()));             
                    // formData.append("payment_amount", parseFloat($("#payment_sum_price").text()) / parseFloat(i18n.getI18n('payment_rate')));             
                    formData.append("point_for_use", parseFloat($("#payment_select_point").text()));
                    // formData.append("point_for_use", parseFloat($("#payment_select_point").text()) / parseFloat(i18n.getI18n('payment_rate')));
                    formData.append("pay_by", "web");

                    formData.append("promo_code", $("#payment_usingCoupon").val());
                    
            
                    startLoading();
                    $.ajax({
                        url: '/api/user/requests/'+$(this).attr("value")+'/payment/start',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: "JSON",
                        type: 'POST',
                        error: function(xhr, ajaxOptions, thrownError){
                            // if (xhr.status == 417) {
                            //     alert("올바르지 않은 이메일 주소입니다.");
                            // }
                            // else if(xhr.status == 412){
                            //     alert("이미 존재하는 이메일 주소입니다.");
                            // }
                            endLoading();
                            alert("Something went wrong");
                        }
                    }).done(function(data){
                        endLoading();
                        requestComplete = true;
                        location.href = data.link;
                    });
                }
                
                $("#payment_sum_amount").text(parseFloat($("#payment_sum_price").text())-parseFloat($("#payment_sum_coupon").text())-parseFloat($("#payment_sum_point").text()));
                if(parseFloat($("#payment_sum_amount").text()) <= 0){
                    $("#payment_select_payment").text(i18n.getI18n("request_payment_onlyPoint"));
                    $("#payment_sum_amount").text(0);
                }

                $(".payment_select")[2].click();
            });
            
            $(".payment_button").click(function(){
                
                if($("#payment_select_payment").text() == i18n.getI18n("request_payment_onlyPoint")){
                    $(".payment_button").removeClass("selected");
                    $("#payment_iamport").hide();
                    return;
                }
                
                $(".payment_button").removeClass("selected");
                $(this).addClass("selected");
                
            });
            
            $("#pay_paypal").click(function(){
                if($("#payment_select_payment").text() == i18n.getI18n("request_payment_onlyPoint"))return;
                $("#payment_select_payment").text("PayPal");
                $("#payment_iamport").hide();
            });
            $("#pay_alipay").click(function(){
                if($("#payment_select_payment").text() == i18n.getI18n("request_payment_onlyPoint"))return;
                $("#payment_select_payment").text("Alipay");
                $("#payment_iamport").hide();
            });
            $("#pay_iamport").click(function(){
                if($("#payment_select_payment").text() == i18n.getI18n("request_payment_onlyPoint"))return;
                $("#payment_select_payment").text(i18n.getI18n("request_payment_CreditCard"));
                $("#payment_iamport").show();
            });
            
        
            
            $(".payment_close").click(function(){
                $(".popup_payment").remove(); 
            });
            
            
            $(".payment_select").click(function(){
                $(".payment_select").removeClass("selected");
                $(this).addClass("selected");
                $(".payment_tab").removeClass("selected");
                $("#payment_tab" + $(this).attr("value")).addClass("selected");
                if($(this).attr("value") == '2'){
                    $('.payment_submit').text(i18n.getI18n("request_payment_pay"));
                }
                else{
                    $('.payment_submit').text(i18n.getI18n("request_payment_apply"));
                }
            });
            
            // $(".payment").click(function(){
                
            // })
            
            $(".payment_submit").click();
        });

    }
    
    if(localStorage.getItem("content").length>0){
        $("#request_text").val(localStorage.getItem("content"));
        $("#selectLanguageFrom").ddslick('select', {index: localStorage.getItem("originalLang") });
        $("#selectLanguageTo").ddslick('select', {index: localStorage.getItem("targetLang") });
        $("#request_text").change();
        localStorage.clear();
    }
    
    
    $('body').off("selectstart");
    $('body').off("dragstart");
    $('body').off("contextmenu"); 
    
    // setPrice();
});


function setPrice($text) {

    if($("#request_isPhoto").val() === '1'){
        $text = $previewText;
    }
    else if($("#request_isFile").val() === '1'){
        $text = "";

        $.each(previewTextList,function(i, val){
            $text = $text + val;
        });
    }
    else if($("#request_isI18n").val() === '1'){
        $text = "";
        $text = $(".file_i18n_line_content").map(function () {
			return $(this).text();
		}).get().join(" ");
    }
    
    $text = $text || $("#request_text").val();
    // if($text.length <= 140){

    //     $("#priceBar").attr("min",10);



    //     // if($("#request_isText").val()!="1"){
            
    //     //     // $(".priceMin").text("USD 10.00부터~");
    //     //     $("#priceBar").attr("min",10);
            
    //     // }
    //     // else{
            
    //     //     // $(".priceMin").text("USD 0.00부터~");
    //     //     $("#priceBar").attr("min",0);
    //     // }
    // }
    // else if($text.length <= 166){
    //     $("#priceBar").attr("min",10);
    //     // $(".priceMin").text("USD 10.00부터~");
    // }
    // else
    {
        // alert('lang_' + $('input[name=request_originalLang]').val() + '_countBy');
        if(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_countBy') == 'letter'){
            $("#priceBar").attr("min",(parseFloat(($text.length * parseFloat(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_price'))))).toFixed(2));
            // $("#priceBar").attr("min",changeMoney(parseFloat(($text.length * parseFloat(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_price'))))));
        }
        else if(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_countBy') == 'word'){
            $("#priceBar").attr("min",(parseFloat(($text.replace(/^[\s,.;]+/, "").replace(/[\s,.;]+$/, "").split(/[\s,.;]+/).length * parseFloat(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_price'))))).toFixed(2));
            // $("#priceBar").attr("min",changeMoney(parseFloat(($text.replace(/^[\s,.;]+/, "").replace(/[\s,.;]+$/, "").split(/[\s,.;]+/).length * parseFloat(i18n.getI18n('lang_' + $('input[name=request_originalLang]').val() + '_price'))))));
            
        }

    }
    
    $(".priceMin").text(i18n.getI18n("request_price_startingAt", {money: parseFloat( $("#priceBar").attr("min") ).toFixed(2)}));
    // $(".priceMin").text((parseFloat( $("#priceBar").attr("min") )).toFixed(2)+ " " + i18n.getI18n('payment_currency') + "부터~");

    $("#priceBar").attr("max",parseFloat( $("#priceBar").attr("min") )* 2 + 99.99);
    // $("#priceBar").attr("max",parseFloat( $("#priceBar").attr("min") )* 2 + changeMoney(99.99));
    if (parseFloat($("#priceBar").attr("min")) >= parseFloat($("#nowPrice").val()))
        $("#nowPrice").val(parseFloat($("#priceBar").attr("min")).toFixed(2));
    $("#localPrice").text(changeMoney($("#nowPrice").val()));

    if(i18n.getI18n('payment_rate') == '1'){
        $("#spanLocalPrice").hide();
    } 
    else{
        $("#spanLocalPrice").show();
    }

}
