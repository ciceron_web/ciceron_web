$(document).ready(function(){

	$(".btnRequest").click(function(){
		$("body").prepend('<div class="popup popup_payment"> <div class="popup_background"></div>' +
			'<div class="payment">' +
			'<div class="div_1-2" style="height: 100%; position: relative; overflow-y: visible;"> ' +
			'<div class="div_1-2_1" style="position: relative;"> ' +
			'<div id="payment_select">' +
			'<div class="payment_select" value="0" style="display: none;"> ' +
			'쿠폰 사용 ' +
			'<span class="payment_select_now" id="payment_select_coupon">없음</span> ' +
			'</div> ' +
			'<div class="payment_select" value="1" style="display: none;">' +
			'포인트 사용 ' +
			'<span class="payment_select_now" id="payment_select_point">0</span> ' +
			'</div>' +
			'<div class="payment_select selected" value="2"> ' +
			'결제수단 선택 ' +
			'<span class="payment_select_now" id="payment_select_payment">-</span>' +
			'</div> ' +
			'<div class="payment_sum"> ' +
			'<div style="display: none;">의뢰 금액(₩<span id="payment_sum_price"></span>) - 쿠폰(₩<span id="payment_sum_coupon"></span>) - 포인트(₩<span id="payment_sum_point"></span>)<br /></div>' +
			'<span class="payment_select_now" style="font-weight: bold;">결제 금액: ₩<span id="payment_sum_amount"></span></span> ' +
			'</div>' +
			'</div>' +
			'</div> ' +
			'<div class="div_1-2_2" style="position: relative;">' + 
			'<div id="payment_tab"> ' +
			'<div class="payment_tab" id="payment_tab0">' +
			'<span class="payment_tab_title"> 쿠폰번호 입력: </span>' +
			'<div class="payment_tab_content"> ' +
			'<input type="text" placeholder="없음" id="payment_usingCoupon"/> </div>' +
			'</div>' +
			'<div class="payment_tab" id="payment_tab1">' +
			'<span class="payment_tab_title"> 현재 내 포인트: <span id="payment_myPoint"></span> </span>' +
			'<br /> ' +
			'<span class="payment_tab_title"> 사용할 포인트: </span>' +
			'<div class="payment_tab_content">' +
			'<input type="number" step="0.01" min="0" max="100" value="0" id="payment_usingPoint"/> ' +
			'</div>' +
			'</div> ' +
			'<div class="payment_tab selected" id="payment_tab2"> ' +
			'<span class="payment_tab_title"> 결제 수단을 선택해주세요. </span> ' +
			'<div class="payment_tab_content">' +
			'<div class="payment_buttons"> ' +
			'<button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_paypal">' +
			'<img src="/static/img/payment/paypal.png" style="width: 150px;"/>' +
			'</button>' +
			'<button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_alipay">' +
			'<img src="/static/img/payment/alipay.png" style="width: 150px;"/>' +
			'</button> ' +
			'</div> ' +
			'또는' +
			'<div class="payment_buttons"> ' +
			'<button type="button" style="border: 1px solid #ddd;" class="payment_button" id="pay_iamport">' +
			'카드번호 입력' +
			'</button>' +
			' </div> ' +
			'<div class="payment_tab_content" id="payment_iamport" style="display: none;"> ' +
			'카드번호: ' +
			'<input type="text" maxlength="4" size="4" id="payment_card1" placeholder="0000">' +
			'- <input type="text" maxlength="4" size="4" id="payment_card2" placeholder="0000">' +
			'- <input type="password" maxlength="4" size="4" id="payment_card3" placeholder="0000">' +
			'- <input type="password" maxlength="4" size="4" id="payment_card4" placeholder="0000"> ' +
			'<br /> ' +
			'유효기간: ' +
			'<input type="text" maxlength="2" size="2" id="payment_month" placeholder="00">' +
			'월 ' +
			'<input type="text" maxlength="4" size="4" id="payment_year" placeholder="0000">' +
			'년' +
			'<br /> ' +
			'생년월일(6글자) 또는 사업자등록번호(10글자): ' +
			'<input type="text" maxlength="10" size="10" id="payment_birth" placeholder="000000">' +
			'<br />' +
			'비밀번호 앞 두 자리:' +
			'<input type="password" maxlength="2" size="2" id="payment_pwd" placeholder="00">' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'<button class="payment_submit" value=""> 결제하기 </button>' +
			'<button class="payment_close"> 취소 </button> ' +
			'</div> ' +
			'</div>');

			

			$("#payment_sum_coupon").text("0");
			$("#payment_sum_point").text("0");
			$("#payment_sum_amount").text(4721200);
			
			$(".payment_submit").attr("value", 360);
			
			$("#payment_sum_price").text(4721200);
			
			$("#payment_myPoint").text(0);
			
			// if(parseFloat($("#nowPrice").val()) < parseFloat($userInfomation.user_point)){
			// 	$("#payment_usingPoint").attr("max",$("#nowPrice").val());
				
			// }else{
			// 	$("#payment_usingPoint").attr("max",$userInfomation.user_point);
			// }
			
			$("#payment_usingPoint").change(function(){
				if(parseFloat( $("#payment_usingPoint").val()) > parseFloat($("#payment_usingPoint").attr("max"))){
					$("#payment_usingPoint").val( parseFloat($("#payment_usingPoint").attr("max")));
				}
			});
			
			
			$(".payment_submit").click(function(){
				if($(".payment_select.selected").attr("value") == "0"){
					if($("#payment_usingCoupon").val() == ""){
						$("#payment_sum_coupon").text("0");
						
					}
					else{
						alert("등록되지 않은 쿠폰입니다.");
						$("#payment_sum_coupon").text("0");
					}
				}else if($(".payment_select.selected").attr("value") == "1"){
					$("#payment_select_point").text($("#payment_usingPoint").val());
					$("#payment_sum_point").text($("#payment_usingPoint").val());
					
				}else if($(".payment_select.selected").attr("value") == "2"){
					//$(".payment_button.selected")
					
					if(parseFloat($("#payment_sum_price").text()) < 2.5 && parseFloat($("#payment_sum_price").text()) > 0){
						alert("죄송합니다. 현재 서비스 내에서 결제는 $2.5 이상만 가능합니다.\n불편을 드려 죄송합니다.");
						return;
					}

					formData = new FormData();
					
					
					if($("#payment_select_point").text() == "-") {
						alert("사용할 포인트를 먼저 적용해주세요.");
						return;
					}
					
					if($("#payment_select_payment").text() == "-") {
						alert("결제 수단을 선택해주세요.");
						return;
					}
					else if($("#payment_select_payment").text() == "PayPal"){
						formData.append("payment_platform", "paypal");             
					}
					else if($("#payment_select_payment").text() == "Alipay"){
						formData.append("payment_platform", "alipay");             
					}
					else if($("#payment_select_payment").text() == "직접입력"){
						formData.append("payment_platform", "iamport");
						formData.append("card_number", $("#payment_card1").val() + "-" + $("#payment_card2").val() + "-" + $("#payment_card3").val() + "-" + $("#payment_card4").val());
						formData.append("expiry", $("#payment_year").val() + "-" + $("#payment_month").val());
						formData.append("birth", $("#payment_birth").val());
						formData.append("pwd_2digit", $("#payment_pwd").val());
					}
					else if($("#payment_select_payment").text() == "포인트로만"){
						formData.append("payment_platform", "point_only");             
					}
					
					formData.append("payment_amount", parseFloat($("#payment_sum_price").text()) / 1000);             
					// formData.append("payment_amount", parseFloat($("#payment_sum_price").text()) / parseFloat(i18n.getI18n('payment_rate')));             
					formData.append("point_for_use", parseFloat($("#payment_select_point").text()) / 1000);
					// formData.append("point_for_use", parseFloat($("#payment_select_point").text()) / parseFloat(i18n.getI18n('payment_rate')));
					formData.append("pay_by", "web");
					
			
					startLoading();
					$.ajax({
						url: '/api/user/requests/'+$(this).attr("value")+'/payment/start',
						data: formData,
						processData: false,
						contentType: false,
						dataType: "JSON",
						type: 'POST',
						error: function(xhr, ajaxOptions, thrownError){
							// if (xhr.status == 417) {
							//     alert("올바르지 않은 이메일 주소입니다.");
							// }
							// else if(xhr.status == 412){
							//     alert("이미 존재하는 이메일 주소입니다.");
							// }
							endLoading();
							alert("결제에 실패하였습니다.");
						}
					}).done(function(data){
						endLoading();
						requestComplete = true;
						alert("결제에 성공하였습니다.");
						location.href = "http://ciceron.me";
					});
				}
				
				$("#payment_sum_amount").text(parseFloat($("#payment_sum_price").text())-parseFloat($("#payment_sum_coupon").text())-parseFloat($("#payment_sum_point").text()));
				if(parseFloat($("#payment_sum_amount").text()) <= 0){
					$("#payment_select_payment").text("포인트로만");
				}

				$(".payment_select")[2].click();
			});
			
			$(".payment_button").click(function(){
				
				if($("#payment_select_payment").text() == "포인트로만"){
					$(".payment_button").removeClass("selected");
					$("#payment_iamport").hide();
					return;
				}
				
				$(".payment_button").removeClass("selected");
				$(this).addClass("selected");
				
			});
			
			$("#pay_paypal").click(function(){
				// if($("#payment_select_payment").text() == "포인트로만")return;
				// $("#payment_select_payment").text("PayPal");
				// $("#payment_iamport").hide();
				alert("관리자에 의해 제한된 결제 수단입니다.");
			});
			$("#pay_alipay").click(function(){
				// if($("#payment_select_payment").text() == "포인트로만")return;
				// $("#payment_select_payment").text("Alipay");
				// $("#payment_iamport").hide();
				alert("관리자에 의해 제한된 결제 수단입니다.");
			});
			$("#pay_iamport").click(function(){
				if($("#payment_select_payment").text() == "포인트로만")return;
				$("#payment_select_payment").text("직접입력");
				$("#payment_iamport").show();
			});
			

			
			$(".payment_close").click(function(){
				$(".popup_payment").remove(); 
			});
			
			
			$(".payment_select").click(function(){
				$(".payment_select").removeClass("selected");
				$(this).addClass("selected");
				$(".payment_tab").removeClass("selected");
				$("#payment_tab" + $(this).attr("value")).addClass("selected");
				if($(this).attr("value") == '2'){
					$('.payment_submit').text("결제하기");
				}
				else{
					$('.payment_submit').text("적용하기");
				}
			});
			
			// $(".payment").click(function(){
				
			// })
			
			// $(".payment_submit").click();


	});
	
	function startLoading(){
		$("body").prepend('<div class="popup" id="popup_loading"><img class="loading" src="/static/img/loading.gif" style="position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);"/></div>');
		
	}

	function endLoading(){
		$("#popup_loading").remove();
	}



});