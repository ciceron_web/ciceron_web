/**
 * 	AuthService 
 *	사용자의 인증관련 작업과 (로그인, 로그아웃, 회원가입) 사용자 정보 수정, 권한요청을 수행하는 컴포넌트
 *	
 * @class AuthService
 * @author happyhj@gmail.com
 * @since 2015.09.15
 *
 */
define(['sha256', 'jquery', 'eventemitter', 'app/domain'], function(Sha256, $, EventEmitter, domain) {

	// 의존성 Sha256, jQuery, EventEmitter
	function AuthService() {
		EventEmitter.call(this);
		this.state = {};
		this.state.isLoggedIn = !! AuthService.LOGGED_OUT; // login || logout
		this.state.userEmail;
		this.state.salt;
	}
	
	EventEmitter.inherits(AuthService, EventEmitter);
	
	AuthService.API_DOMAIN = domain.API_DOMAIN;
	AuthService.LOGGED_OUT = 0;
	AuthService.LOGGED_IN = 1;
	AuthService.events = {};
	AuthService.events.LOGGED_OUT = "loggedOut";
	AuthService.events.LOGGED_IN = "loggedIn";
	AuthService.events.WRONG_PASSWORD = "wrongPassword";
	AuthService.events.SIGNED_UP = "signedUp";
	AuthService.events.SALT_FETCHED = "saltFetched";
	AuthService.events.PROFILE_FETCHED = "profileFetched";

	AuthService.events.LOG_IN_FAIL = "logInFail";
	
	AuthService.prototype._hash = function(source) {
		return Sha256.hash(this.state.salt + this._plainHash(source) + this.state.salt).toString();
	}
	AuthService.prototype._plainHash = function(source) {
		return Sha256.hash(source).toString();
	}
	
	// salt 가져오기
	AuthService.prototype.fetchSalt = function() {
		$.ajax({
			type: "GET",
			url: AuthService.API_DOMAIN + "/login",
			dataType: "JSON",
			xhrFields: {
				withCredentials: true
			},
			success: function(data, status, xhr) {
				this.state.salt = data.identifier;
				this.emit(AuthService.events.SALT_FETCHED);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
			}.bind(this)
		});
	}

	AuthService.prototype.getUserProfile = function(param) {
		var htUserProfile = JSON.parse(sessionStorage.getItem('userProfile'));
		if(param) {
			var fnCallback = param.success;
			var fnErrorCallback = param.error;
		}
		
		// 무조건 다시 불러오게 하기위한 조작임 
		
		this.state = htUserProfile || {};
					
		if(htUserProfile !== null) {
			if(fnCallback) {
				fnCallback(htUserProfile);	
			}		
		} else {
			this.fetchUserProfile({
				success: function(data) {
					if(fnCallback) {
						fnCallback(data);
					}
					this.setUserProfile(data);
					this.emit(AuthService.events.PROFILE_FETCHED);
				}.bind(this),
				error: function(xhr){
					if(fnErrorCallback) {
						fnErrorCallback();
					}
				}
			});
		}
		return htUserProfile;
	};
	
	AuthService.prototype.setUserProfile = function(htUserProfile) {
		sessionStorage.setItem('userProfile', JSON.stringify(htUserProfile));
	};
		
	AuthService.prototype.fetchUserProfile = function(param) {
		$.ajax({
			type: "GET",
			url: AuthService.API_DOMAIN + "/user/profile",
			dataType: "JSON",
			success: function(data, status, xhr) {
				var data = $.extend({}, data);
		
				var aDefaultUserPic = "img/20150905070803887471.png"; // TODO : 고쳐야됨 i18n 에 적혀있는 걸루
				
				if(data.user_profilePicPath == null) {
					delete data.user_profilePicPath;
				}
				
				// 없는 값 디폴트값 주입하기
				var htUserProfile = $.extend({}, {
					user_profilePicPath : aDefaultUserPic
				}, data);
			
				this.state.htUserProfile = htUserProfile;
				
				this.setUserProfile(this.state);
				
				param.success(this.getUserProfile());
				
				this.emit(AuthService.events.PROFILE_FETCHED);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				param.error(xhr);
			}.bind(this)
		});
	}

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1);
			if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
		}
		return "";
	}
	
	// 가입하기
	AuthService.prototype.signup = function(param) {
		$.ajax({
			type: "POST",
			url: AuthService.API_DOMAIN + "/signup",
			data: {
				"email": param.email,
				"password": this._plainHash(param.password),
				"name": param.name,
				"mother_language_id": param.mother_language_id
			},
			dataType: "JSON",
			success: function(data, status) {
				// 회원가입 실패와 성공을 구분해야할 까..
				this.emit(AuthService.events.SIGNED_UP);
				param.success(success);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				param.error(xhr);
			}.bind(this)
		});
	};
	
	// 로그인 상태 체크
	AuthService.prototype.checkLogin = function(callback) {
		$.ajax({
			type: "GET",
			url: AuthService.API_DOMAIN,
			dataType: "JSON"
		})
		.done(function(data, status) {
			if (data.isLoggedIn === true && this.state.isLoggedIn === false) {
				this.state.isLoggedIn = !! AuthService.LOGGED_IN;
				this.state.userEmail = data.useremail;
				this.emit(AuthService.events.LOGGED_IN);
			}
			if (data.isLoggedIn === false && this.state.isLoggedIn === true) {
				this.state.isLoggedIn = !! AuthService.LOGGED_OUT;
				this.state.userEmail = undefined;
				this.emit(AuthService.events.LOGGED_OUT);
			}
		}.bind(this))
		.fail(function(xhr, ajaxOptions, thrownError) {
			var data = xhr.responseJSON;
			if (xhr.status === 403) {
				if (data.isLoggedIn === true && this.state.isLoggedIn === false) {
					this.state.isLoggedIn = !! AuthService.LOGGED_IN;
					this.state.userEmail = data.useremail;
					this.emit(AuthService.events.LOGGED_IN);
				}
				if (data.isLoggedIn === false && this.state.isLoggedIn === true) {
					this.state.isLoggedIn = !! AuthService.LOGGED_OUT;
					this.state.userEmail = undefined;
					this.emit(AuthService.events.LOGGED_OUT);
				}
			}
		}.bind(this))
		.always(function(){
			callback(this.state);	
		}.bind(this))
		;
	};
	
	// 로그인 하기
	AuthService.prototype.login = function(param, callback) {
		
		if (!this.state.salt) return;
		$.ajax({
			type: "POST",
			url: AuthService.API_DOMAIN + "/login",
			data: {
				"email": param.email,
				"password": this._hash(param.password)
			},
			dataType: "JSON",
			success: function(data, status) {
				this.emit(AuthService.events.LOGGED_IN);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					this.emit(AuthService.events.LOG_IN_FAIL);
				}
			}.bind(this)
		});
	};
	
	// 로그아웃하기 
	AuthService.prototype.logout = function(param, callback) {
		$.get(AuthService.API_DOMAIN + "/logout", function(data, status) {
			callback(data);
		});
	};

	// 번역가 권한 요청 하기
	AuthService.prototype.applyHero = function(param) {
		$.ajax({
			type: "POST",
			url: AuthService.API_DOMAIN + "/user/be_hero",
			data: {
				"name" : param.name,
				"email" : param.email,
				"language" : param.language,
				"school" :  param.school,
				"major" :  param.major,
				"residence" :  param.residence,
				"score" :  param.score
			},
			dataType: "JSON",
			success: function(data, status) {
				param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
				}
			}.bind(this)
		});
	};
	
	// 복구 코드 요청
	AuthService.prototype.requestRecoveryCode = function(param) {
		if (!this.state.salt) return;
		$.ajax({
			type: "POST",
			url: AuthService.API_DOMAIN + "/user/create_recovery_code",
			data: {
				"email": param.email,
			},
			dataType: "JSON",
			success: function(data, status) {
				param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				param.error();
			}.bind(this)
		});
	};	

	// 복구 코드  전달
	AuthService.prototype.sendRecoveryCode = function(param) {
		if (!this.state.salt) return;
		$.ajax({
			type: "POST",
			url: AuthService.API_DOMAIN + "/user/recover_password",
			data: {
				"email": param.email,
				"code": this._plainHash(param.code), 
				"new_password": this._plainHash(param.new_password)
			},
			dataType: "JSON",
			success: function(data, status) {
				param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				param.error();
			}.bind(this)
		});
	};	

	// 노티 얻어오기
	AuthService.prototype.getNotification = function(param) {
		$.ajax({
			type: "GET",
			url: AuthService.API_DOMAIN + "/notification",
			dataType: "JSON",
			success: function(data, status, xhr) {
				var aNoti = data.data.splice(0);
				param.success(aNoti);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				param.error(xhr);
			}.bind(this)
		});
	}
			
	return new AuthService();
});