define(['jquery', 'app/AuthService', 'app/TemplateMap', 'app/TranslatedItemListController', "app/RequestService", "jquery-ui/selectmenu"], function($, authService, templateMap, TranslatedItemListController, requestService) {

	var authService = authService;
	var htUserProfile = null;
	
	function ActivityView($elContainer, htProfile) {
		htUserProfile = htProfile;
		
		this.$elContainer = $elContainer;
				
		this._htOption = {};
		this._lastScrollTime = new Date();

		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
		
		window.onscroll = function(ev) {	
			var body = document.body,
			    html = document.documentElement;
			
			var height = Math.max( body.scrollHeight, body.offsetHeight, 
			                       html.clientHeight, html.scrollHeight, html.offsetHeight );

			if (($(window).innerHeight() + $(window).scrollTop() - 56) >= height) {
				var newTime = new Date();
				newTime.setSeconds(newTime.getSeconds() - 2);
				if (newTime > this._lastScrollTime){
					this._lastScrollTime = new Date();
					this.scrollEvents();
				}
			}
		}.bind(this);
	}

	ActivityView.prototype.loadTemplate = function(htParam) {
		templateMap.get("ActivityView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			this._htOption.data = {};
			this._htOption.data.translatedCount = htUserProfile.user_numOfTranslationsCompleted;
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
			// i18n 엔터 교체 
			for(var i in this._htOption.i18n) {
				this._htOption.i18n[i] = this._htOption.i18n[i].replace(/\n/g, "<br/>");
			}
			
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};	
	
	ActivityView.prototype.initAfterLoaded = function() {
		this.render();
		this.initEvents();	
	};
	
	ActivityView.prototype.render = function() {
		this._htOption.translatedCount;
		
		var $elRenderedHtml = $(this._template(this._htOption));
		this.$elContainer.html($elRenderedHtml);
	};
	
	ActivityView.prototype.initEvents = function() {
		// 잔액 과 번역건수 표시			
		$(".revenueValue span").html(htUserProfile.user_revenue.toFixed(2));

		var $elListContainer = this.$elContainer.find(".translatedItemContainer"); 
		new TranslatedItemListController($elListContainer);			
	};
	
	ActivityView.prototype.scrollEvents = function() {
		// 잔액 과 번역건수 표시			
		//$(".revenueValue span").html(htUserProfile.user_revenue.toFixed(2));

		var $elListContainer = this.$elContainer.find(".translatedItemContainer"); 
		new TranslatedItemListController($elListContainer);			
	};
	return ActivityView;
});