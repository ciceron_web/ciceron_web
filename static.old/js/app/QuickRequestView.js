define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/TemplateMap'], function($, EventEmitter, requestService, authService, templateMap) {

	function QuickRequestView($elContainer, htOption) {
		this._htOption = htOption;
		this.$elContainer = $elContainer;
				
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}

	QuickRequestView.prototype.loadTemplate = function(htParam) {
		templateMap.get("QuickRequestView", function(sTemplate, i18n) {
			this._htOption.i18n = i18n;
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};
	
	QuickRequestView.prototype.initAfterLoaded = function() {
		this.buildUpElements();
		this.initEvents();	
	};
	
	QuickRequestView.prototype.buildUpElements = function() {
		this.$elContainer.html(this._template(this._htOption));

		this.$elToggle = this.$elContainer.find(".toggle");
		this.$elContent = this.$elContainer.find(".content");
		this.$elTextarea = this.$elContainer.find("textarea");
		this.$elMsgLength = this.$elContainer.find(".msglength");
		this.$elMsgMax = this.$elContainer.find(".msgmax");
		this.$elSubmit = this.$elContainer.find("button");
		this.$elOriginalLang = this.$elContainer.find("#original_language_select");
		this.$elTargetLang = this.$elContainer.find("#target_language_select");
		this.$elLangSwitch = this.$elContainer.find(".js-lang-switch");
	};

	function showDropdown(element) {
	    var event;
	    event = document.createEvent('MouseEvents');
	    event.initMouseEvent('mousedown', true, true, window);
	    element.dispatchEvent(event);
	};
	
	QuickRequestView.prototype.initEvents = function() {
		// 언어변경
		this.$elContainer.find(".departureLangContainer").on("click", function() {
//			this.$elContainer.find(".departureLangContainer").css("border-color", "white");
			showDropdown(this.$elOriginalLang[0])
		}.bind(this));
		this.$elContainer.find(".arrivalLangContainer").on("click", function() {
//			this.$elContainer.find(".arrivalLangContainer").css("border-color", "white");
			showDropdown(this.$elTargetLang[0])
		}.bind(this));
		
		
		
		// 접고 펴기
		this.$elToggle.on("click", function() {
			this.$elContent.toggle();
			this.$elToggle.toggleClass("expanded");
		}.bind(this));
		
		// 메시지 길이 제한
		this._onSosMsgChange = $.proxy(function() {
			var msgLength = this.$elTextarea.val().length;
			var msgMax = parseInt(this.$elMsgMax.html());
			
			if(msgLength > msgMax) {
				var msg = this.$elTextarea.val().substring(0, msgMax);
				this.$elTextarea.val(msg);
			}
			this.$elMsgLength.html(this.$elTextarea.val().length);
		}, this);
		
		this.$elTextarea.on("focus", function() {
			setInterval(this._onSosMsgChange , 500);
		}.bind(this));
		
		this.$elTextarea.on("blur", function() {
			clearInterval(this._onSosMsgChange);
		}.bind(this));		
		
		this.$elTextarea.on("keyup", this._onSosMsgChange);
		
		// 메시지 보내기
		this.$elSubmit.on("click", function() {
			if(this._validateRequest()) {
				// 문제가 없다면 요청을 보내고 화면을 리프레시한다.
				// TODO: 전체 리퀘스트 컨트롤러에게 리로드를 요청해도 된다.

			}
		}.bind(this));
		
		// 언어선택 둘 중 하나가 값이 바뀌었는데 바뀐 값이 다른 값이랑 같다면 다른 값을 바뀐값의 이전값으로 넣어준다.
		
		this.$elOriginalLang.attr("data-old-value", this.$elOriginalLang.val());
		this.$elTargetLang.attr("data-old-value", this.$elTargetLang.val());
		

		updateIcon(this);

		this.$elOriginalLang.on("change", $.proxy(function(self){
			if($(this).val() === self.$elTargetLang.val()) {
				self.$elTargetLang.val($(this).attr("data-old-value"));
				self.$elTargetLang.attr("data-old-value", self.$elTargetLang.val());
			}
			$(this).attr("data-old-value", $(this).val());

			
			updateIcon(self);
		}, undefined, this));

/*
		this.$elOriginalLang.on("blur", function(){
			$(".langContainer").css("border-color", "gray");
		});
		this.$elTargetLang.on("blur", function(){
			$(".langContainer").css("border-color", "gray");
		});
*/
				
		function updateIconStyle($el, langIndex) {
			$el.find(".lang").removeClass("KOREAN ENGLISH_US ENGLISH_UK CHINESE_MANDARIN CHINESE_CANTONESE");
			var langClass;
			switch(langIndex) {
				case 0:
					langClass = "KOREAN";
					break;
				case 1:
					langClass = "ENGLISH_US";
					break;
				case 2:
					langClass = "ENGLISH_UK";
					break;
				case 3:
					langClass = "CHINESE_MANDARIN";
					break;
				case 4:
					langClass = "CHINESE_CANTONESE";
					break;
			}
			$el.find(".lang").addClass(langClass);
		}
		function updateIcon(self) {
			updateIconStyle(self.$elContainer.find(".departureLangContainer"), parseInt(self.$elOriginalLang.val()));
			updateIconStyle(self.$elContainer.find(".arrivalLangContainer"), parseInt(self.$elTargetLang.val()));			
		}
		
		this.$elTargetLang.on("change", $.proxy(function(self){
			if($(this).val() === self.$elOriginalLang.val()) {
				self.$elOriginalLang.val($(this).attr("data-old-value"));
				self.$elOriginalLang.attr("data-old-value", self.$elOriginalLang.val());
			}
			
			$(this).attr("data-old-value", $(this).val());
			updateIcon(self);

		}, undefined, this));


		this.$elLangSwitch.on("click", function() {
			var tempTargetIndex = this.$elTargetLang.val();
			var tempTargetOldIndex = this.$elTargetLang.attr("data-old-value");
			
			this.$elTargetLang.val(this.$elOriginalLang.val());
			this.$elTargetLang.attr("data-old-value", this.$elOriginalLang.attr("data-old-value"));
			
			this.$elOriginalLang.val(tempTargetIndex);
			this.$elOriginalLang.attr("data-old-value", tempTargetOldIndex);

			updateIcon(this);
		}.bind(this));
		
		this.$elContainer.find(".actionBtn.delete").on("click", function(){
			requestService.deleteRequest(this._htOption.rawData.request_id);
		}.bind(this));
		
	};
	
	QuickRequestView.prototype._validateRequest = function() {
		if(this.$elTextarea.length > parseInt(this.$elMsgMax.html())) {
			alert("너무길다");
		} else {
			if(this.$elTextarea.val() == null) {
				alert("뭐라도 적으셔야 합니다");
				return;
			}
			console.log(authService.state);
			requestService.postQuickRequest({
				userEmail: authService.state.htUserProfile.user_email,
				originalLang: this.$elOriginalLang.val(),
				targetLang: this.$elTargetLang.val(),
				text: this.$elTextarea.val(),
				success: function(data){
					console.log(data, this);
					// 이렇게 하면 안된다!!!
					location.reload();
				}.bind(this),
				error: function(){
					
				}
			});
		}
	};
	
	return QuickRequestView;
});