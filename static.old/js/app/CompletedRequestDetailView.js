define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/TemplateMap', 'jquery-ui/magnific-popup'], function($, EventEmitter, requestService, authService, templateMap) {
	
	function CompletedRequestDetailView($elContainer) {
		EventEmitter.call(this);
		this.$elContainer = $elContainer;
		this.$elItem;
		this._htOption = {};
				
		this.loadTemplate({
			callback : function() {			
				//this.initAfterLoaded();
			}.bind(this)
		});
	}
	
	EventEmitter.inherits(CompletedRequestDetailView, EventEmitter);
	
	CompletedRequestDetailView.prototype.loadTemplate = function(htParam) {
		templateMap.get("CompletedRequestDetailView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
						
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};


		
	CompletedRequestDetailView.prototype.buildUpElements = function(htData) {
		var _htData = $.extend({}, htData);
		
		//this._htData = _htData;
		
			// 원문 엔터 교체 
			_htData.data.request_text = _htData.data.request_text.replace(/\n/g, "<br/>");
			_htData.data.request_translatorComment = _htData.data.request_translatorComment.replace(/\n/g, "<br/>");
			_htData.data.translated_text = _htData.data.translated_text.replace(/\n/g, "<br/>");
		
		var $elRenderedHtml = $(this._template($.extend(_htData,this._htOption)));
		this.$elItem = $elRenderedHtml;
		this.$elContainer.find(".detailConatiner").html(this.$elItem);
		if(!htData.data.formatIndex) {
			this.$elItem.find(".categoryIcon").remove();
			this.$elItem.find(".languageIcon:first-child").css("margin-left", "65px");
		}
		if(typeof this._htData.rawData.request_feedbackScore === "number") {
			this.$elItem.find(".makeFeedbackBtn").remove();
		} else {
			this.$elContainer.find(".feedbackRow").remove();
		}
		
		if(typeof this._htData.rawData.request_translatedTone !== "number") {
			this.$elItem.find(".translatorComment .tone").remove();
		}
	};
	
	CompletedRequestDetailView.prototype.initEvents = function() {	
		
		var sPopupTemplate = this.$elContainer.find("#feedbackPopup").html();

		this.$elItem.find(".makeFeedbackBtn").on("click", function() {		
			$.magnificPopup.open({
				items: [{
					src: $(sPopupTemplate),// Dynamically created element
					type: 'inline'
				}]
			});
			
		}.bind(this));

		this.$elItem.find(".backBtn").on("click", function() {		
			this.close();		
		}.bind(this));
		
		
		$("body").on("click", ".feedbackOption", function() {
			$(".feedbackOption").removeClass("selected");
			$(this).addClass("selected");
			if($(".sendBtn[disabled]").length>0) {
				$(".sendBtn[disabled]")[0].disabled = false;
			}
		});	
		
		$("body").on("click", ".sendBtn", function() {
			var $elSelected = $(".feedbackOption.selected");
			if($elSelected.length > 0) {
				var nFeedbackVal = parseInt($elSelected.attr("data-feedback-value"));
				requestService.rateRequestAndInvokeMoneyTransaction({
					request_id : this._htData.request_id,
					request_feedbackScore : nFeedbackVal,
					success: function(data){
						console.log(data);
						// 패드백 팝업을 닫음 
						$.magnificPopup.instance.close();
						
						this._htData.rawData.request_feedbackScore = nFeedbackVal;

						this.open(this._htData);
						/*

						// 피드백 버튼을 없애기 
						$(".makeFeedbackBtn").remove();
						*/
						// 다시 열 경우 피드백 결과가 렌더링 되게한다.
						//location.reload();
					}.bind(this)
				})
			}
		}.bind(this));	
	};
	
	CompletedRequestDetailView.prototype.prepareData = function(htData) {
		return 	{
			data: {
				// 바로찍힐 데이터 값들 
				request_id : htData.request_id,
				request_translatorPicPath : htData.request_translatorPicPath,
				request_translatorId : htData.request_translatorId,
				request_translatorBadgeList : htData.request_translatorBadgeList,
				request_translatorComment : htData.request_translatorComment,
				request_text : htData.request_text,
				translated_text : htData.translated_text,
				completed_date : htData.completed_date,
				formatIconPath : templateMap.resourceMapperFormatIcon(htData.formatIndex),
				formatIndex : htData.formatIndex,
				subjectIconPath : templateMap.resourceMapperSubjectIcon(htData.subjectIndex),
				subjectIndex : htData.subjectIndex,
				originalLangIndex : htData.originalLangIndex,
				originalLangPicPath : templateMap.resourceMapperLangFlag(htData.originalLangIndex),
				targetLangIndex : htData.targetLangIndex,
				targetLangPicPath : templateMap.resourceMapperLangFlag(htData.targetLangIndex),
				title : htData.title,
				feedback : htData.rawData.request_feedbackScore,
				translator_name : htData.translator_name,				
				// 바뀌는값인데 로컬라이징 되어야하는 값들 
				message: {
					subject: this._htOption.i18n[templateMap.getKeyForSubject(htData.subjectIndex)],
					format: this._htOption.i18n[templateMap.getKeyForFormat(htData.formatIndex)],
					targetLang : this._htOption.i18n[templateMap.getKeyForLanguage(htData.targetLangIndex)],
					originalLang : this._htOption.i18n[templateMap.getKeyForLanguage(htData.originalLangIndex)],
					tone : this._htOption.i18n[templateMap.getKeyForTone(parseInt(htData.rawData.request_translatedTone))],
					feedbackmsg : this._htOption.i18n[templateMap.getKeyForFeedback(parseInt(htData.rawData.request_feedbackScore))]
				}
			}
		};		
	};
	
	CompletedRequestDetailView.prototype.open = function(htData) {
		// 원래 뷰를 안보이게 하고 
		this.$elContainer.find(".navContainer").hide();
		
		this._htData = htData;
		
		// 재 렌더링 해서 show
		this.buildUpElements(this.prepareData(htData));
		this.initEvents();

		this.$elContainer.find(".detailConatiner").show();
	};		

	CompletedRequestDetailView.prototype.close = function(htData) {
		this.$elContainer.find(".detailConatiner").html("");
		this.$elContainer.find(".detailConatiner").hide();
		this.$elContainer.find(".navContainer").show();
	};	

	return CompletedRequestDetailView;
});