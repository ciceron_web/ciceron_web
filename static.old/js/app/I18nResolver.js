define(['jquery', 'app/TemplateMap' ], function($, templateMap) {
	
	function I18nResolver() {
		this.init();
	}
	
	I18nResolver.prototype.init = function() {
		var sMenuLanguageIndex = localStorage.getItem('menuLanguageIndex');
		var sLocale = navigator.language;


		if(!sMenuLanguageIndex) {
			if(sLocale.indexOf("ko") !== -1) {
				sMenuLanguageIndex = 1;
			} else if(sLocale.indexOf("en") !== -1) {
				sMenuLanguageIndex = 0;
			} else {
				sMenuLanguageIndex = 0;
			}
		}
	
		var nMenuLanguageIndex = parseInt(sMenuLanguageIndex);
		
		var sLangKey = this._getResourceKey(nMenuLanguageIndex);
		this.map = i18nResource[sLangKey];
		
		this.map.defaultProfilePic = "img/20150905070803887471.png";
		this.map.LANGUAGE_KEY = '[\"KOREAN\", \"ENGLISH_US\", \"ENGLISH_UK\", \"CHINESE_MANDARIN\", \"CHINESE_CANTONESE\"]';

	};

	I18nResolver.prototype.getMap = function() {
		return this.map;
	};
	
	I18nResolver.prototype._getResourceKey = function(nMenuLanguageIndex) {
		return i18nKeys[nMenuLanguageIndex];
	};

var i18nKeys = ["EN", "KR", "MANDARIN"];
	
var i18nResource = {
	"KR" : {
		"LANGUAGE" : "언어",
		
		"KOREAN" : "한국어",
		"ENGLISH_US" : "영어-미국",
		"ENGLISH_UK" : "영어-영국",
		"CHINESE_MANDARIN" : "중국어-북경어",
		"CHINESE_CANTONESE" : "중국어-광동어",
		"TAIWANESE" : "태국어",
		
		// 랜딩페이지 & 로그인 & 회원가입
		"MAIN_CENTER_MSG" : "번역이 필요하세요? 지금 사용해 보세요.",
		"LANDING_UNDERSTANDING" : "씨세론은 당신을 이해하는 번역을 합니다.",
		"LOGIN" : "로그인",
		"EMAIL" : "이메일",
		"PASSWORD" : "비밀번호",
		"PERSIST_LOGIN_STATUS" : "로그인 유지",
		"FIND_MY_PASSWORD" : "비밀번호 찾기",
		"LOGIN_WITH_FACEBOOK" : "페이스북으로 로그인하기",
		"SIGN_UP" : "회원가입",
		"LAST_NAME" : "성",
		"FIRST_NAME" : "이름",
		"CONFIRM_PASSWORD" : "암호 재입력",
		"NATIVE_LANGUAGE" : "모국어 설정",
		"SIGN_UP" : "회원가입",
		"AUTH_INPUT_MISSED" : "입력하지 않은 항목이 있습니다.",
		"AUTH_LOGIN_FAIL" : "이메일 또는 암호가 틀렸습니다.",
		"AUTH_EMAIL_DUPLICATED" : "이미 가입한 이메일이 있습니다.",
		"AUTH_EMAIL_NOT_SIGNUP" : "가입한 이메일이 아닙니다.",
		"AUTH_PASSWORD_WRONG" : "암호가 일치하지 않습니다.",
		"AUTH_RECOVERY_WRONG" : "복원코드가 틀렸습니다.",
		"AUTH_SIGNUP_COMPLETE": "회원 가입이 완료되었습니다!",
					
		// 메뉴
		"MENU_REQUEST" : "의뢰",
		"MENU_TRANSLATE" : "번역",
		"MENU_MAKE_REQUEST" : "의뢰하기",
		"MENU_PROCESSING_REQUEST" : "처리상황",
		"MENU_COMPLETED_REQUEST" : "완료된 번역",
		"MENU_CART" : "장바구니",
		"MENU_TRANSLATING" : "작업 중",
		"MENU_HISTORY" : "활동기록",
		
		// 단문번역
		"MENU_SHORT_REQUEST_INTRO" : "단문 번역이 필요하세요?",
		"MENU_SHORT_REQUEST_WRITE_HERE" : "여기에 작성하세요.",
		"MENU_SHORT_REQUEST_DESCRIPTION" : "140자 이하의 번역은 무료이며 30분 동안 히어로에게 보여집니다.",
		"MENU_SHORT_REQUEST_SUBMIT" : "등록",
		
		// 번역의뢰하기 - 시작
		"MAKE_REQUEST_READY" : "의뢰를 시작해 볼까요?",
		"MAKE_REQUEST_SIX_QUESTIONS" : "오직 6개의 질문에 답하시면 됩니다.",
		"MAKE_REQUEST_START" : "시작",
		
		// 번역의뢰하기 - 포멧
		"MAKE_REQUEST_WHAT_FORMAT" : "의뢰물의 형식은 무엇인가요?",
		"MAKE_REQUEST_FORMAT_EMAIL" : "전자우편",
		"MAKE_REQUEST_FORMAT_LETTER" : "편지",
		"MAKE_REQUEST_FORMAT_ESSAY" : "에세이",
		"MAKE_REQUEST_FORMAT_AD" : "광고",
		"MAKE_REQUEST_FORMAT_INVITATION" : "초대장",
		"MAKE_REQUEST_FORMAT_GREETINGS" : "안부",
		"MAKE_REQUEST_FORMAT_PUBLICATION" : "출판물",
		"MAKE_REQUEST_FORMAT_SCRIPT" : "대본",
		"MAKE_REQUEST_FORMAT_RESEARCH" : "리서치",
		"MAKE_REQUEST_FORMAT_REPORT" : "레포트",
		"MAKE_REQUEST_FORMAT_MENU" : "메뉴",
		"MAKE_REQUEST_FORMAT_APPLICATION" : "지원서",
		"MAKE_REQUEST_FORMAT_MANUAL" : "설명서",
		"MAKE_REQUEST_FORMAT_SNS" : "SNS",
		"MAKE_REQUEST_FORMAT_OTHER" : "기타",
		
		// 번역의뢰하기 - 분야
		"MAKE_REQUEST_WHAT_SUBJECT" : "의뢰물의 분야는 무엇인가요?",
		"MAKE_REQUEST_SUBJECT_ARTS" : "예술",
		"MAKE_REQUEST_SUBJECT_SPORTS" : "스포츠",
		"MAKE_REQUEST_SUBJECT_SCIENCE" : "과학",
		"MAKE_REQUEST_SUBJECT_HEALTH" : "건강",
		"MAKE_REQUEST_SUBJECT_ECONOMY" : "경제",
		"MAKE_REQUEST_SUBJECT_MUSIC" : "음약",
		"MAKE_REQUEST_SUBJECT_LITERATURE" : "문학",
		"MAKE_REQUEST_SUBJECT_POLITICS" : "정치",
		"MAKE_REQUEST_SUBJECT_BUSINESS" : "비즈니스",
		"MAKE_REQUEST_SUBJECT_RELIGION" : "종교",
		"MAKE_REQUEST_SUBJECT_SHOPPING" : "쇼핑",
		"MAKE_REQUEST_SUBJECT_FASHION" : "패션",
		
		"MAKE_REQUEST_SELECT_LANGUAGE" : "의뢰언어를 선택하고 의뢰물을 입력해주세요.",
		"MAKE_REQUEST_ORIGINAL_LANGUAGE" : "의뢰언어",
		"MAKE_REQUEST_TARGET_LANGUAGE" : "번역언어",
		"MAKE_REQUEST_TYPE_HERE" : "이곳에 본문을 작성해 주세요.",
		
		"MAKE_REQUEST_PRICE" : "가격은 얼마로 하시겠어요?",
		"MAKE_REQUEST_PRICE_10_IS_MIN" : "10달러는 CICERON이 제시하는 최소한의 비용입니다.",
		"MAKE_REQUEST_PRICE_YOU_CAN_PAY_MORE" : "추가 지불로 번역가를 독려할 수 도 있습니다.",

		"MAKE_REQUEST_LEAVE_MESSAGES_TO_TRANSLATOR" : "번역가에게 전달하고싶은 메시지를 적어주세요.",
		
		"MAKE_REQUEST_WHEN" : "번역이 언제까지 완료되어야 하나요?",
		"MAKE_REQUEST_DEADLINE" : "마감일",				

		"MAKE_REQUEST_CONFIRM_QUESTION" : "아래 항목이 맞나요?",
		"MAKE_REQUEST_CONFIRM_OK" : "예. 의뢰합니다.",
		"MAKE_REQUEST_CONFIRM_START_OVER" : "아니오, 다시 작성합니다.",

		"MAKE_REQUEST_SELECT_PAYMENT" : "결제 방식을 선택하세요.",
		"MAKE_REQUEST_PROCEED_PAYMENT" : "결제하기",
	
		"BANNER_REQUEST_SUCCESS" : "일반번역 의뢰를 성공적으로 등록하였습니다.",
	
		"PAY_WITH_ALIPAY" : "알리페이로 결제하기",
		"PAY_WITH_PAYPAL" : "페이팔로 결제하기",
		
		// 티켓 	
		"TICKET_SOMEONES_TRANSLATION" : "{{translatorName}}의 번역",

		"TRANSLATOR_TOGETHER" : "함께 번역하시겠어요?",
		"TRANSLATOR_GIVING_CERT" : "CICERON은 신뢰할 수 있는 번역을 제공하기 위해 히어로에게 권한을 부여하고 있습니다.",
		
		// 번역중 
		"TRANSLATING_TIME_REMAINING" : "약속한 시간까지 {{remainingMinutes}} 분 남았습니다.",
		"TRANSLATING_TEMPORARY_SAVE" : "임시저장",
		"TRANSLATING_ALERT_TEMPORARY_SAVED" : "번역중인 작업물이 임시저장 되었습니다.",
		"TRANSLATING_ALERT_SUBMMITTED" : "번역중인 작업물이 최종제출 되었습니다.",
		"TRANSLATING_EMPTY" : "번역중인 작업물이 없습니다.",
		"TRANSLATING_COMMENT" : "코멘트",
		"TRANSLATING_COMMENT_PLACEHOLDER" : "의뢰인이 참고할 내용이 있다면 여기에 적어주세요.",
		"TRANSLATING_TONE" : "어조",
		"TRANSLATING_SUBMIT" : "최종제출",
		
		// 톤
		"TONE_RESENTFUL" : "분한",
		"TONE_URGENT" : "급한",
		"TONE_NETURAL" : "중립적",
		"TONE_RESPECTFUL" : "공손한",
		"TONE_APPEALING" : "호소하는",
		"TONE_CRITICAL" : "비판적",
		"TONE_APOLOGTIC" : "미안해하는",
		"TONE_FRANK" : "솔직한",
		"TONE_EMOTIONAL" : "감정적인",
		"TONE_GRATEFUL" : "감사하는",
		
		// Acticity 
		"ACTIVITY_COUNT" : "훌륭해요! 현재까지 총 {{translatedCount}} 건 번역하셨습니다.",
		
		// ALERT
		"ALERT_WHEN_CAN_YOU_FINISH" : "{{translatorName}}님의 번역은 언제 완료되나요?",
		"ALERT_REPLY" : "답장",
		"ALERT_REPLY_SEND" : "보내기",
		"ALERT_CAN_BE_DONE" : "{{estimatedHours}}시간 뒤 까지 완료 할 수 있습니다.",
		
		"ALERT_WARN" : "완료 가능한 시간을 알려주세요! 답장하지 않을시 다른 번역가에게 권한이 넘어갑니다.",
		"ALERT_CAN_BE_DONE_DETAIL" : "{{day}}일 {{hour}}시간 {{minute}}분 뒤까지 완료할 수 있습니다.",
		"ALERT_EXPECTEDTIME_SUBMITTED": "예상 완료 가능 시간이 성공적으로 입력되었습니다!",

		"ALERT_CHECK_DEADLINE" : "{{translatorName}} 님께서 예상 마감일을 말씀해주셨어요. 확인해주세요",

		
		"ALERT_COMPLETED" : "{{translatorName}}님의 번역이 완료 되었습니다.",
		"ALERT_WAITING" : "{{requesteeName}}님의  의뢰가 새로운 번역가를 기다리고 있습니다.",
		"ALERT_YOURE_LOSING_TICKET" : "{{translatorName}}님의 의뢰물 번역권한이 사라집니다.",
		"ALERT_VIEW_CONTENT" : "의뢰물 간단히보기",
		"ALERT_UNVIEW_CONTENT" : "의뢰물 접기",
				
		"ALERT_CHECK_TICKETS_FOR_YOU" : "님, 일감이 왔어요. 찾아보세요",
		"ALERT_FEEDBACK_COMPLETED" : "{{requesteeName}}님께서 평가를 완료하셨습니다.",

		"ALERT_TRANSLATING_STOP" : "제한시간을 초과하셔서 {{requesteeName}} 님의 의뢰 번역이 중단되었습니다.",
		"ALERT_TRANSLATING_RESPOND_DELAY_STOP" : "{{requesteeName}} 님의 의뢰를 3분의 1 시점까지 응답하지 않아 의뢰가 자동 취소되었습니다.",

		"ALERT_ESTIMATED_REMAINING_TIME" : "예상 남은 시간은 다음과 같습니다.",
		"ALERT_NO_ONE_COMES" : "아무도 오지 않네요 ㅜㅜ 계속 게시하거나 의뢰를 취소할 수 있습니다.", 

		"ALERT_TRANSLATING_STOP2" : "{{translatorName}} 님이 3분의 1 시점까지 의뢰 예상 완료 시간에 답을 하지 않아 스토아로 갑니다.",

		"ALERT_GOT_HERO" : "{{translatorName}} 님께서 당신의 의뢰를 가져가셨습니다", 
		"ALERT_TRANSLATE_COMPLETE" : "{{translatorName}} 님께서 당신의 의뢰를 완료하였습니다! 원하는 번역이 되었는지 평가해주세요", 

		"ALERT_NOT_COMPLETED_YET": "{{translatorName}} 님께서 아직 의뢰를 완료하지 못했습니다. 의뢰를 취소하거나 마감을 연장할 수 있습니다."	,
		
		// 티켓
		"TICKET_TRANSLATE" : "번역하기",
		"TICKET_REMAINING_HOURS" : "남은시간",
		"TICKET_CONTEXT" : "맥락",
		"TICKET_CART" : "장바구니",
		"TICKET_SHORT_REQUEST" : "단문번역",
		"TICKET_CONTENT" : "본문",
		"TICKET_WHO_TRANSLATED" : "님께서 번역하셨습니다.",
		"TICKET_WHO_TRANSLATING" : "님이 번역중",
		"TICKET_COMPLETED" : "번역완료!",
		"TICKET_ADD_TITLE" : "나중에 기억할 수 있게 의뢰물 제목을 지어주세요",
		"TICKET_DONE" : "확인",
		"TICKET_RESUBMIT" : "재등록",
		"TICKET_DELETE" : "삭제",
		"TICKET_CANCEL" : "의뢰취소",
		"TICKET_SOMEONES_REQUEST_CONTENT" : "님의 번역의뢰 내용",
		"TICKET_REQUEST_CONTENT" : "번역의뢰 내용",
		"TICKET_MORE" : "더보기",
		
		"TICKET_ARE_YOU_SURE_TO_DELETE" : "정말 티켓을 취소하시겠습니까?", 
		"TICKET_ARE_YOU_SURE_TO_DELETE_FROM_CART" : "정말 티켓을 장바구니에서 없애시겠습니까?",
		"TICKET_ARE_YOU_SURE_TO_TRANSLATE" : "번역을 지금 시작하면, 이 티켓이 번역가님에게 할당되고 스토아에서 보이지 않게 됩니다. 시작하시겠습니까?",
		"TICKET_CANNOT_TRANSLATE_OWN_TICKET" : "자기 자신의 티켓은 번역할 수 없습니다!",
		"TICKET_ADD_CART" : "티켓이 장바구니에 담겼습니다!",
		"TICKET_NO_AUTH_TO_TRANSLATE_LANG": "해당 언어의 번역 권한이 없습니다.\n현재 히어로라도 해당 언어의 히어로 권한이 없다면 다시 신청해주셔야 합니다!",
		"TICKET_NOT_TRANSLATOR": "아직 히어로 권한이 없습니다!\n이번에 히어로가 되어 보는 건 어떨까요?",
		"TICKET_ALREADY_IN_CART": "이미 장바구니에 담은 티켓입니다 :)",
		"TICKET_ALREADY_WORK_OTHER_TRANSLATOR": "이미 다른 히어로가 번역중입니다. ㅜㅜ\n스토아에서 다른 티켓을 찾아보세요!",
		
		"DAYS" : "일",		
		"HOURS" : "시간",
		"MINUTES" : "분",
		"WORDS" : "단어",
		
		// 프로파일
		"PROFILE_LOGOUT" : "로그아웃",
		"PROFILE_COUNT_REQUEST" : "의뢰한 수",
		"PROFILE_COUNT_TRANSLATION" : "번역한 수",
		
		"CART_IS_EMPTY" : "장바구니가 비어있습니다.",
		"PENDING_EMPTY" : "요청한 번역이 없습니다.",
		
		"COMPLETED_FEEDBACK" : "평가하기",
		"COMPLETED_BACK" : "뒤로",
		"COMPLETED_REPORT" : "신고",	
		"COMPLETED_LIKE_IT" : "결과물이 마음에 드세요?",
		"COMPLETED_SEND_MESSAGE" : "번역가에게 메시지를 전달해주세요.",
		
		
		"NO_TRANSLATOR_LICENSE" : "번역 권한이 없습니다",
		"APPLY_TRANSLATOR_NOW_AND_BE_A_HERO" : "지금 신청하고 Ciceron의 히어로가 되세요!",
		"APPLY_TRANSLATOR_NOW" : "권한 신청하기",
		"APPLY_TRANSLATOR_APPLICATION_IN_REVIEW" : "번역 권한을 검토중 입니다.",
		"APPLY_TRANSLATOR_APPLICATION_IN_REVIEW_MSG" : "기다려주세요. 검토가 끝나면 메일이 발송됩니다.",
		
		
		// 피드백
		"FEEDBACK_LOVE" : "제가 원하던 번역이네요! 감사합니다",
		"FEEDBACK_SOSO" : "잘 받았습니다. 참고하겠습니다.",
		"FEEDBACK_BAD" : "부족하네요",
		
		"REQUEST_CONTEXT" :  "{{requesteeName}}님의 의뢰맥락",
		"COMPLETED_TIME" : "완료시각",
		"CAN_VIEW_ONLY_CONTEXT": "의뢰인의 소중한 정보를 보호하기 위해\n번역이 끝난 의뢰물의 맥락만 열람이 가능합니다.",
		"REVENUE" : "총 수입",
		
		"MY_POINT": "나의 적립금",
		"MY_POINT_DESC": "의뢰 취소, 혹은 번역 완료시 적립금이 쌓이게 됩니다.\n 추후 의뢰 시, 적립금을 사용하여 결제가 가능하며 적립금 환급도 가능합니다.",
		"MY_POINT_EXPIRATION_DATE": "만료 일시",
		"MY_POINT_POINTS": "적립 금액",
		"NAME": "이름",
		"MY_POINT_FOLD_DETAIL": "적립금 내역 접기",
		"MY_POINT_EXPAND_DETAIL": "적립금 내역 보기",
		"MY_POINT_REQUEST_REFUND": "적립금 환급 받기",
		
		"MY_POINT_REQUEST_REFUND_DESC": "환급 관련 정보를 메일로 발송해 드리겠습니다",
		"MY_POINT_REQUEST_REFUND_MAIL": "환급 안내 메일 받기",
		
		"ALERT": "알림",
		"USABLE_POINT": "사용가능 적립금",
		"TRANSLATED_TEXT_EMPTY": "번역문을 적어주세요",
		"COMMENT_EMPTY": "번역문에 대한 설명을 적어주세요",
		"TONE_EMPTY": "어조를 선택해주세요",
		
		"EXPIRED": "만료됨",
		"ENTER_RESUBMIT_DEADLINE": "번역이 언제까지 완료되어야 하나요?",
		"RESUBMIT_DEADLINE": "재의뢰 합니다",
		
		"POINT_USE_DESC": "보유하신 포인트만큼 차감되어 적용된 금액입니다",
		"TRANSLATING_REWARD": "번역대금",
		"POINT": "포인트"
	},
		
		
	"EN" : {
		"LANGUAGE" : "Language",		
		
		"KOREAN" : "Korean",
		"ENGLISH_US" : "English (US)",
		"ENGLISH_UK" : "English (UK)",
		"CHINESE_MANDARIN" : "Chinese (Mandarin)",
		"CHINESE_CANTONESE" : "Chinese (Cantonese)",
		"TAIWANESE" : "Taiwanese",
		
		// 랜딩페이지 & 로그인 & 회원가입
		"MAIN_CENTER_MSG" : "Need help translating? Try CICERON now.",
		"LANDING_UNDERSTANDING" : "CICERON understands what you really want to say.",
		"LOGIN" : "Log In",
		"EMAIL" : "Email",	
		"PASSWORD" : "Password",
		"PERSIST_LOGIN_STATUS" : "Stay Logged In",
		"FIND_MY_PASSWORD" : "Find Pasword",
		"LOGIN_WITH_FACEBOOK" : "Log in with Facebook",
		"SIGN_UP" : "Sign Up",
		"LAST_NAME" : "Last Name",
		"FIRST_NAME" : "First Name",
		"CONFIRM_PASSWORD" : "Confirm Password",
		"NATIVE_LANGUAGE" : "Native Language",
		"SIGN_UP" : "Sign-Up",
		"AUTH_INPUT_MISSED" : "Input missed",
		"AUTH_LOGIN_FAIL" : "Email or password error.",
		"AUTH_EMAIL_DUPLICATED" : "Already signed up with this email.",
		"AUTH_EMAIL_NOT_SIGNUP" : "Not signed up email.",
		"AUTH_SIGNUP_COMPLETE": "Sign up complete!",
		"AUTH_PASSWORD_WRONG" : "Password is not matched",
		"AUTH_RECOVERY_WRONG" : "Wrong recovery code",	
		
		// 메뉴
		"MENU_REQUEST" : "REQUEST",
		"MENU_TRANSLATE" : "TRANSLATE",
		"MENU_MAKE_REQUEST" : "Request",
		"MENU_PROCESSING_REQUEST" : "Status",
		"MENU_COMPLETED_REQUEST" : "Completed",
		"MENU_CART" : "Cart",
		"MENU_TRANSLATING" : "In Progress",
		"MENU_HISTORY" : "History",
		
		// 단문번역
		"MENU_SHORT_REQUEST_INTRO" : "Need something short translated?",
		"MENU_SHORT_REQUEST_WRITE_HERE" : "Please type here.",
		"MENU_SHORT_REQUEST_DESCRIPTION" : "Requests of less than 140 characters are translated for free and shown to our Heroes for 30 minutes.",
		"MENU_SHORT_REQUEST_SUBMIT" : "Submit",
		
		// 티켓
		"TICKET_TRANSLATE" : "Translate",
		
		// 번역의뢰하기 - 시작 
		"MAKE_REQUEST_READY" : "Ready to create your ticket?",
		"MAKE_REQUEST_SIX_QUESTIONS" : "You only need to answer 6 simple questions.",
		"MAKE_REQUEST_START" : "Start",
		
		// 번역의뢰하기 - 형식
		"MAKE_REQUEST_WHAT_FORMAT" : "What's the format?",
		"MAKE_REQUEST_FORMAT_EMAIL" : "Email",
		"MAKE_REQUEST_FORMAT_LETTER" : "Letter",
		"MAKE_REQUEST_FORMAT_ESSAY" : "Essay",
		"MAKE_REQUEST_FORMAT_AD" : "AD",
		"MAKE_REQUEST_FORMAT_INVITATION" : "Invitation",
		"MAKE_REQUEST_FORMAT_GREETINGS" : "Greetings",
		"MAKE_REQUEST_FORMAT_PUBLICATION" : "Publication",
		"MAKE_REQUEST_FORMAT_SCRIPT" : "Script",
		"MAKE_REQUEST_FORMAT_RESEARCH" : "Research",
		"MAKE_REQUEST_FORMAT_REPORT" : "Report",
		"MAKE_REQUEST_FORMAT_MENU" : "Menu",
		"MAKE_REQUEST_FORMAT_APPLICATION" : "Application",
		"MAKE_REQUEST_FORMAT_MANUAL" : "Manual",
		"MAKE_REQUEST_FORMAT_SNS" : "SNS",
		"MAKE_REQUEST_FORMAT_OTHER" : "Other",
		
		// 번역의뢰하기 - 분야
		"MAKE_REQUEST_WHAT_SUBJECT" : "What's the subject?",
		"MAKE_REQUEST_SUBJECT_ARTS" : "Arts",
		"MAKE_REQUEST_SUBJECT_SPORTS" : "Sports",
		"MAKE_REQUEST_SUBJECT_SCIENCE" : "Science",
		"MAKE_REQUEST_SUBJECT_HEALTH" : "Health",
		"MAKE_REQUEST_SUBJECT_ECONOMY" : "Economy",
		"MAKE_REQUEST_SUBJECT_MUSIC" : "Music",
		"MAKE_REQUEST_SUBJECT_LITERATURE" : "Literature",
		"MAKE_REQUEST_SUBJECT_POLITICS" : "Politics",
		"MAKE_REQUEST_SUBJECT_BUSINESS" : "Business",
		"MAKE_REQUEST_SUBJECT_RELIGION" : "Religion",
		"MAKE_REQUEST_SUBJECT_SHOPPING" : "Shopping",
		"MAKE_REQUEST_SUBJECT_FASHION" : "Fashion",
		
		"MAKE_REQUEST_SELECT_LANGUAGE" : "Please select the original language and submit your content content.",
		"MAKE_REQUEST_ORIGINAL_LANGUAGE" : "Original Language",
		"MAKE_REQUEST_TARGET_LANGUAGE" : "Target Language",
		"MAKE_REQUEST_TYPE_HERE" : "Please type here.",
		
		"MAKE_REQUEST_PRICE" : "How much are you willing to pay?",
		"MAKE_REQUEST_PRICE_10_IS_MIN" : "$ 10 is the minimum cost that CICERON presented",
		"MAKE_REQUEST_PRICE_YOU_CAN_PAY_MORE" : "You can also encourage translators to additional payments.",
	
		"MAKE_REQUEST_LEAVE_MESSAGES_TO_TRANSLATOR" : "Leave messages to translator.",
	
		"MAKE_REQUEST_WHEN" : "By when does it need to be completed?",
		"MAKE_REQUEST_DEADLINE" : "Deadline",
				
		"MAKE_REQUEST_CONFIRM_QUESTION" : "Does everything seem OK?",
		"MAKE_REQUEST_CONFIRM_START_OVER" : "No, I'll start over.",
		"MAKE_REQUEST_CONFIRM_OK" : "Yes, it's all good.",
	
		"MAKE_REQUEST_SELECT_PAYMENT" : "Select a payment method.",
		"MAKE_REQUEST_PROCEED_PAYMENT" : "Proceed",

		"PAY_WITH_ALIPAY" : "Pay with Alipay",
		"PAY_WITH_PAYPAL" : "Pay with PayPal",
			
		// 티켓 	
		"TICKET_SOMEONES_TRANSLATION" : "{{translatorName}}'s Translation",

		"TRANSLATOR_TOGETHER" : "Would you like to join our initiative?",
		"TRANSLATOR_GIVING_CERT" : "In order to ensure the quality of our translations, CICERON authorizes qualified individuals and name them our Heroes.",
		
		// 번역중 
		"TRANSLATING_TIME_REMAINING" : "{{remainingMinutes}} minutes remaining until the promised time",
		"TRANSLATING_TEMPORARY_SAVE" : "Save",
		"TRANSLATING_ALERT_TEMPORARY_SAVED" : "Your work is temporarily saved.",
		"TRANSLATING_ALERT_SUBMMITTED" : "Your work is finally submitted.",
		"TRANSLATING_EMPTY" : "You don't have translating item.",
		"TRANSLATING_COMMENT" : "Comment",
		"TRANSLATING_COMMENT_PLACEHOLDER" : "Please jot down anything you'd like to share with the requester.",
		"TRANSLATING_TONE" : "Tone",
		"TRANSLATING_SUBMIT" : "Submit",
		
		// 톤
		"TONE_RESENTFUL" : "Resentful",
		"TONE_URGENT" : "Urgent",
		"TONE_NETURAL" : "Netural",
		"TONE_RESPECTFUL" : "Respectful",
		"TONE_APPEALING" : "Appealing",
		"TONE_CRITICAL" : "Critical",
		"TONE_APOLOGTIC" : "Apologtic",
		"TONE_FRANK" : "Frank",
		"TONE_EMOTIONAL" : "Emotional",
		"TONE_GRATEFUL" : "Grateful",
		
		// Acticity 
		"ACTIVITY_COUNT" : "Awesome! You've done {{translatedCount}} translations so far.",
		
		// ALERT
		"ALERT_WHEN_CAN_YOU_FINISH" : "By when can you finish {{translatorName}}'s ticket?",
		"ALERT_REPLY" : "Respond",
		"ALERT_REPLY_SEND" : "Send",
		"ALERT_CAN_BE_DONE" : "It can be done in {{estimatedHours}} hour(s).",
		
		"ALERT_WARN" : "Please share your progress. If we don't hear from you, the ticket will sadly be handed over to another Hero",
		"ALERT_CAN_BE_DONE_DETAIL" : "It can be done in {{day}} day(s) {{hour}} hour(s) {{minute}} minute(s).",
		"ALERT_EXPECTEDTIME_SUBMITTED": "Expected due time is submitted successfully!",


		"ALERT_CHECK_DEADLINE" : "Your hero {{translatorName}} answered the expected finish of ticket!",
		
		"ALERT_COMPLETED" : "{{translatorName}}'s ticket is completed.",
		"ALERT_WAITING" : "{{requesteeName}}'s ticket is now awaiting a new Hero.",
		"ALERT_YOURE_LOSING_TICKET" : "Sorry, {{translatorName}}'s ticket will soon disappear from your queue.",
		"ALERT_VIEW_CONTENT" : "View Content",
		"ALERT_UNVIEW_CONTENT" : "Fold Content",
		
		"ALERT_CHECK_TICKETS_FOR_YOU" : ", a new ticket is waiting for your help!",
		"ALERT_FEEDBACK_COMPLETED" : "{{requesteeName}} sent you a feedback.",

		"ALERT_TRANSLATING_STOP" : "We are terribly sorry to inform that you are ceased to access {{requesteeName}} due to expired deadline.",
		"ALERT_TRANSLATING_RESPOND_DELAY_STOP" : "We are terribly sorry to inform that other hero will work for {{requesteeName}} due to no response of expected finish.",

		"ALERT_ESTIMATED_REMAINING_TIME" : "Estimated remaining time is like below.",
		"ALERT_NO_ONE_COMES" : "We are sorry to say that no hero wants to help your ticket.", 

		"ALERT_TRANSLATING_STOP2" : "Your hero {{translatorName}} has not set expected finish time. Therefore, your ticket has just moved to stoa",	

		"ALERT_GOT_HERO" : "{{translatorName}} is your hero for this ticket!", 

		"ALERT_TRANSLATE_COMPLETE" : "Your ticket has just been solved! Hero {{translatorName}} submitted a translation", 
		
		"ALERT_NOT_COMPLETED_YET": "We are sorry to say that your hero {{translatorName}} miss the deadline of the ticket. Please visit here and make decision of your ticket.",

		"BANNER_REQUEST_SUCCESS" : "We have successfully registered your request.",

		// 티켓
		"TICKET_TRANSLATE" : "Translate",
		"TICKET_REMAINING_HOURS" : "Remaining",
		"TICKET_CONTEXT" : "Context",
		"TICKET_CART" : "Cart",
		"TICKET_SHORT_REQUEST" : "Short Translation",
		"TICKET_CONTENT" : "Contents",
		"TICKET_WHO_TRANSLATED" : "{{translatorName}} has translated.",
		"TICKET_WHO_TRANSLATING" : "{{translatorName}} is translating",
		"TICKET_COMPLETED" : "Complete!",
		"TICKET_ADD_TITLE" : "Please give this completed ticket a name for future reference.",
		"TICKET_DONE" : "Done",
		"TICKET_RESUBMIT" : "Resubmit",
		"TICKET_DELETE" : "Delete",
		"TICKET_CANCEL" : "Cancel Request",
		"TICKET_REQUEST_CONTENT" : "Request Content",
		"TICKET_MORE" : "More",
		"TICKET_SOMEONES_REQUEST_CONTENT" : "'s Request Content",		
		"TICKET_ARE_YOU_SURE_TO_DELETE" : "Are you sure to cancel the request?", 
		"TICKET_ARE_YOU_SURE_TO_DELETE_FROM_CART" : "Are you sure to cancel the request from the cart?",
		"TICKET_ARE_YOU_SURE_TO_TRANSLATE" : "When you start translating now, this ticket will assign to you, and being invisible in the Stoa . Would you like to start translating?",
		"DAYS" : "Days",		
		"HOURS" : "Hours",
		"MINUTES" : "Minutes",
		"WORDS" : "Words",
		
		"TICKET_ARE_YOU_SURE_TO_DELETE" : "정말 티켓을 취소하시겠습니까?", 
		"TICKET_ARE_YOU_SURE_TO_DELETE_FROM_CART" : "정말 티켓을 장바구니에서 없애시겠습니까?",
		"TICKET_ARE_YOU_SURE_TO_TRANSLATE" : "번역을 지금 시작하면, 이 티켓이 번역가님에게 할당되고 스토아에서 보이지 않게 됩니다. 시작하시겠습니까?",
		"TICKET_CANNOT_TRANSLATE_OWN_TICKET" : "자기 자신의 티켓은 번역할 수 없습니다!",
		"TICKET_ADD_CART" : "티켓이 장바구니에 담겼습니다!",
		"TICKET_NO_AUTH_TO_TRANSLATE_LANG": "해당 언어의 번역 권한이 없습니다.\n현재 히어로라도 해당 언어의 히어로 권한이 없다면 다시 신청해주셔야 합니다!",
		"TICKET_NOT_TRANSLATOR": "아직 히어로 권한이 없습니다!\n이번에 히어로가 되어 보는 건 어떨까요?",
		"TICKET_ALREADY_IN_CART": "이미 장바구니에 담은 티켓입니다 :)",
		"TICKET_ALREADY_WORK_OTHER_TRANSLATOR": "이미 다른 히어로가 번역중입니다. ㅜㅜ\n스토아에서 다른 티켓을 찾아보세요!",
		
		// 프로파일
		"PROFILE_LOGOUT" : "Log out",
		"PROFILE_COUNT_REQUEST" : "Number of Requests",
		"PROFILE_COUNT_TRANSLATION" : "Number of Translations",
		
		
		"CART_IS_EMPTY" : "Cart is empty.",
		"PENDING_EMPTY" : "There is no request.",
		
		"COMPLETED_FEEDBACK" : "Send feedback",
		"COMPLETED_BACK" : "Back",
		"COMPLETED_REPORT" : "Report",
		"COMPLETED_LIKE_IT" : "Do you like the translation result?",
		"COMPLETED_SEND_MESSAGE" : "Please send feedback to the translator.",
		
		
		"NO_TRANSLATOR_LICENSE" : "You have no translator license yet.",
		"APPLY_TRANSLATOR_NOW_AND_BE_A_HERO" : "Apply now and Be a Ciceron's hero!",
		"APPLY_TRANSLATOR_NOW" : "Apply to be a hero",

		"APPLY_TRANSLATOR_APPLICATION_IN_REVIEW" : "Translator application is in review",
		"APPLY_TRANSLATOR_APPLICATION_IN_REVIEW_MSG" : "Please be patient, we'll mail you soon",
		
		// 피드백
		"FEEDBACK_LOVE" : "Love it!",
		"FEEDBACK_SOSO" : "Thanks",
		"FEEDBACK_BAD" : "This is shit",
		
		"REQUEST_CONTEXT" :  "{{requesteeName}}'s request context",
		"COMPLETED_TIME" : "completed",
		"CAN_VIEW_ONLY_CONTEXT": "You can view only request context, because of privacy issue.",
		"REVENUE" : "Revenue",
		
		"MY_POINT": "My Point",
		"MY_POINT_DESC": "The point is added when you finished the translation.\nOr, it is also accumulated when you canceled your request.\nYou may get refund the point, or reuse it for next requests.",
		"MY_POINT_EXPIRATION_DATE": "Expiration date",
		"MY_POINT_POINTS": "Point",
		"NAME": "Username",
		
		"MY_POINT_FOLD_DETAIL": "Fold detail",
		"MY_POINT_EXPAND_DETAIL": "Expand detail",
		"MY_POINT_REQUEST_REFUND": "Request refund",
		
		"MY_POINT_REQUEST_REFUND_DESC": "We will send you refund guide your registered email address.",
		"MY_POINT_REQUEST_REFUND_MAIL": "Send me a refund guide",
		
		"ALERT": "Alert",
		"USABLE_POINT": "Usable Point",
		"TRANSLATED_TEXT_EMPTY": "Please write your translation",
		"COMMENT_EMPTY": "Please describe the translation",
		"TONE_EMPTY": "Please select the tone",
		
		"EXPIRED": "Expired",
		"ENTER_RESUBMIT_DEADLINE": "When is new deadline?",
		"RESUBMIT_DEADLINE": "Resubmit Request",
		
		"POINT_USE_DESC": "Points are deducted as long as you hold the amount applied",
		"TRANSLATING_REWARD": "Translation price",
		"POINT": "point"
	},
	
	"MANDARIN" : {
		"LANGUAGE" : "語言*",
		
		"KOREAN" : "朝鮮的*",
		"ENGLISH_US" : "英語 - 美國*",
		"ENGLISH_UK" : "英語 - 英國*",
		"CHINESE_MANDARIN" : "中國 - 普通話*",
		"CHINESE_CANTONESE" : "中國 - 廣東*",
		"TAIWANESE" : "泰國*",
		
		// 랜딩페이지 & 로그인 & 회원가입
		"MAIN_CENTER_MSG" : "需要翻译吗？现在就来试试吧。",
		"LANDING_UNDERSTANDING" : "CICERON是给您提供容易理解的翻译。",
		"LOGIN" : "註冊*",
		"EMAIL" : "电子邮件",
		"PASSWORD" : "密码",
		"PERSIST_LOGIN_STATUS" : "保持登录",
		"FIND_MY_PASSWORD" : "忘记登录密码",
		"LOGIN_WITH_FACEBOOK" : "登录到Facebook",
		"SIGN_UP" : "注册",
		"LAST_NAME" : "性别",
		"FIRST_NAME" : "名字",
		"CONFIRM_PASSWORD" : "重新输入密码",
		"NATIVE_LANGUAGE" : "母语设置",
		"SIGN_UP" : "注册",
		"AUTH_INPUT_MISSED" : "您沒有輸入此項目。*",
		"AUTH_LOGIN_FAIL" : "電子郵件或密碼不正確。*",
		"AUTH_EMAIL_DUPLICATED" : "這個電子郵件地址已經被認購。*",
		"AUTH_EMAIL_NOT_SIGNUP" : "沒有申請開通此項電子郵件。*",
		"AUTH_SIGNUP_COMPLETE": "Sign up complete!",
		"AUTH_PASSWORD_WRONG" : "密碼不匹配。*",
		"AUTH_RECOVERY_WRONG" : "修復代碼錯誤。*",
					
		// 메뉴
		"MENU_REQUEST" : "委托",
		"MENU_TRANSLATE" : "翻译",
		"MENU_MAKE_REQUEST" : "委托",
		"MENU_PROCESSING_REQUEST" : "处理情况",
		"MENU_COMPLETED_REQUEST" : "完成譯文*",
		"MENU_CART" : "购物车",
		"MENU_TRANSLATING" : "正在处理",
		"MENU_HISTORY" : "活动记录",
		
		// 단문번역
		"MENU_SHORT_REQUEST_INTRO" : "你需要简短的翻译吗?",
		"MENU_SHORT_REQUEST_WRITE_HERE" : "请写在这儿。",
		"MENU_SHORT_REQUEST_DESCRIPTION" : "140个字以内的翻译是免费的，给英雄看30分钟。",
		"MENU_SHORT_REQUEST_SUBMIT" : "登记",
		
		// 번역의뢰하기 - 시작
		"MAKE_REQUEST_READY" : "让我们开始您的工作?",
		"MAKE_REQUEST_SIX_QUESTIONS" : "请只回答六个问题。",
		"MAKE_REQUEST_START" : "开始",
		
		// 번역의뢰하기 - 포멧
		"MAKE_REQUEST_WHAT_FORMAT" : "委托的分类是什么样的?",
		"MAKE_REQUEST_FORMAT_EMAIL" : "电子邮件",
		"MAKE_REQUEST_FORMAT_LETTER" : "信",
		"MAKE_REQUEST_FORMAT_ESSAY" : "随笔",
		"MAKE_REQUEST_FORMAT_AD" : "广告",
		"MAKE_REQUEST_FORMAT_INVITATION" : "请帖",
		"MAKE_REQUEST_FORMAT_GREETINGS" : "问候",
		"MAKE_REQUEST_FORMAT_PUBLICATION" : "刊物",
		"MAKE_REQUEST_FORMAT_SCRIPT" : "戏剧",
		"MAKE_REQUEST_FORMAT_RESEARCH" : "调查",
		"MAKE_REQUEST_FORMAT_REPORT" : "报告",
		"MAKE_REQUEST_FORMAT_MENU" : "菜单",
		"MAKE_REQUEST_FORMAT_APPLICATION" : "志愿书",
		"MAKE_REQUEST_FORMAT_MANUAL" : "说明书",
		"MAKE_REQUEST_FORMAT_SNS" : "社交网络服务",
		"MAKE_REQUEST_FORMAT_OTHER" : "其他",
		
		// 번역의뢰하기 - 분야
		"MAKE_REQUEST_WHAT_SUBJECT" : "您的委托的领域是什么样的?",
		"MAKE_REQUEST_SUBJECT_ARTS" : "艺术",
		"MAKE_REQUEST_SUBJECT_SPORTS" : "体育",
		"MAKE_REQUEST_SUBJECT_SCIENCE" : "科学",
		"MAKE_REQUEST_SUBJECT_HEALTH" : "健康",
		"MAKE_REQUEST_SUBJECT_ECONOMY" : "经济",
		"MAKE_REQUEST_SUBJECT_MUSIC" : "音乐",
		"MAKE_REQUEST_SUBJECT_LITERATURE" : "文学",
		"MAKE_REQUEST_SUBJECT_POLITICS" : "政治",
		"MAKE_REQUEST_SUBJECT_BUSINESS" : "商务",
		"MAKE_REQUEST_SUBJECT_RELIGION" : "宗教",
		"MAKE_REQUEST_SUBJECT_SHOPPING" : "购物",
		"MAKE_REQUEST_SUBJECT_FASHION" : "时尚",
		
		"MAKE_REQUEST_SELECT_LANGUAGE" : "请选择语言，然后输入委托。",
		"MAKE_REQUEST_ORIGINAL_LANGUAGE" : "委托语言",
		"MAKE_REQUEST_TARGET_LANGUAGE" : "翻译语言",
		"MAKE_REQUEST_TYPE_HERE" : "请在这里填写文字。",
		
		"MAKE_REQUEST_PRICE" : "您想价格是多少钱？",
		"MAKE_REQUEST_PRICE_10_IS_MIN" : "CICERON开价至少$10的费用。",
		"MAKE_REQUEST_PRICE_YOU_CAN_PAY_MORE" : "您也可以鼓励翻译人员额外支付。",

		"MAKE_REQUEST_LEAVE_MESSAGES_TO_TRANSLATOR" : "请写下您想给翻译家的要求。",
		
		"MAKE_REQUEST_WHEN" : "应该翻译到什么时候?",
		"MAKE_REQUEST_DEADLINE" : "截止日期",				

		"MAKE_REQUEST_CONFIRM_QUESTION" : "是这个项目，对吗？",
		"MAKE_REQUEST_CONFIRM_OK" : "是。要委托。",
		"MAKE_REQUEST_CONFIRM_START_OVER" : "不是。从新委托。",

		"MAKE_REQUEST_SELECT_PAYMENT" : "请选择付款方式。",
		"MAKE_REQUEST_PROCEED_PAYMENT" : "进行付款",
	
		"BANNER_REQUEST_SUCCESS" : "一般翻譯委託成功註冊。*",
	
		"PAY_WITH_ALIPAY" : "讓支付寶*",
		"PAY_WITH_PAYPAL" : "要使用貝寶支付*",
		
		// 티켓 	
		"TICKET_SOMEONES_TRANSLATION" : "{{translatorName}}翻譯*",

		"TRANSLATOR_TOGETHER" : "想一起翻译吗？",
		"TRANSLATOR_GIVING_CERT" : "CICERON被授權英雄，以便提供可靠的翻譯。*",
		
		// 번역중 
		"TRANSLATING_TIME_REMAINING" : "到约定时间还剩{{remainingMinutes}}分钟。",
		"TRANSLATING_TEMPORARY_SAVE" : "临时保存",
		"TRANSLATING_ALERT_TEMPORARY_SAVED" : "這一行動已經暫時儲存的水正在翻譯。*",
		"TRANSLATING_ALERT_SUBMMITTED" : "已提交您正在使用的最後用水的轉換。*",
		"TRANSLATING_EMPTY" : "有沒有作品被翻譯。*",
		"TRANSLATING_COMMENT" : "評論*",
		"TRANSLATING_COMMENT_PLACEHOLDER" : "如果此信息諮詢的客戶，請寫在這裡。*",
		"TRANSLATING_TONE" : "Nuance的*",
		"TRANSLATING_SUBMIT" : "最後提交*",
		
		// 톤
		"TONE_RESENTFUL" : "屈辱*",
		"TONE_URGENT" : "匆*",
		"TONE_NETURAL" : "中性*",
		"TONE_RESPECTFUL" : "客氣*",
		"TONE_APPEALING" : "交感神經*",
		"TONE_CRITICAL" : "批判性*",
		"TONE_APOLOGTIC" : "我很抱歉*",
		"TONE_FRANK" : "直率的*",
		"TONE_EMOTIONAL" : "情緒化*",
		"TONE_GRATEFUL" : "感謝*",
		
		// Acticity 
		"ACTIVITY_COUNT" : "太棒了！他一共有{{translatedCount}}個案件迄今翻譯。*",
		
		// ALERT
		"ALERT_WHEN_CAN_YOU_FINISH" : "{{translatorName}}威爾的翻譯完成的時候？*",
		"ALERT_REPLY" : "回答*",
		"ALERT_REPLY_SEND" : "派*",
		"ALERT_CAN_BE_DONE" : "你可以完成長達{{estimatedHours}}個小時了。*",
		
		"ALERT_WARN" : "告訴我們完整的時間！ 該機構進行的時候其他翻譯不回复。*",
		"ALERT_CAN_BE_DONE_DETAIL" : "{{minute}}分鐘到{{hour}}個小時後{{day}}天才能完成。*",

		"ALERT_CHECK_DEADLINE" : "上帝告訴我想到了{{translatorName}}的最後期限。請檢查*",
		"ALERT_EXPECTEDTIME_SUBMITTED": "Expected due time is submitted successfully!",

		"ALERT_COMPLETED" : "{{translatorName}}的翻譯已經完成。*",
		"ALERT_WAITING" : "{{requesteeName}}的佣金正在等待新的翻譯。*",
		"ALERT_YOURE_LOSING_TICKET" : "{{translatorName}}的翻譯請求水權將消失。*",
		"ALERT_VIEW_CONTENT" : "簡言之水資源委員會*",
		"ALERT_UNVIEW_CONTENT" : "水委託折疊*",
				
		"ALERT_CHECK_TICKETS_FOR_YOU" : "的，來到ilgam這一點。查找*",
		"ALERT_FEEDBACK_COMPLETED" : "他有{{requesteeName}}完成評估*",

		"ALERT_TRANSLATING_STOP" : "{{requesteeName}}的請求超時此翻譯被中斷。*",
		"ALERT_TRANSLATING_RESPOND_DELAY_STOP" : "{{requesteeName}}沒有第一點回應，委員會的第三次請求已被取消。*",

		"ALERT_ESTIMATED_REMAINING_TIME" : "剩餘的估計時間如下。*",
		"ALERT_NO_ONE_COMES" : "沒有人來了。您可以取消或繼續進行發布請求。*", 

		"ALERT_TRANSLATING_STOP2" : "{{translatorName}}沒有一分回答委員會的預計完成時間在三分鐘去STOA 。*",

		"ALERT_GOT_HERO" : "他帶來了神{{translatorName}}站在你的要求*", 
		"ALERT_TRANSLATE_COMPLETE" : "尊敬的地位{{translatorName}}完成你的佣金！如果需要的話，請評價翻譯*", 

		"ALERT_NOT_COMPLETED_YET": "親愛的{{translatorName}}立場依然未能完成請求。您可以取消該請求或延長期限。*"	,
		
		// 티켓
		"TICKET_TRANSLATE" : "譯*",
		"TICKET_REMAINING_HOURS" : "剩餘時間*",
		"TICKET_CONTEXT" : "上下文*",
		"TICKET_CART" : "購物車*",
		"TICKET_SHORT_REQUEST" : "短翻譯*",
		"TICKET_CONTENT" : "正文*",
		"TICKET_WHO_TRANSLATED" : "完成翻譯。*",
		"TICKET_WHO_TRANSLATING" : "翻譯*",
		"TICKET_COMPLETED" : "翻譯完成的！*",
		"TICKET_ADD_TITLE" : "請能記住以後委託建設節水標題*",
		"TICKET_DONE" : "確認*",
		"TICKET_RESUBMIT" : "重新登記*",
		"TICKET_DELETE" : "刪除*",
		"TICKET_CANCEL" : "委員會取消*",
		"TICKET_SOMEONES_REQUEST_CONTENT" : "翻譯請求信息*",
		"TICKET_REQUEST_CONTENT" : "翻譯請求信息*",
		"TICKET_MORE" : "查看更多*",
		"TICKET_ARE_YOU_SURE_TO_DELETE" : "您確定要取消的請求？*", 
		"TICKET_ARE_YOU_SURE_TO_DELETE_FROM_CART" : "你想真正擺脫從車的要求？*",
		"TICKET_ARE_YOU_SURE_TO_TRANSLATE" : "當你現在就開始翻譯，這張票分配給翻譯一直看不見的斯多葛。你想開始？*",
		
		"TICKET_ARE_YOU_SURE_TO_DELETE" : "정말 티켓을 취소하시겠습니까?", 
		"TICKET_ARE_YOU_SURE_TO_DELETE_FROM_CART" : "정말 티켓을 장바구니에서 없애시겠습니까?",
		"TICKET_ARE_YOU_SURE_TO_TRANSLATE" : "번역을 지금 시작하면, 이 티켓이 번역가님에게 할당되고 스토아에서 보이지 않게 됩니다. 시작하시겠습니까?",
		"TICKET_CANNOT_TRANSLATE_OWN_TICKET" : "자기 자신의 티켓은 번역할 수 없습니다!",
		"TICKET_ADD_CART" : "티켓이 장바구니에 담겼습니다!",
		"TICKET_NO_AUTH_TO_TRANSLATE_LANG": "해당 언어의 번역 권한이 없습니다.\n현재 히어로라도 해당 언어의 히어로 권한이 없다면 다시 신청해주셔야 합니다!",
		"TICKET_NOT_TRANSLATOR": "아직 히어로 권한이 없습니다!\n이번에 히어로가 되어 보는 건 어떨까요?",
		"TICKET_ALREADY_IN_CART": "이미 장바구니에 담은 티켓입니다 :)",
		"TICKET_ALREADY_WORK_OTHER_TRANSLATOR": "이미 다른 히어로가 번역중입니다. ㅜㅜ\n스토아에서 다른 티켓을 찾아보세요!",
		
		"DAYS" : "日*",		
		"HOURS" : "時刻*",
		"MINUTES" : "分*",
		"WORDS" : "這句話*",
		
		// 프로파일
		"PROFILE_LOGOUT" : "註銷*",
		"PROFILE_COUNT_REQUEST" : "受託*",
		"PROFILE_COUNT_TRANSLATION" : "翻譯*",
		
		"CART_IS_EMPTY" : "您的購物車是空的。*",
		"PENDING_EMPTY" : "沒有翻譯要求。*",
		
		"COMPLETED_FEEDBACK" : "信息反饋*",
		"COMPLETED_BACK" : "背部*",
		"COMPLETED_REPORT" : "聲明*",	
		"COMPLETED_LIKE_IT" : "結果你喜歡的？*",
		"COMPLETED_SEND_MESSAGE" : "請將消息傳遞給翻譯。*",
		
		
		"NO_TRANSLATOR_LICENSE" : "沒有翻譯權*",
		"APPLY_TRANSLATOR_NOW_AND_BE_A_HERO" : "加入我們成為Ciceron的英雄！*",
		"APPLY_TRANSLATOR_NOW" : "要申請授權*",
		"APPLY_TRANSLATOR_APPLICATION_IN_REVIEW" : "回顧翻譯特權。*",
		"APPLY_TRANSLATOR_APPLICATION_IN_REVIEW_MSG" : "請稍候。在審查電子郵件的末尾將被發送。*",
		
		
		// 피드백
		"FEEDBACK_LOVE" : "這是翻譯我想要的！謝謝*",
		"FEEDBACK_SOSO" : "好評。我們將拭目以待。*",
		"FEEDBACK_BAD" : "你失望*",
		
		"REQUEST_CONTEXT" :  "{{requesteeName}}的請求上下文*",
		"COMPLETED_TIME" : "完成時間*",
		"CAN_VIEW_ONLY_CONTEXT": "為了保護在客戶端的有價值的信息，只翻譯委託水檢查可能的情況下。*",
		"REVENUE" : "財政收入*",
		
		"MY_POINT": "我的觀點*",
		"MY_POINT_DESC": "點是取消的請求。\n您可以得到退款或重新付款。*",
		"MY_POINT_EXPIRATION_DATE": "失效日期*",
		"MY_POINT_POINTS": "點*",
		"NAME": "命名*",
		
		"MY_POINT_FOLD_DETAIL": "褶皺細節*",
		"MY_POINT_EXPAND_DETAIL": "展開詳細*",
		"MY_POINT_REQUEST_REFUND": "要求退款*",

		"MY_POINT_REQUEST_REFUND_DESC": "我們會向您退還指導您註冊的郵件地址。*",
		"MY_POINT_REQUEST_REFUND_MAIL": "給我退款指南*",
		
		"ALERT": "警報*",
		"USABLE_POINT": "可用儲備*",
		"TRANSLATED_TEXT_EMPTY": "請填寫您的翻譯*",
		"COMMENT_EMPTY": "請描述翻譯*",
		"TONE_EMPTY": "請選擇音色*",
		
		"EXPIRED": "過期*",
		"ENTER_RESUBMIT_DEADLINE": "如果是新的期限？*",
		"RESUBMIT_DEADLINE": "重新調試*",
		
		"POINT_USE_DESC": "點，你持有施用量中扣除，只要*",
		"TRANSLATING_REWARD": "翻譯價格*",
		"POINT": "點*"
	}	
};

	return new I18nResolver();
});