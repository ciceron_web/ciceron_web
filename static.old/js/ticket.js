requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    },
    
    shim: {
        'jquery': [],
        'headerfooter': ['jquery']
    }
});

requirejs(['jquery', 'headerfooter', 'i18n'], function($, headerfooter, i18n) {
	
    var i18n = new i18n();
    
    $(document).ready(function(){
        $(".notification_close").click(function(){
            $(this).parent().remove();
            if(!$.trim($("#notification").html())){
                $("#notification").remove();
                $("#subjectNotification").remove();
            }
        });
        
        getRequestsList(1);
        
        ///api/user/requests/incomplete
        
     
    });
    
    
    
    function getRequestsList(t){
        $.ajax({
            url: '/api/user/requests/incomplete?page=1',
            processData: false,
            contentType: false,
            cache : false,
            dataType: "JSON",
            type: 'GET',
            error: function(xhr, ajaxOptions, thrownError){
                // if (xhr.status == 417) {
                //     alert("올바르지 않은 이메일 주소입니다.");
                // }
                // else if(xhr.status == 412){
                //     alert("이미 존재하는 이메일 주소입니다.");
                // }
                // alert("에러!");
            }
        }).done(function(data){
            $(data.data).each(function(i, item) {
                $("#tickets").append('<div class="ticket" id="ticket_'+item.request_id+'"><div class="ticket_header"><div class="ticket_221_2"><span class="ticket_name">'+item.request_clientName+'</span></div><div class="ticket_221_2 invisible1024"><span class="subject" style="padding-top: 10px; margin: 0px;">context</span></div><div class="ticket_221_1 right"><div class="ticket_type">무료단문번역</div></div></div><hr class="invisible1024"/><div class="ticket_content"><div class="ticket_221_2"><div class="ticket_profile" style="float: left;"><img src="/api/access_file/'+item.request_clientPicPath+'"/><br /><div class="ticket_timeRemaining">'+ (item.request_status!=-1?('<p>남은 시간</p><p>'+(new Date(item.request_dueTime-(new Date()).getTime())).getMinutes()+'분</p>'):'') + '</div></div><div class="ticket_infomation"><div style="width: 100%; background-color:#eee;"><div class="ticket_language"><img src="/static/img/flags/round/'+item.request_originalLang+'.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_originalLang)+'</p></div><div style="float: left; width: 10px; padding-top: 20px;"><img src="/static/img/flags/round/cn.png" style="width: 10px; border-radius: 50%;" /></div><div class="ticket_language"><img src="/static/img/flags/round/'+item.request_targetLang+'.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">'+i18n.getI18n("lang_" + item.request_targetLang)+'</p></div></div><div style="width: 100%; background-color: transparent;"><div class="ticket_contentType"><img src="/static/img/flags/round/cn.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">출판물</p></div><div style="float: left; width: 10px; padding-top: 20px;"></div><div class="ticket_contentType"><img src="/static/img/flags/round/cn.png" style="height: 30px; border-radius: 50%;" /><p style="font-size: 8px; padding: 0px;">과학</p></div></div><p class="ticket_wordCount">'+item.request_words+' 단어</p></div></div><div class="ticket_221_2">'+item.request_text+'</div><div class="ticket_221_1 right" style="position: absolute; bottom: 0px; right: 0px;">'+(new Date(item.request_registeredTime)).toLocaleString()+'</div></div></div>');
                
                if(item.request_status==-1){
                    $("#tickets").find(".ticket").last().prepend('<div class="ticket_alert"><div class="ticket_alert_background"></div><div class="ticket_alert_inner"><span>히어로가 번역에 착수하기 전에 만료되었습니다.</span><button type="button" class="positive">내용 확인 / 재등록</button><button type="button" class="negative request_delete" value="'+item.request_id+'">삭제하기</button></div></div>');
                    
                    
                    $(".request_delete").off("click").on("click", function(e){
                        var what = $(this);
                            
                        $("body").prepend('<div class="popup" id="popup'+what.val()+'"><div class="popup_background"></div><div class="dialog"><div class="dialog_image"><img src="/static/img/9.7.1feedback3_web.png" /></div><span class="dialog_message">정말로 삭제하시겠습니까?</span><div class="dialog_buttons"><button type="button" class="positive">아니오</button><button type="button" class="negative">예</button></div></div></div>');
                    
                        $("#popup"+what.val()).find(".negative").click(function(){
                            $.ajax({
                                url: '/api/user/requests/incomplete/' + what.val(),
                                processData: false,
                                contentType: false,
                                cache : false,
                                dataType: "JSON",
                                type: 'DELETE',
                                error: function(xhr, ajaxOptions, thrownErr7or){
                                    // if (xhr.status == 417) {
                                    //     alert("올바르지 않은 이메일 주소입니다.");
                                    // }
                                    // else if(xhr.status == 412){
                                    //     alert("이미 존재하는 이메일 주소입니다.");
                                    // }
                                    // alert("에러!");
                                }
                            }).done(function(data){
                                alert("삭제되었습니다.");
                                $("#ticket_" + what.val()).remove();
                                
                                $("#popup"+what.val()).remove();
                            });
                        });
                        
                        $("#popup"+what.val()).find(".positive").click(function(){
                            $("#popup"+what.val()).remove();
                        });
                        
                        
                    });
                }
            
        
            });
        });
    }
});