requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    }
});

requirejs(['jquery', 'app/domain', 'app/AuthService', 'app/RequestService', 'app/QuickRequestView', 'app/RequestListController', 'app/GnbView', 'app/SideNavView'], function($, domain, authService, requestService, quickRequestView, requestListController, GnbView, SideNavView) {
	
	authService.setUserProfile(null);

	// 라우팅... 블록 or 리다이렉팅 
	authService.getUserProfile({
		success: function(data) { 

			var oGnbView = new GnbView($('.topbar'));
			
			var oSideNavView = new SideNavView($("#page-container"));
			
			// requestService 를 이용해서 화면에 request List 를 보여준다
			var oQuickRequestView = new quickRequestView($(".sosRequest-container"), {
				messages : {
					header : "단문 번역이 필요하세요?",
					submit : "올리기",
					textAreaPlaceholder : ""
				},
				data : {
					maxLength : 140,
					currentLength : 0
				}
			});	
			var OrequestListController = new requestListController($(".requestList-container"));	


/*
			var htUserProfile = data.htUserProfile;
			$(".translator-nav").on("click", "a", function(event) {
				if(htUserProfile.user_isTranslator === false) {
					event.preventDefault();
					alert("번역가가 되세요 \n\n라는 팝업을 띄운다.\n원하는 언어를 선택해서 신청하도록 함 \n(추후구현)");
				}	 
			});
*/
		},
		error: function(){
			location.href='/about';
		}
	});
    
});