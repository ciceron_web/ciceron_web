(function ($) {
 var it;
 var el;
 var txt;
    $.fn.helper = function($txt) {
		
		if($('.helper').length>0){
			return;
		}

		$('*').blur();
		el = $(this);
		txt = $txt;
		$('body').prepend('<div class="helper helper_background"></div>');
		// $('body').prepend('<div class="helper helper_vertical helper_top"></div>');
		// $('body').prepend('<div class="helper helper_horizontal helper_right"></div>');
		// $('body').prepend('<div class="helper helper_vertical helper_bottom"></div>');
		// $('body').prepend('<div class="helper helper_horizontal helper_left"></div>');
		$('body').prepend('<div class="helper helper_message"></div>');

		// $('.helper_vertical, .helper_horizontal').css({
		// 	"position": "absolute", 
		// 	"background-color": "black", 
		// 	"opacity": "0.2", 
		// 	"z-index":99999998, 
		// 	"border": "none", 
		// 	"display": "none"
		// });

		

		$('.helper_message').text($txt);

		// $('.helper_horizontal').css('width',"100%");
		// $('.helper_horizontal').css('height',el.outerHeight() + 40);
		// $('.helper_horizontal').css('top', el.offset().top - 20);

		// $('.helper_vertical').css('width',"100%");
		// $('.helper_vertical').css('height',"100%");
		// $('.helper_vertical').css('left',"0px");

		// $('.helper_left').css('left', "calc(-100% + "+(el.offset().left-20)+"px)");
		// $('.helper_right').css('left', el.offset().left + el.outerWidth() + 20);

		// $('.helper_top').css('top', "calc(-100% + "+(el.offset().top-20)+"px)");
		// $('.helper_bottom').css('top', el.offset().top + el.outerHeight() + 20);

		var maxZ = Math.max.apply(null,$.map($('body > *'), function(e,n){
			// if($(e).css('position')=='absolute')
					return parseInt($(e).css('z-index'))||1 ;
			})
		);

		var lastZ = $(this).css('z-index');
		var lastColor = $(this).css('background-color');
		var lastPosition = $(this).css('position');

		$(this).css('background-color', 'white');
		$(this).css('position', 'relative');

		$('<div class="helper helper_splash" style="-webkit-transition: all 0.5s ease-in-out; -moz-transition: all 0.5s ease-in-out; transition: all 0.5s ease-in-out;position: fixed; left: 0; top: 0; right: 0; bottom: 0%; background-color: black; opacity: 0.0; z-index: '+( maxZ + 1 )+';"></div>').insertBefore($(this));

		$(this).css('z-index', maxZ + 2);

		$('.helper_background').css({"position": "fixed", "left": "0px", "top": "0px", "width": "100%", "height": "100%", "z-index":100000000});
		$('.helper_message').css({
			"position": "absolute", 
			"z-index":99999999, 
			"border": "none", 
			"display": "none",
			"background-color": "black",
			"border-radius": "2px",
			"color": "yellow",
			"font-size": "20px;",
			"font-weight": "100",
			"left": el.offset().left,
			"top": el.offset().top + el.outerHeight() - 20,
			"width": el.outerWidth()
		});

		$('html,body').animate({scrollTop: el.offset().top - $('body').height() / 2}, 500);

		$('.helper_splash').css('opacity' ,'.6');
		$('.helper_message').fadeIn(200);

		var lastWidth = $(window).width();
		var lastHeight = $(window).height();


		it = setInterval(function(){
			if($('.helper').length<1){
				$('.helper_background').click();
				return;
			}
			$('html,body').animate({scrollTop: el.offset().top - $('body').height() / 2}, 0);
			if(lastWidth != $(window).width() || lastHeight != $(window).height()){
				$('.helper_background').click();
				return;
			}
			else{
				lastWidth = $(window).width();
				lastHeight = $(window).height();
			}
		}, 100);

		$('.helper_background').click(function(){
			clearInterval(it);
			$('.helper_splash').css('opacity' ,'0');
			$('.helper_message').fadeOut(200);
			setTimeout(function() {
				$('.helper').remove();
				$(el).css('z-index', lastZ);
				$(el).css('background-color', lastColor);
				$(el).css('position', lastPosition);
			}, 600);
			$('body').off('scroll touchmove mousewheel');
		});

		$('body').on('scroll touchmove mousewheel', function(e){
			e.preventDefault();
			e.stopPropagation();
			return false;
		});

		$(document).keydown(function(e) {
			if (e.keyCode == 27 || e.keyCode == 13) {
				$('.helper_background').click();
			}
		});
		

        return this;
    };
 
}( jQuery ));