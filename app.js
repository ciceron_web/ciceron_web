
global.api_url = "http://ciceron.me:5000";
global.secure_url = "https://secure.localhost:4430";

var subdomain = require('express-subdomain');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');



var routeWww = require('./routes/www');
var routeApi = require('./routes/api');
var routeSecure = require('./routes/secure');



var app = express();
// var proxy = require('express-http-proxy');
var httpProxy = require('http-proxy');
var proxy = httpProxy.createProxyServer({});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());

// app.use('/api', proxy(global.api_url, {
//   preserveHostHdr: true,
//   forwardPath: function(req, res) {
//     // console.log(require('url').parse(req.url).path);
//     return "/api" + require('url').parse(req.url).path;
//   },
//   decorateRequest: function(proxyReq, originalReq) {
//     // console.log(originalReq.url);
//     var ip = (originalReq.headers['x-forwarded-for'] || '').split(',')[0] 
//                    || originalReq.connection.remoteAddress;

//     // you can update headers 
//     // proxyReq.url = "/api" + originalReq.url;
//     // if (originalReq.body) {
//     //   proxyReq.bodyContent = JSON.stringify(originalReq.body);
//     // }




//     proxyReq.headers['x-forwarded-for-client-ip'] = ip;
//     // you can change the method 
//     // proxyReq.method = 'GET';
//     // you can munge the bodyContent. 
//     // proxyReq.bodyContent = proxyReq.bodyContent.replace(/losing/, 'winning!');
//     return proxyReq;
//   }
// }));

// app.use('/', routes);



// var secure_router = express.Router();
// var ciceron_landing_router = express.Router();

// secure_router.get('/', function(req, res) {
//     res.send('Welcome to our API!');
// } );

// ciceron_landing_router.get('/', function(req, res) {
//     res.send('home!');
//     console.log(req);
// } );


app.use(subdomain('secure', routeSecure));
// app.use(subdomain('www', routeWww));
app.use(routeWww);
app.use("/api", routeApi);
app.use("/api/v2", routeApi);

// app.use(express.static(path.join(__dirname, 'static')));
app.use(express.static(path.join(__dirname, 'public')));
// app.use('/static',express.static(path.join(__dirname, 'static')));



app.use('/api', function (req, res) {
  var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
    || req.connection.remoteAddress;
  req.headers['x-forwarded-for-client-ip'] = ip;
  // console.log('API REQUEST');
  // console.log(ip);
  proxy.web(req, res, {
    target: global.api_url + "/api"
  });
  proxy.on('error', function (e) {
    console.log(e);
  });
})



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error.jade', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error.jade', {
    message: err.message,
    error: {}
  });
});




module.exports = app;
