var $userInfomation;
var $apiURL = "/api/user/requests";
// $(window).on('popstate',function(event) {
//     if(!$("#header-profile").hasClass('invisible')){
//         history.forward();
//         setTimeout(function() {
//             $("#header-profile").addClass('invisible');
//         }, 100);
//     }
//     else{
//         location.reload();
//     }
// });


//<!--GoogleAnalyticsObject
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-82046811-1', 'auto');
  ga('send', 'pageview');
//-->GoogleAnalyticsObject

function $_GET(param) {
    var vars = {};
    window.location.href.replace( 
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function( m, key, value ) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );

    if ( param ) {
        return vars[param] ? vars[param] : "";	
    }
    return vars;
}


$(window).on('popstate',function(event) {
    
    if(!$("#header-profile").hasClass('invisible')){
        $("#header-profile").addClass('invisible');
    }
});


$.ajax({
    url: '/api/user/profile',
    processData: false,
    contentType: false,
    cache : false,
    async: false,
    dataType: 'JSON',
    type: 'GET',
    error: function(){
        alert("로그인이 필요한 서비스입니다.");
        location.href = "/about";
    }
}).done(function(data){
    
    $userInfomation = data;
    if($userInfomation.user_isTranslator) {
        $apiURL = "/api/user/translations";
        $('body').addClass('translator');
    }
    else{
        $apiURL = "/api/user/requests";
        $('body').addClass('requester');
    }
    $.cookie($userInfomation.user_email + "translate", $.cookie($userInfomation.user_email + "translate") || $userInfomation.user_motherLang);
    // $userInfomation.user_motherLang = $.cookie($userInfomation.user_email + "translate") || $userInfomation.user_motherLang;

    
});
$("#header").load("header.html",function(){
    
    // $('body').on("selectstart", function(event){ return false; });
    // $('body').on("dragstart", function(event){ return false; });
    // $('body').on("contextmenu",function(){ return false; }); 
    // $("body").prepend('<div id="popup" class="invisible"><div id="popup_background"></div></div>');
    
    $(document).bind('mousedown', function(e){
        if(!$(e.target).hasClass('img-header') && !$('#header-profile').find(e.target).length){
            if(!$("#header-profile").hasClass('invisible')){
                $("#header-profile").addClass('invisible');
                history.back();
            }
        }
    });
    
    $("a[href|='"+$(location).attr('pathname') +"']").addClass("selected");
    
    $(".img-header").click(function(){
        if($("#header-profile").hasClass('invisible')){
            $("#header-profile-container").css("max-height",$(window).height() - 150);
            $("#header-profile").removeClass('invisible');
            var obj = { Page: "CICERON", Url: location.href };
            history.pushState(obj, obj.Page, obj.Url);
            
        }
        else{
            $("#header-profile").addClass('invisible');
            history.back();
            
        }
    });
    
    $("#btn_logout").click(function(){
        
        $.ajax({
            url: '/api/logout',
            processData: false,
            contentType: false,
            cache : false,
            dataType: 'JSON',
            type: 'GET',
            error: function(){
                alert("잠시 후 시도해주세요.");
            }
        }).done(function(data){
            location.href = "/about";
        });
        
    });
    
    $("#footer").load("footer.html",function(){

        $("#img-header").attr('src', '/api/access_file/' + $userInfomation.user_profilePicPath);
        $("#img-profile").attr('src', '/api/access_file/' + $userInfomation.user_profilePicPath);
        $("#header-profile-name").text($userInfomation.user_name);

        $("#translatePage option:eq("+($.cookie($userInfomation.user_email + "translate")-1) +")").prop("selected", true);
        
        $("#translatePage").change(function(){
            
            $.cookie($userInfomation.user_email + "translate", $(this).find("option:selected").val());
            // $userInfomation.user_motherLang = $(this).find("option:selected").val();
            i18n.setLanguage($(this).find("option:selected").val());
            // alert($.cookie($userInfomation.user_email + "translate"));
            // alert($(this).find("option:selected").val());
        
        });

        $.ajax({
            url: '/api/notification',
            processData: false,
            contentType: false,
            cache : false,
            dataType: 'JSON',
            type: 'GET',
            error: function(){
                //location.href = "/about";
            }
        }).done(function(data){
            // if(data.numberOfNoti>0&&$(location).attr('pathname')!="/status"){
            if(data.numberOfNoti>0&&$(location).attr('pathname')!="/notification"){
                $('.notification_count').css('width','100px');
                $('.notification_count').text(i18n.getI18n('notification_number', data));
                $('.notification_count').show();
                setTimeout(function() {
                    $('.notification_count').text(data.numberOfNoti>9?"9+":data.numberOfNoti);                
                    $('.notification_count').css('width','15px');
                }, 2000);
                // $(".notification_count").addClass("shake-rotate shake-constant shake-constant--hover");
                // $("#btnStatus").append('<i class="fa fa-circle" style="color: #FE8B97; font-size: 5px; position: absolute;"></i>');
                // $("#btnStatus").addClass("shake-slow shake-constant shake-constant--hover");
            }
           
           
            
            $('a').on('click',function(e){
                // e.preventDefault();
                // var obj = { Page: "CICERON", Url: $(this).attr('href') };
                // history.pushState(obj, obj.Page, obj.Url);
                // location.replace(obj.Url);
                // $.ajax({url: obj.Url}).done(function(data){
                //     $('html').html(data);
                // });
                
                // $('html').empty();
                // $('html').append('<iframe style="width: 100%; height: 1000px; border: none;"></iframe>')
                // $('iframe').attr('src',obj.Url);
                
            });

        });
    });
    
    
    
});


var scrollTimer = null;
var functionScroll = function(e){
    var $nowScrollTop = $(this).scrollTop();
    if($nowScrollTop > 10){
        $("#header").addClass('scrolled');
    }
    else{
        $("#header").removeClass('scrolled');                
    }
};


$(this).scroll(function(){
    if (scrollTimer) {
        clearTimeout(scrollTimer);   // clear any previous pending timer
    }
    scrollTimer = setTimeout(functionScroll, 5);   // set new timer
});
$('body').on('touchmove', function () {
    if (scrollTimer) {
        clearTimeout(scrollTimer);   // clear any previous pending timer
    }
    scrollTimer = setTimeout(functionScroll, 5);   // set new timer
});
functionScroll();

$('head').append('<link rel="stylesheet" href="css/csshake.min.css">');



function changeMoney($USD){
    return (parseFloat($USD) * parseFloat(i18n.getI18n('payment_rate'))).toFixed(2);
}

function startLoading(){
    $("body").prepend('<div class="popup" id="popup_loading"><img class="loading" src="/static/img/loading.gif" style="position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);"/></div>');
    
}

function endLoading(){
    $("#popup_loading").remove();
}




function showTicket($id, $isI18n, $status){
    
    startLoading();
    $isI18n = typeof $isI18n !== 'undefined' ?  $isI18n : false;
    $status = typeof $status !== 'undefined' ?  $status : "";
    
    $.ajax({
        url: $apiURL + '/' + $status + ( $isI18n ? "i18n/" : "" ) + $id,
        processData: false,
        contentType: false,
        cache : false,
        async: true,
        dataType: 'JSON',
        type: 'GET',
        error: function(){
            $("body").prepend('<div class="popup" id="popup'+$id+'"><div class="popup_background"></div><div class="dialog" style="display: none;"><div class="dialog_image"><img src="/static/img/9.7.1feedback3_web.png" /></div><span class="dialog_message">에고! 존재하지 않거나 삭제된 의뢰물입니다.</span><div class="dialog_buttons"><button type="button" class="positive">확인</button></div></div></div>');
            // $("#popup"+item.request_id).delay(1).queue(function(){$("#popup"+item.request_id).removeClass("invisible");});
            $("#popup"+$id + " .dialog").fadeIn(300);
            
            $("#popup"+$id).find(".positive").click(function(){
                $("#popup"+$id).remove();
            });

            endLoading();
        }
    }).done(function(data){
        
        // alert(data.data[0].request_id);
        
        if(data.data.length < 1){
            
            $("body").prepend('<div class="popup" id="popup'+$id+'"><div class="popup_background"></div><div class="dialog" style="display: none;"><div class="dialog_image"><img src="/static/img/9.7.1feedback3_web.png" /></div><span class="dialog_message">에고! 존재하지 않거나 삭제된 의뢰물입니다.</span><div class="dialog_buttons"><button type="button" class="positive">확인</button></div></div></div>');
            // $("#popup"+item.request_id).delay(1).queue(function(){$("#popup"+item.request_id).removeClass("invisible");});
            $("#popup"+$id + " .dialog").fadeIn(300);
            
            $("#popup"+$id).find(".positive").click(function(){
                $("#popup"+$id).remove();
                
            });
        }
        if(data.data[0].request_isI18n && !$isI18n &&!data.data[0].request_status == 0){
            var s = data.data[0].request_status == 1 ? "ongoing/" : "complete/";
            endLoading();
            showTicket($id, true, s);
            return;
        }

        $(data.data).each(function(i, item) {
            $isI18n = item.request_isI18n?item.request_isI18n:false;

            $("body").prepend('<div class="popup" id="popup_' + $id +'"><div class="popup_background"></div></div>');
            $("#popup_" + item.request_id).append('<div class="detail" style="display:none;">' +
                '<div class="detail_close"><i class="icon ion-ios-close-empty fa-4x"></i></div>' + 
                '<div class="detail_header">' +
                '<span class="detail_header_left"> Infomation </span>' +
                '<span class="detail_header_right">' +(new Date(item.request_registeredTime)).toLocaleString() +'<br />~' +(new Date(item.request_dueTime)).toLocaleString() +'</span>' +
                '</div>' + 
                '<hr />' +
                '<div id="detail_context">' +
                '<span class="detail_title">Context</span>' +
                '<span class="detail_content">' +item.request_context+'</span>' +
                '</div>' +
                '<div class="detail_infomations">' +
                '<div class="detail_infomation">' +
                '<span class="detail_infomation_title">언어</span>' +
                '<hr /> <div class="detail_infomation_icons">' +
                '<div class="detail_infomation_icon">' +
                '<img src="/static/img/flags/round/' +item.request_originalLang+'.png" style="height: 30px; border-radius: 50%;" />' +
                '<p style="font-size: 8px; padding: 0px; width: 30px;">' +i18n.getI18n("lang_" + item.request_originalLang) +'</p>' +
                '</div>' +
                '<div style="float: left; width: 10px; padding-top: 20px;">' +
                '<img src="/static/img/flags/round/arrow.png" style="width: 10px; border-radius: 50%;" />' +
                '</div>' +
                '<div class="detail_infomation_icon">' +
                '<img src="/static/img/flags/round/' +item.request_targetLang+'.png" style="height: 30px; border-radius: 50%;" />' +
                '<p style="font-size: 8px; padding: 0px; width: 30px;">' +i18n.getI18n("lang_" + item.request_targetLang) +'</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                (!item.request_isSos&&!$isI18n? '<div class="detail_infomation"> <span class="detail_infomation_title">포맷</span> <hr /> <div class="detail_infomation_icons"> <div class="detail_infomation_icon"> <img src="/static/img/format/' +item.request_format +'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px; width: 30px;">' + i18n.getI18n('format_' + item.request_format) +'</p> </div> <div style="float: left; width: 10px; padding-top: 20px;"> </div> <div class="detail_infomation_icon"> <img src="/static/img/subject/' +item.request_subject+'.png" style="height: 30px; border-radius: 50%;" /> <p style="font-size: 8px; padding: 0px; width: 30px;">' + i18n.getI18n('subject_' + item.request_subject) +'</p> </div> </div> </div>':'<div class="detail_infomation" style="display: none;"> <span class="detail_infomation_title">포맷</span> <hr /> <span class="detail_infomation_title">' + i18n.getI18n("status_sos") +'</span> </div>') +
                '<div class="detail_infomation">' +
                (!$isI18n?
                '<span class="detail_infomation_title">단어수</span>' +
                '<hr /> <span class="detail_infomation_words">' +item.request_words+'</span>'
                :
                ''
                )+
                '</div>' +
                '</div>' +
                (!$isI18n?
                    '<span class="detail_title">Original Text</span>' +
                    '<span class="detail_content"><pre>' +item.request_text+'</pre></span>' +
                    '<div id="detail_translated_text">' +
                    '<span class="detail_title">Translated Text</span>' +
                    '<span class="detail_content"><pre>' +item.request_translatedText+'</pre></span>' +
                    '</div>'
                    :
                    ''
                )
                +
                (!$isI18n?
                    '<div class="detail-header clearfix">' +
                    '<div class="left-side"></div>' +
                    '<div class="right-side">' +
                    (item.request_status == 2 ?('<span class="detail_title">Result</span>' +
                    '<hr />' +
                    '<div class="left-guidance col-lg-5 col-sm-10 clearfix">' +
                    '<button class="toggle-memo fa fa-commenting-o rm-button-style"></button>' +
                    '<button class="show-split select-view-type fa fa-columns rm-button-style active" data-view-type="split-view"></button>' +
                    '<button class="show-vertical select-view-type fa fa-indent rm-button-style" data-view-type="vertical-view"></button>' +
                    '<button class="show-sentence select-view-type fa fa-align-justify rm-button-style" data-view-type="sentence-view"></button>' +
                    '</div>' +
                    '<div class="right-guidance col-lg-5 col-sm-10 text-right clearfix">' +
                    '<div class="color-guide show-origin-text">원문</div>' +
                    '<div class="color-guide show-trans-text">번역</div>' +
                    '<div class="color-guide show-opinion">문단견해</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="detail-content split-view clearfix">' +
                    showSplitView(data.realData) +
                    '</div>'):"") +

                    
                    '</div>'
                    :
                '')
                +($isI18n&&item.request_status == 2?'<div class="detail_i18n_download">'+
                '<span class="detail_title">Download: </span><a class="buttons" href="/api/user/requests/complete/i18n/'+item.request_id+'/download?format=android">Android</a> <a class="buttons" href="/api/user/requests/complete/i18n/'+item.request_id+'/download?format=iOS">iOS</a> <a class="buttons" href="/api/user/requests/complete/i18n/'+item.request_id+'/download?format=unity">Unity</a> <a class="buttons" href="/api/user/requests/complete/i18n/'+item.request_id+'/download?format=json">Json</a> <a class="buttons" href="/api/user/requests/complete/i18n/'+item.request_id+'/download?format=xamarin">Xamarin</a>'
                +'</div>':'')
                +($isI18n?'<div class="detail_i18n"></div>':'')
                
                );

            if($isI18n){
                $(".detail_i18n").append(
                    '<div class="detail_i18n_line">'
                        +'<div class="detail_i18n_name">'
                            + 'Variable ID'
                        +'</div>'
                        +'<div class="detail_i18n_from">'
                            + 'Original'
                        +'</div>'
                        +'<div class="detail_i18n_to">'
                            + 'Translation'
                        +'</div>'
                    +'</div>'
                );
                
                $.each(data.realData, function(key, value){
                    var s = "";
                    var t = "";

                    $.each(value.texts, function(key2, value2){
                        s = s +	" " + (value2.sentence?value2.sentence:"");
                        t = t + " " + (value2.translations?value2.translations:"");
                        
                    });
                    $(".detail_i18n").append(
                        '<div class="detail_i18n_line" variable_id="'+escapeHtml(value.variable_id)+'">'
                            +'<div class="detail_i18n_name">'
                                + escapeHtml(key)
                            +'</div>'
                            +'<div class="detail_i18n_from" title="'+escapeHtml(s)+'">'
                                + escapeHtml(s)
                            +'</div>'
                            +'<div class="detail_i18n_to">'
                                + escapeHtml(t)
                            +'</div>'
                        +'</div>'
                    );
				// $clamp($(".detail_i18n .detail_i18n_line div"), {clamp: 3});	
                });
            }

            if(item.request_isSos){
                $("#detail_context").remove();
            }
            
            if(!item.request_translatedText){
                $("#detail_translated_text").remove();
            }
            
            if(item.request_clientId == $userInfomation.user_email){
                
                if(item.request_status == -1){
                    $('.detail_header_left').html(i18n.getI18n('status_expired'));
                    $('.detail_header_left').css("color", "red");
                }
                
            }
            // $("#popup_"+item.request_id).removeClass("invisible");
            // $("#popup_"+item.request_id).delay(1).queue(function(){$("#popup_"+item.request_id).removeClass("invisible");});
            $("#popup_"+item.request_id + " .detail").fadeIn(300);
            $("#popup_"+item.request_id).find(".detail_close i").click(function(){
                
                $("#popup_"+item.request_id + " .detail").fadeOut(300,function(){$("#popup_"+item.request_id).remove();});
            });
        });
        endLoading();
    });
}

var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
}




  $('body').on('mouseenter touchstart', '.result-image', function () {
    var $cardContainer = $(this).closest('.card-container');

    $($cardContainer).find('.user-image, .text-info, .translate-date').addClass('get-dimed');
    $($cardContainer).find('.talk-balloon').removeClass('none');
  });

  $('body').on('mouseout touchend', '.result-image', function () {
    var $cardContainer = $(this).closest('.card-container');

    $($cardContainer).find('.user-image, .text-info, .translate-date').removeClass('get-dimed');
    $($cardContainer).find('.talk-balloon').addClass('none');
  });

  $('body').on('click', '.more-info', function () {
    var $card = $(this).closest('.translate-card');

    $($card).find('.more-info, .more-info-background').addClass('transparent');
    $($card).find('.card-function-menu').removeClass('collapsed');
  });

  $('body').on('click', '.show-card', function () {
    var $card = $(this).closest('.translate-card');
    var cardId = $($card).attr('data-id');

    showCard(cardId);
  });

  $('body').on('click', '.toggle-memo', function () {
    $(this).closest('.popup').find('.left-side').toggleClass('collapsed');
    $(this).closest('.popup').find('.right-side').toggleClass('opened');
  });

  $('body').on('click', '.select-view-type', function () {
    $('.select-view-type').removeClass('active');
    $(this).addClass('active');
    $('.detail-content').removeClass('split-view vertical-view sentence-view').addClass($(this).attr('data-view-type'));

    $('.detail-content .selected').removeClass('selected');
    $('.detail-content .has-memo').removeClass('none').removeClass('active');
    $('.detail-content .sentence-memo').addClass('none');

    setMemoPosition();
  });

  $('body').on('click', '.show-origin-text', function () {
    if ($('.show-trans-text').hasClass('selected') && $('.show-opinion').hasClass('selected')) {
      return;
    }

    if ($(this).hasClass('selected')) {
      $('.origin-paragraph *, .content-sentence.origin, .content-sentence.origin *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.origin-paragraph *, .content-sentence.origin, .content-sentence.origin *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  $('body').on('click', '.show-trans-text', function () {
    if ($('.show-origin-text').hasClass('selected') && $('.show-opinion').hasClass('selected')) {
      return;
    }

    if ($(this).hasClass('selected')) {
      $('.trans-paragraph .content-sentence, .trans-paragraph .content-sentence *, .content-sentence.trans, .content-sentence.trans *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.trans-paragraph .content-sentence, .trans-paragraph .content-sentence *, .content-sentence.trans, .content-sentence.trans *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  $('body').on('click', '.show-opinion', function () {
    if ($('.show-origin-text').hasClass('selected') && $('.show-trans-text').hasClass('selected')) {
      return;
    }

    if ($(this).hasClass('selected')) {
      $('.trans-paragraph .content-opinion, .trans-paragraph .content-opinion *, .mix-paragraph .content-opinion, .mix-paragraph .content-opinion *').removeClass('transparent');
      $(this).removeClass('selected');
    } else {
      $('.trans-paragraph .content-opinion, .trans-paragraph .content-opinion *, .mix-paragraph .content-opinion, .mix-paragraph .content-opinion *').addClass('transparent');
      $(this).addClass('selected');
    }
  });

  $('body').on('click', '.popup .origin-paragraph .content-sentence span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.right-side').find('.trans-paragraph .content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $(this).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, this);
    setMemoPosition();
  });

  $('body').on('click', '.popup .trans-paragraph .content-sentence span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.right-side').find('.origin-paragraph .content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $($another).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, $another);
    setMemoPosition();
  });

  $('body').on('click', '.popup .origin-paragraph .content-sentence .has-memo', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $anotherOne = $(this).closest('.right-side').find('.origin-paragraph').find('.content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $anotherTwo = $(this).closest('.right-side').find('.trans-paragraph').find('.content-sentence[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');

    $($anotherOne).toggleClass('selected');
    $($anotherTwo).toggleClass('selected');

    $(this).toggleClass('active');

    toggleSentenceMemo($memo, $anotherOne);
    setMemoPosition();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence.origin span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.right-side').find('.mix-paragraph .content-sentence.trans[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $(this).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, this);
    setMemoPosition();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence.trans span:not(.has-memo)', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $another = $(this).closest('.right-side').find('.mix-paragraph .content-sentence.origin[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');
    var $memoButton = $($another).parent().find('.has-memo');

    $(this).toggleClass('selected');
    $($another).toggleClass('selected');

    $($memoButton).toggleClass('active');

    toggleSentenceMemo($memo, $another);
    setMemoPosition();
  });

  $('body').on('click', '.popup .mix-paragraph .content-sentence .has-memo', function () {
    var sentenceId = $(this).parent().attr('data-seq-id');
    var $anotherOne = $(this).closest('.right-side').find('.mix-paragraph').find('.content-sentence.origin[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $anotherTwo = $(this).closest('.right-side').find('.mix-paragraph').find('.content-sentence.trans[data-seq-id="' + sentenceId + '"] span:not(.has-memo)');
    var $memo = $(this).closest('.content-paragraph').find('.sentence-memo[data-seq-id="' + sentenceId + '"]');

    $($anotherOne).toggleClass('selected');
    $($anotherTwo).toggleClass('selected');

    $(this).toggleClass('active');

    toggleSentenceMemo($memo, $anotherOne);
    setMemoPosition();
  });

  var toggleSentenceMemo = function (memo, origin) {
    var y = -1 * ($(origin).closest('.right-side').offset().top - $(origin).offset().top);

    $(memo).toggleClass('none').css('top', y + 'px');
  };

var setMemoPosition = function () {
  var els = $('.has-memo');

  var i = 0;
  for (; i < els.length; i++) {
    var el = $(els.eq(i));
    var y = -1 * ($(el).closest('.right-side').offset().top - $(el).parent().find('span:not(.has-memo)').offset().top);

    $(el).css('top', (y + 5) + 'px');
  }
};




var showSplitView = function (obj) {
    if(!obj) return;
  var template = '';

  var i = 0;
  for (; i < obj.length; i++) {
    var o = obj[i], originStr = '', transStr = '', opinionStr = '', memoStr = '', mixStr = '';

    if (o.paragraph_comment) {
      opinionStr = '<p class="content-opinion" data-seq-id="' + o.paragraph_seq + '">' +
                   '<span>' + o.paragraph_comment + '</span>' +
                   '</p>';
    }

    var j = 0;
    for (; j < o.sentences.length; j++) {
        var memoStrInToggled = "";
        var memoStr = "";
      var oo = o.sentences[j], hasMemoStr = '';
      if (oo.sentence_comment) {
        memoStr += '<div class="sentence-memo none" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                   '<div class="memo-wrapper">' +
                   '<div class="memo-content">' + oo.sentence_comment + '</div>' +
                   '<div class="memo-triangle"></div>' +
                   '</div>' +
                   '</div>';

        memoStrInToggled = '<div class="sentence-memo none" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                           '<div class="memo-wrapper">' +
                           '<div class="memo-content">' + oo.sentence_comment + '</div>' +
                           '<div class="memo-triangle"></div>' +
                           '</div>' +
                           '</div>';

        hasMemoStr = '<span class="has-memo"></span>';
      }

      originStr += '<span class="content-sentence" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                   hasMemoStr +
                   '<span>' + oo.original_text + '</span>' +
                   '<div class="memo-in-toggled">' +
                   (memoStrInToggled.length ? memoStrInToggled : '') +
                   '</div>' +
                   '</span>';

      transStr += '<span class="content-sentence" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                  '<span>' + oo.translated_text + '</span>' +
                  '</span>';

      mixStr += '<span class="content-sentence origin" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                hasMemoStr +
                '<span>' + oo.original_text + '</span>' +
                '</span>' +
                '<div class="memo-in-toggled">' +
                (memoStrInToggled.length ? memoStrInToggled : '') +
                '</div>' +
                '<span class="content-sentence trans" data-seq-id="' + o.paragraph_seq + '-' + oo.sentence_seq + '">' +
                '<span>' + oo.translated_text + '</span>' +
                '</span>';
    }

    template += '<div class="content-paragraph clearfix">' +
                '<div class="left-side">' +
                memoStr +
                '</div>' +
                '<div class="right-side">' +
                '<div class="origin-paragraph col-lg-5 col-sm-5">' +
                originStr +
                '</div>' +
                '<div class="trans-paragraph col-lg-5 col-sm-5">' +
                transStr +
                opinionStr +
                '</div>' +
                '<div class="mix-paragraph col-lg-5 col-sm-5">' +
                mixStr +
                opinionStr +
                '</div>' +
                '</div>' +
                '</div>';
  }

  return template;
};