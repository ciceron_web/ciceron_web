requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    }
}); 

requirejs(['jquery', 'app/domain', 'app/AuthService', 'app/GnbView', 'app/SideNavView', 'app/ActivityView' ], function($, domain, authService, GnbView, SideNavView, ActivityView) {
	authService.setUserProfile(null);

	authService.getUserProfile({
		success: function(data) { 
			var htUserProfile = data.htUserProfile;
			
			if(htUserProfile.user_isTranslator === false) {
				location.href='/';
			}
			
			var oGnbView = new GnbView($('.topbar'));		
			var oSideNavView = new SideNavView($("#page-container"));
			
			new ActivityView($(".content-main > .container"), htUserProfile);
		},
		error: function(){

		}
	});
});
