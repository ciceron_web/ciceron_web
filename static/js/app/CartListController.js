define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/QuickRequestTicketView', 'app/RequestTicketView', 'app/I18nResolver'], function($, EventEmitter, requestService, authService, quickRequestTicketView, requestTicketView, i18nResolver) {
	function CartListController($elContainer) {
				
		this.$elContainer = $elContainer;
		this._htData;
		
		this._fetchRequestList();
		//this.buildUpElements();
		//this.initEvents();
	}
	
	CartListController.prototype._fetchRequestList = function() {
		requestService.getRequestsInCart({
			success: function(data) {
				if(data.data.length > 0) {
					this._htData = data.data;
					this.render();
				} else {
					this.$elContainer.html("<h1>"+i18nResolver.getMap()["CART_IS_EMPTY"]+"</h1>");
				}
			}.bind(this)
		});
	};
	
	// _htData 를 이용해 $elContainer 속에 티켓들을 렌더링한다.
	CartListController.prototype.render = function() {
		for(var i in this._htData) {
			if(this._htData[i].request_isSos === true) {
				new quickRequestTicketView(this.$elContainer, {
					isAppending: true,
					rawData: this._htData[i],
					hasTranslateNow: true, // Todo authService 를 통해 결정 
					hasDeleteFromCart: true,
					data:{
						clientName: this._htData[i].request_clientName,
						originalText: this._htData[i].request_text,
						clientPicPath:	this._htData[i].request_clientPicPath || "profile_pic/2015-07-22_1.54.47.png",
						originalLangIndex: this._htData[i].request_originalLang,
						targetLangIndex: this._htData[i].request_targetLang,
						translatorPicPath:	this._htData[i].request_translatorPicPath || "profile_pic/2015-07-22_1.54.47.png",
						translatorName:	this._htData[i].request_translatorName,
					},
					deleteFromCartSuccess: function(data, ticket) {
						ticket.$elItem.remove();
					}
				});				
			} else {
				new requestTicketView(this.$elContainer, {
					isAppending: true,
					isConfirmTicket: false,
					hasTranslateNow: true, // Todo authService 를 통해 결정 
					hasDeleteFromCart: true,
					rawData: this._htData[i],
					data:{
						clientName: this._htData[i].request_clientName,
						request_points: this._htData[i].request_points, // 금액
						remainingHours: this._getRemainingHoursFromNow(this._htData[i].request_dueTime), // "2 시간", // TODO 남은 시간을 효과적으로 표시하는 함수를 i18n 지원하도록 만들어야함
						words: this._htData[i].request_words,
						
						formatIndex: this._htData[i].request_format,
						subjectIndex: this._htData[i].request_subject,
											
						originalText: this._htData[i].request_text,
						contextText: this._htData[i].request_context,
						clientPicPath:	this._htData[i].request_clientPicPath || "profile_pic/2015-07-22_1.54.47.png",
						originalLangIndex: this._htData[i].request_originalLang,
						targetLangIndex: this._htData[i].request_targetLang,
						translatorPicPath:	this._htData[i].request_translatorPicPath || "profile_pic/2015-07-22_1.54.47.png",
						translatorName:	this._htData[i].request_translatorName,
						translatorsInQueue: this._htData[i].request_translatorsInQueue
					},
					deleteFromCartSuccess: function(data, ticket) {
						ticket.$elItem.remove();
					}
				});					
			}
			

		}
	};
	
	CartListController.prototype._getRemainingHoursFromNow = function(sTargetDatetime) {
		var diff = Math.abs(new Date() - new Date(sTargetDatetime.replace(/-/g,'/')+ " GMT"));
		var sec = diff / 1000;
		var min = sec / 60;
		var hour = min / 60;
		return parseFloat(hour);
	};	
		
	return CartListController;
});