define(['jquery', 'app/domain', 'app/AuthService', 'app/TemplateMap', 'app/ApplyHeroView', "jquery-ui/selectmenu"], function($, domain, authService, templateMap, ApplyHeroView) {
	var authService = authService;
	var ApplyHeroView = ApplyHeroView;

	function ProfileView($elContainer, htUserProfile) {
		this._htOption = {};
		this._htUserProfile = htUserProfile;
		
		this.$elContainer = $elContainer;
		
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}
	
	ProfileView.prototype.loadTemplate = function(htParam) {
		templateMap.get("ProfileView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				if(sKey !== "LAUGUAGE_KEY") {
					this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
				} else {
					this._htOption.i18n[sKey] = JSON.parse(this._htOption.i18n[sKey]);
				}
			}
						
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	ProfileView.prototype.initAfterLoaded = function() {
		this.render();
		this.initEvents();
	};
		
	ProfileView.prototype.render = function() {
		var $elRenderedHtml = $(this._template(this._htOption));
		this.$elContainer.html($elRenderedHtml);
	};
	
	ProfileView.prototype.initEvents = function() {
		
			this.$elContainer.find('.applyTranslator button').on("click", function() {
				ApplyHeroView.open();
			});
		
			this.$elContainer.find('.profilepic-container > img').attr("src", "/api/access_file/" + this._htUserProfile.user_profilePicPath);	
			this.$elContainer.find('.profile-cover-bg').css('background-image', 'url('+"/api/access_file/" + this._htUserProfile.user_profilePicPath+')');

			this.$elContainer.find('.profilename-container .email').html(this._htUserProfile.user_email);
			this.$elContainer.find('.profilename-container .name').html(this._htUserProfile.user_name);					
			this.$elContainer.find('.translatorLicense-container .name').html(this._htUserProfile.user_name);					
			this.$elContainer.find('.profileText-container').html(this._htUserProfile.user_profileText);					
			this.$elContainer.find('.translationsCount').html(this._htUserProfile.user_numOfTranslationsCompleted);					
			this.$elContainer.find('.requestCount').html(this._htUserProfile.user_numOfRequestsCompleted);					

			var sLangKey = JSON.parse(this._htOption.i18n.LANGUAGE_KEY)[this._htUserProfile.user_motherLang];
			
			this.$elContainer.find('.motherLanguage > .text').html(this._htOption.i18n[sLangKey]);
			
			var sLanguages = this._htUserProfile.user_translatableLang.split(",").map(function(sLangIndex) {
				var sLangKey = JSON.parse(this._htOption.i18n.LANGUAGE_KEY)[parseInt(sLangIndex)];
				return this._htOption.i18n[sLangKey];
			}.bind(this)).join(', ');
			console.log(sLanguages);		

			this.$elContainer.find('.translatorLicense-container .language .value').html(sLanguages);						
			this.$elContainer.find('.translatorLicense-container .email .value').html(this._htUserProfile.user_email);
												
			if(this._htUserProfile.user_transRequestState === 2) {
				this.$elContainer.find('.applyTranslator').remove();									
				this.$elContainer.find('.processingTranslator').remove();									
			}
			else if(this._htUserProfile.user_transRequestState === 1) {
				this.$elContainer.find('.applyTranslator').remove();									
			}
			else if(this._htUserProfile.user_transRequestState === 0) {
				this.$elContainer.find('.processingTranslator').remove();									
			}

			// 프로필 사진 바꾸기
			this.$elContainer.find('.js-upload-profilepic').on("click", function() {
	
				this.$elContainer.find("input[name='profilepic-file']").click();
	
				this.$elContainer.find("input[name='profilepic-file']").on("change", function(){
					alert("file change");
					var file = this.$elContainer.find("input[name='profilepic-file']")[0].files[0];
					var formData = new FormData();
					formData.append("user_profilePic", file, file.name);
//					formData.append("user_profileText", "내 소개입니다", file.name);
					console.log(file.name, file);
					$.ajax({
						type: "POST",
						url: domain.API_DOMAIN+ "/user/profile",
						data: formData,
						cache: false,
						processData: false,
						contentType: false,
						success: function(data, status) {
							location.reload();
						}.bind(this),
						error: function(xhr, ajaxOptions, thrownError) {
							if (xhr.status == 403) {
	// 							console.log("권한이 없다요");
								//this.emit(AuthService.events.LOGGED_OUT);
							}
						}.bind(this)
					});
				}.bind(this));
			}.bind(this));
			
			this.$elContainer.find(".profileText-container").on("click", function() {
				var sNewUserProfileText = window.prompt("새로운 자기소개를 입력해 주세요", this._htUserProfile.user_profileText);
				if(sNewUserProfileText) {
					var formData = new FormData();
					formData.append("user_profileText", sNewUserProfileText);
					$.ajax({
						type: "POST",
						url: "/api/user/profile",
						data: formData,
						cache: false,
						processData: false,
						contentType: false,
						success: function(data, status) {
							location.reload();
						}.bind(this)
					});						
				}			
			}.bind(this));		
					
			this.$elContainer.find(".js-logout").on("click", function(){
				$.ajax({
					type: "GET",
					url: domain.API_DOMAIN+ "/logout",
					success: function(data, status) {
						location.reload();
					}.bind(this),
					error: function(xhr, ajaxOptions, thrownError) {
						if (xhr.status == 403) {
							console.log("권한이 없다요");
						}
					}.bind(this)
				});
			});
	};

	return ProfileView;
});