define(['jquery', 'app/AuthService', 'app/TemplateMap', "jquery-ui/selectmenu", 'jquery-ui/magnific-popup'], function($, authService, templateMap) {

	var authService = authService;
	
	function ApplyHeroView($elContainer) {

		this.$elContainer = $elContainer;
				
		this._htOption = {};

		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
				/*
		templateMap.get("ApplyHeroView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			this._template = Handlebars.compile(sTemplate);
		}.bind(this));	
		*/	
	}

	ApplyHeroView.prototype.loadTemplate = function(htParam) {
		templateMap.get("ApplyHeroView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			this._htOption.data = {};
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
				
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	ApplyHeroView.prototype.initAfterLoaded = function() {

	};
	
	ApplyHeroView.prototype.open = function() {
		var sRenderedHtml = this._template({
			data: this._htOption,
			i18n: this._htOption.i18n
		});
		
		$.magnificPopup.open({
			items: [{
				src: $(sRenderedHtml),// Dynamically created element
				type: 'inline'
			}]
		});	
		this.initEvents();		
	};
	

/*
	GnbView.prototype.render = function() {
		var $elRenderedHtml = $(this._template({
			data: this._htOption,
			i18n: this._htOption.i18n
		}));
		this.$elContainer.html($elRenderedHtml);
	};
*/
	
	ApplyHeroView.prototype.initEvents = function() {
		$(".beTranslator-popup").on("click", function(event){
			event.stopPropagation();
		});
		$(".beTranslator-popup .applyContainer .handle").on("click", function(event){
			event.stopPropagation();
			$(".beTranslator-popup .applyContainer").toggleClass("expand");
		});
		
		$(".formContainer .submit").on("click", function() {
			// 이름 이메일 언어만 있으면 된다.
			var $elInput = $(".formContainer > .formItem > input");
			
			var sName = $elInput[0].value;
			var sEmail =  $elInput[1].value;
			var sLanguage =  $elInput[4].value;
			
			var sSchool =  $elInput[2].value;
			var sMajor =  $elInput[3].value;
			var sResidence =  $elInput[5].value;
			var sScore =  $elInput[6].value;
			
			if(sName.length > 0 && sEmail.length > 0 && sLanguage.length > 0) {
				authService.applyHero({
					success : function() {
						alert("히어로 지원이 잘 접수되었습니다. 발송된 이메일을 확인하시기 바랍니다.");
						location.reload();
					},
					name : sName,
					email : sEmail,
					language : sLanguage,
					school : sSchool,
					major : sMajor,
					residence : sResidence,
					score : sScore
				});
			}
		});
/*
		

*/
/*
		var sMenuLanguageIndex = localStorage.getItem('menuLanguageIndex');
		var sLocale = Intl.DateTimeFormat().resolved.locale;

		if(!sMenuLanguageIndex) {
			if(sLocale === "ko") {
				sMenuLanguageIndex = 1;
			} else if(sLocale.indexOf("en") !== -1) {
				sMenuLanguageIndex = 1;
			}
		}
		
		
		$("#menu_language_select > option[value="+sMenuLanguageIndex+"]").attr("selected", true);			
		$("#menu_language_select").on("change", function(){
			localStorage.setItem('menuLanguageIndex', $(this).val());
			location.reload();
		});
*/
	};

	return new ApplyHeroView();
});