define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/TemplateMap', 'jquery-ui/magnific-popup'], function($, EventEmitter, requestService, authService, templateMap) {
	
	function CompletedGroupItemTicketView($elContainer, htOption) {
		EventEmitter.call(this);
		this.$elContainer = $elContainer;
		this.$elItem;
		this._htOption = $.extend({}, htOption);
		
		this._htOption.data.originalLangPicPath = templateMap.resourceMapperLangFlag(this._htOption.data.originalLangIndex);
		this._htOption.data.targetLangPicPath = templateMap.resourceMapperLangFlag(this._htOption.data.targetLangIndex);

		// 단문번역이 아니라면 필요함
		this._htOption.data.formatIconPath = templateMap.resourceMapperFormatIcon(this._htOption.data.formatIndex);
		this._htOption.data.subjectIconPath = templateMap.resourceMapperSubjectIcon(this._htOption.data.subjectIndex);

		// 번역문 엔터 교체 
		this._htOption.data.translatedText = this._htOption.data.translatedText.replace(/\n/g, "<br/>");
				
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}
	
	EventEmitter.inherits(CompletedGroupItemTicketView, EventEmitter);
	
	CompletedGroupItemTicketView.prototype.loadTemplate = function(htParam) {
		templateMap.get("CompletedGroupItemTicketView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
						
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	CompletedGroupItemTicketView.prototype.initAfterLoaded = function() {
		// 값에따라 동적으로 i18n을 적용해야하는 값을 적용한다.
		this._htOption.data.messages = this._htOption.data.messages || {};
		this._htOption.data.messages.originalLang = this._htOption.i18n[templateMap.getKeyForLanguage(this._htOption.data.originalLangIndex)];
		this._htOption.data.messages.targetLang = this._htOption.i18n[templateMap.getKeyForLanguage(this._htOption.data.targetLangIndex)];	
		this._htOption.data.messages.format = this._htOption.i18n[templateMap.getKeyForFormat(this._htOption.data.formatIndex)];
		this._htOption.data.messages.subject = this._htOption.i18n[templateMap.getKeyForSubject(this._htOption.data.subjectIndex)];
			
		this.buildUpElements();	
		this.initEvents();
	};
	
	
	CompletedGroupItemTicketView.prototype.buildUpElements = function() {
		var $elRenderedHtml = $(this._template(this._htOption));
		
		if(this._htOption.isAppending) {
			this.$elContainer.append($elRenderedHtml);
		} else {
			this.$elContainer.html($elRenderedHtml);
		}
		
		this.$elItem = $elRenderedHtml;

		if(this._htOption.rawData.request_isSos === true) {
			this.$elItem.find(".context .category").remove();
		}
	};
	
	CompletedGroupItemTicketView.prototype.initEvents = function() {
		var isTranslator = authService.state.htUserProfile.user_isTranslator;
		
		this.$elItem.find(".detailViewBtn").on("click", function() {		
			this.emit('clickDetail', {
				request_id : this._htOption.rawData.request_id,
				request_translatorPicPath : this._htOption.rawData.request_translatorPicPath,
				request_translatorId : this._htOption.rawData.request_translatorId,
				request_translatorBadgeList : this._htOption.rawData.request_translatorBadgeList,
				request_translatorComment : this._htOption.rawData.request_translatorComment,
				request_text : this._htOption.rawData.request_text,
				translated_text : this._htOption.data.translatedText,
				completed_date : this._htOption.data.completedDate,
				formatIconPath : this._htOption.data.formatIconPath,
				formatIndex : this._htOption.data.formatIndex,
				subjectIconPath : this._htOption.data.subjectIconPath,
				subjectIndex : this._htOption.data.subjectIndex,
				originalLangIndex : this._htOption.data.originalLangIndex,
				originalLangPicPath : this._htOption.data.originalLangPicPath,
				targetLangIndex : this._htOption.data.targetLangIndex,
				targetLangPicPath : this._htOption.data.targetLangPicPath,
				title : this._htOption.data.title,
				translator_name : this._htOption.data.translatorName,
				rawData : this._htOption.rawData
			});
/*
			$.magnificPopup.open({
				items: [{
					src: $('<div class="originalTextPopup"><p style="float:right">'+this._htOption.rawData.request_registeredTime+'</p><h2 style="margin-top:0">'+this._htOption.data.clientName+'<br><span>님의 번역의뢰 내용</span></h2><p class="originalText">'+this._htOption.data.originalText+'</p><div class="logoContainer"><img class="logo" src="/static/img/1.1.1logo_web_n.png"/></div></div>'), // Dynamically created element
					type: 'inline'
				}]
			});
*/			
		}.bind(this));
		
	};
	return CompletedGroupItemTicketView;
});