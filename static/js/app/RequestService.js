define(['jquery', 'eventemitter', 'app/domain'], function($, EventEmitter, domain) {
	// 의존성 Sha256, jQuery, EventEmitter
	function RequestService() {
		EventEmitter.call(this);
		
		this._lastRequestTimestamp;
		this._lastTranslationStartTimestamp;
		this._lastTranslationSubmittedTimestamp;
		
	}
	
	EventEmitter.inherits(RequestService, EventEmitter);

	RequestService.API_DOMAIN = domain.API_DOMAIN;

	RequestService.prototype.requestPaybackEmail = function(param) {
		var htRequestOption = {
			type: "GET",
			url: RequestService.API_DOMAIN + "/user/payback_email",
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success) {
					param.success(data);
				}
					
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		};
		
		$.ajax(htRequestOption);
	}
		
	// 번역중인 의뢰 읽어오기
	RequestService.prototype.getTranslatingRequest = function(param) {
		var htRequestOption = {
			type: "GET",
			url: RequestService.API_DOMAIN + "/user/translations/ongoing",
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success) {
					param.success(data);
				}
					
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		};
		
		$.ajax(htRequestOption);
	};
	
	RequestService.prototype.getTranslatedItem = function(param) {
		var htRequestOption = {
			type: "GET",
			url: RequestService.API_DOMAIN + "/user/translations/complete",
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success) {
					// 마지막 request 의 timestamp 를 저장한다
					if(data.data.length > 0) {
						this._lastTranslationStartTimestamp = data.data[data.data.length - 1].request_submittedTime;
					}
					param.success(data);
				}
					
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		};
		
		if(this._lastTranslationStartTimestamp) {
			var d1 = new Date(this._lastTranslationStartTimestamp);
			
			htRequestOption.data = {
				since : Math.floor(d1.getTime()/ 1000)
			};
		}
		
		$.ajax(htRequestOption);
	};
	
	RequestService.prototype.resubmitRequest = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/requests/incomplete/" + param.request_id,
			data: {
				"user_additionalTime": param.user_addtionalTime
			},
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};	
	
	RequestService.prototype.saveTranslating = function(param) {
		$.ajax({
			type: "PUT",
			url: RequestService.API_DOMAIN + "/user/translations/ongoing/" + param.request_id,
			data: {
				"request_translatedText": param.request_translatedText,
				"request_comment": param.request_comment,
				"request_tone": param.request_tone
			},
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");

				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.submitTranslating = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/translations/complete",
			data: {
				"request_id": param.request_id,
				"request_translatedText": param.request_translatedText,
				"request_comment": param.request_comment,
				"request_tone": param.request_tone
			},
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.moveCompleteRequestToGroup = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/requests/complete/groups/" + param.group_id,
			data: {
				"request_id": param.request_id
			},
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};
		
	
	RequestService.prototype.getRequests = function(param) {
		// param 정의
		/*
		{
			since
			success
		}
		*/
		var htRequestOption = {
			type: "GET",
			url: RequestService.API_DOMAIN + "/requests",
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success) {
					// 마지막 request 의 timestamp 를 저장한다
					if(data.data.length > 0) {
						this._lastRequestTimestamp = data.data[data.data.length - 1].request_registeredTime;
					}
					param.success(data);
				}
					
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		};
		
		if(this._lastRequestTimestamp) {
			var d1 = new Date(this._lastRequestTimestamp);
			
			htRequestOption.data = {
				since : Math.floor(d1.getTime()/ 1000)
			};
		}
		
		$.ajax(htRequestOption);
	}
	
	RequestService.prototype.getProcessingRequests = function(param) {
		htProcessingRequests = {
			pending: [],
			ongoing: [],
			complete: []
		};
		
		var fnSuccessCallback = param.success;
		
		$.when(
			$.ajax({
				type: "GET",
				url: RequestService.API_DOMAIN + "/user/requests/pending",
				dataType: "JSON",
				success: function(data, status, xhr) {
					var aRequests = data.data.filter(function(htRequest) {
						return htRequest.request_status !== -1;
					})
					htProcessingRequests.pending = aRequests;
				}.bind(this)
			}),
			
			$.ajax({
				type: "GET",
				url: RequestService.API_DOMAIN + "/user/requests/ongoing",
				dataType: "JSON",
				success: function(data, status, xhr) {
					var aRequests = data.data.filter(function(htRequest) {
						return htRequest.request_status !== -1;
					})
					htProcessingRequests.ongoing = aRequests;
				}.bind(this)
			}),
			
			$.ajax({
				type: "GET",
				url: RequestService.API_DOMAIN + "/user/requests/complete",
				dataType: "JSON",
				success: function(data, status, xhr) {
					var aRequests = data.data.filter(function(htRequest) {
						return htRequest.request_status !== -1;
					})
					var aCompleted = [];
					aRequests.forEach(function(htRequest) {
						if(!htRequest.request_title) {
							aCompleted.push(htRequest);
						}
					}.bind(this));
					htProcessingRequests.complete = aCompleted;
				}.bind(this)
			}),
			$.ajax({
				type: "GET",
				url: RequestService.API_DOMAIN + "/user/requests/incomplete",
				dataType: "JSON",
				success: function(data, status, xhr) {
					var aRequests = data.data.filter(function(htRequest) {
						return htRequest.request_status === -1;
					})
					var aIncompleted = [];
					aRequests.forEach(function(htRequest) {
						if(!htRequest.request_title) {
							aIncompleted.push(htRequest);
						}
					}.bind(this));
					htProcessingRequests.incomplete = aIncompleted;
				}.bind(this)
			})
		).then(function() {
			fnSuccessCallback(htProcessingRequests);
		});		
	};

	RequestService.prototype.getCompletedRequestsGroup = function(param) {
		$.ajax({
			type: "GET",
			url: RequestService.API_DOMAIN + "/user/requests/complete/groups",
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.renameCompletedRequestsGroup = function(param) {
		$.ajax({
			type: "PUT",
			data: {
				'group_name': param.group_name
			},
			url: RequestService.API_DOMAIN + "/user/requests/complete/groups/" + param.group_id,
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.deleteCompletedRequestsGroup = function(param) {
		$.ajax({
			type: "DELETE",
			url: RequestService.API_DOMAIN + "/user/requests/complete/groups/" + param.group_id,
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};
		
	RequestService.prototype.getCompletedRequestsGroupItems = function(param) {
		$.ajax({
			type: "GET",
			url: RequestService.API_DOMAIN + "/user/requests/complete/groups/" + param.group_id,
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};
	
	
	

	RequestService.prototype.getRequestsInCart = function(param) {
		$.ajax({
			type: "GET",
			url: RequestService.API_DOMAIN + "/user/translations/pending",
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.setTitleToCompletedRequest = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/requests/complete/"+param.request_id+"/title",
			data: {
				"title_text": param.title_text
			},
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");

				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.addRequestToCart = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/translations/pending",
			data: {
				"request_id": param.request_id
			},
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.getIncompleteRequests = function(param) {
		$.ajax({
			type: "GET",
			url: RequestService.API_DOMAIN + "/user/requests/incomplete",
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};
				
	RequestService.prototype.deleteRequest = function(param) {
		$.ajax({
			type: "DELETE",
			url: RequestService.API_DOMAIN + "/user/requests/incomplete/"+ param.request_id,
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.deleteRequestFromCart = function(param) {
		$.ajax({
			type: "DELETE",
			url: RequestService.API_DOMAIN + "/user/translations/pending/"+ param.request_id,
			dataType: "JSON",
			success: function(data, status, xhr) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};
	

	RequestService.prototype.rateRequestAndInvokeMoneyTransaction = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/requests/complete/"+param.request_id+"/rate",
			data: {
				"request_feedbackScore": param.request_feedbackScore
			},
			dataType: "JSON",
			success: function(data, status) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};
	
	RequestService.prototype.postQuickRequest = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/requests",
			data: {
				"request_clientId": param.userEmail,
				"request_originalLang": param.originalLang,
				"request_targetLang": param.targetLang,
				"request_isSos": true,
				"request_registeredTime": yyyy_mm_dd_hh_mm_ss(),
				"request_isText" : true,
				"request_context": 1,
				"request_text": param.text// 한글의 경우 서버오류 발생 
			},
			dataType: "JSON",
			success: function(data, status) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.initiateCheckout = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/requests/"+param.request_id+"/payment/start",
			data: {
				"pay_via": param.via,
				"pay_amount": param.amount,
				"use_point": parseFloat(param.use_point || 0).toFixed(2),
				"pay_by": 'web' 
			},
			dataType: "JSON",
			success: function(data, status) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};
	
	RequestService.prototype.addCompletedRequestGroup = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/requests/complete/groups",
			data: {
				"group_name": param.group_name
			},
			dataType: "JSON",
			success: function(data, status) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};

	RequestService.prototype.sendEstimatedTimeForRequest = function(param) {
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/user/translations/ongoing/"+param.request_id+"/expected",
			data: {
				"deltaFromNow": param.deltaFromNow
			},
			dataType: "JSON",
			success: function(data, status) {
				if(param.success){
					param.success(data);
				}
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				param.error(xhr, thrownError);
			}.bind(this)
		});
	};
		
		
				
	RequestService.prototype.postRequest = function(param) {
		var sCurrentTime = yyyy_mm_dd_hh_mm_ss();
		var nDeltaDue = Math.abs(parseInt((new Date(param.dueTime.replace(/-/g,'/')) - new Date()) / 1000));
		$.ajax({
			type: "POST",
			url: RequestService.API_DOMAIN + "/requests",
			data: {
				"request_clientId": param.userEmail,
				"request_originalLang": param.originalLang,
				"request_targetLang": param.targetLang,
				"request_isSos": false,
				"request_format": param.format,
				"request_subject": param.subject,
				"request_registeredTime": sCurrentTime,
				"request_isText" : true,
				"request_isPhoto": false,
				"request_isSound": false,
				"request_isFile": false,
				"reqeust_words": param.words,
				"request_deltaFromDue": nDeltaDue,
				'request_points': param.reward,
				"request_text" : param.text,
				"request_context": param.context,
			},
			dataType: "JSON",
			success: function(data, status) {
				if(param.success)
					param.success(data);
			}.bind(this),
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status == 403) {
					console.log("권한이 없다요");
				}
				if(param.error)
					param.error(xhr, thrownError);
			}.bind(this)
		});
	};
	
function yyyy_mm_dd_hh_mm_ss (sDatetime) {
	if(sDatetime) {
		var now = new Date(sDatetime);
	} else {
		var now = new Date();
	}
	var year = "" + now.getUTCFullYear();
	var month = "" + (now.getUTCMonth() + 1); if (month.length == 1) { month = "0" + month; }
	var day = "" + now.getUTCDate(); if (day.length == 1) { day = "0" + day; }
	var hour = "" + now.getUTCHours(); if (hour.length == 1) { hour = "0" + hour; }
	var minute = "" + now.getUTCMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	var second = "" + now.getUTCSeconds(); if (second.length == 1) { second = "0" + second; }
	
	return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}
	
	return new RequestService();
});