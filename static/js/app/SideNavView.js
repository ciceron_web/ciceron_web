define(['jquery', 'app/AuthService', 'app/TemplateMap', 'app/ApplyHeroView', "jquery-ui/selectmenu"], function($, authService, templateMap, ApplyHeroView) {

	var authService = authService;
	var ApplyHeroView = ApplyHeroView;
	
	function SideNavView($elContainer) {
		this._htOption = {};
		this.$elContainer = $elContainer;
		
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}
	
	SideNavView.prototype.loadTemplate = function(htParam) {
		templateMap.get("SideNavView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
						
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	SideNavView.prototype.initAfterLoaded = function() {
		this.render();
		
		var primaryPath = "/" + location.pathname.split("/")[1];
		
		// 현재 주소를 인식해서 현재 뷰에 해당하는 메뉴를 하일라이팅 시킨다.
		$("a[href=\\"+primaryPath+"] > div").addClass("selected");
		
		// 메인의 색깔을 가져와 after 장식의 색으로 세팅한다
 		addRule(".dashboard.dashboard-left .nav li div.selected::after", {
			"border-right": "15px solid " + $(".content-main > .container").css("backgroundColor")
		});   

		addRule(".dashboard.dashboard-right .nav li div.selected::after", {
			"border-left": "15px solid " + $(".content-main > .container").css("backgroundColor")
		});


		this.initEvents();
	};
		
	SideNavView.prototype.render = function() {
		var $elRenderedHtml = $(this._template(this._htOption));
		this.$elItem = $elRenderedHtml;
		this.$elContainer.prepend($elRenderedHtml);
	};
	
	SideNavView.prototype.initEvents = function() {
		this.$elContainer.on("click", ".dashboard-right", function(event){
			if(authService.state.htUserProfile.user_isTranslator === false) {
				event.preventDefault();
				ApplyHeroView.open();
			}
		})
		
	};

var addRule = (function(style){
    var sheet = document.head.appendChild(style).sheet;
    return function(selector, css){
        var propText = Object.keys(css).map(function(p){
            return p+":"+css[p]
        }).join(";");
        propText = selector + "{" + propText + "}";
        sheet.insertRule(propText, sheet.cssRules.length);
    }
})(document.createElement("style"));

	return SideNavView;
});