define(['jquery', 'app/domain', 'app/AuthService', 'app/TemplateMap', 'app/NotiItemView'], function($, domain, authService, templateMap, NotiItemView) {
	var authService = authService;

	function GlobalNotificationView($elContainer, htUserProfile) {
		this._htOption = {};
		this._htUserProfile = htUserProfile;
		
		this.$elContainer = $elContainer;
		
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}
	
	GlobalNotificationView.prototype.loadTemplate = function(htParam) {
		templateMap.get("GlobalNotificationView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				if(sKey !== "LAUGUAGE_KEY") {
					this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
				} else {
					this._htOption.i18n[sKey] = JSON.parse(this._htOption.i18n[sKey]);
				}
			}
						
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	GlobalNotificationView.prototype.initAfterLoaded = function() {
		this.render();
		this.initEvents();
	};
		
	GlobalNotificationView.prototype.render = function() {
		var $elRenderedHtml = $(this._template(this._htOption));
		this.$elContainer.html($elRenderedHtml);
	};
	
	GlobalNotificationView.prototype.initEvents = function() {
		var self = this;
		
		$(".closeBtn").on("click", function($evt) {	
			$evt.stopPropagation();
			self.$elContainer.find(".uiToggleFlyout").removeClass("show");
		});
		
		$(".alarmIcon").on("click", function($evt) {	
			$evt.stopPropagation();
			self.$elContainer.find(".uiToggleFlyout").toggleClass("show");
		});
		$(".uiToggleFlyout").on("click", function($evt) {	
			$evt.stopPropagation();
		});		
		$(window).on("click", function($evt) {	
			self.$elContainer.find(".uiToggleFlyout").removeClass("show");
		});
		/*
		$(window).on("click", function($evt) {	
			if($($evt.target).closest(".alarmIcon").length === 0) {
				self.$elContainer.find(".uiToggleFlyout").removeClass("show");
			} else {
				if($($evt.target).closest(".uiToggleFlyout").length === 0 ) {
					self.$elContainer.find(".uiToggleFlyout").toggleClass("show");
				}
			}
		});
		*/
		// inner 스크롤 시 문서 전체 스크롤 막기
		$(document).on('DOMMouseScroll mousewheel', '.innerScroll', function(ev) {
		    var $this = $(this),
		        scrollTop = this.scrollTop,
		        scrollHeight = this.scrollHeight,
		        height = $this.height(),
		        delta = (ev.type == 'DOMMouseScroll' ?
		            ev.originalEvent.detail * -40 :
		            ev.originalEvent.wheelDelta),
		        up = delta > 0;
		
		    var prevent = function() {
		        ev.stopPropagation();
		        ev.preventDefault();
		        ev.returnValue = false;
		        return false;
		    }
		
		    if (!up && -delta > scrollHeight - height - scrollTop) {
		        // Scrolling down, but this will take us past the bottom.
		        $this.scrollTop(scrollHeight);
		        return prevent();
		    } else if (up && delta > scrollTop) {
		        // Scrolling up, but this will take us past the top.
		        $this.scrollTop(0);
		        return prevent();
		    }
		});

		this.$elContainer.find(".countValue").hide();
		
		// 첫 10개의 noti정보를 받아오기
		authService.getNotification({
			success: function(data) {
				
				var nUnreadCount = data.map(function(val){
					return val.is_read === true ? 0 : 1;
				}).reduce(function(prev, cur) {
					return prev + cur;	
				});

				this.$elContainer.find(".countValue").show();

				if(nUnreadCount <= 0) {
					this.$elContainer.find(".countValue").remove();
				} else {
					this.$elContainer.find(".countValue").html(nUnreadCount);
				}
		
				var $ItemContainer = this.$elContainer.find(".js-notiItemContainer");

				for( var i in data) {
					new NotiItemView($ItemContainer, {
						isAppend: true,
						data: data[i]
					})
				}
				
				
				// 모델에 데이터를 저장한다.
				
				// unread 의 갯수만큰 카운터를 세팅한다. 0이면 보이지 않게 한다.
			
				// 받아온 데이터들로 View 를 생성해서 미리 렌더링 해 둔다.	
			}.bind(this)
		});
	};

	return GlobalNotificationView;
});