define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/TemplateMap'], function($, EventEmitter, requestService, authService, templateMap) {	

	var authService = authService;
	
	function TranslatedItemView($elContainer, htOption) {
		this.$elContainer = $elContainer;
		this._htOption = $.extend({}, htOption);
		
		
		var sTs = this._htOption.rawData.request_submittedTime;
		var oDate = new Date(sTs.replace(/-/g,'/')+ " UTC");
		sTs = [oDate.getFullYear(),oDate.getMonth()+1,oDate.getDate()].join("-") + " " +
		[oDate.getHours(),oDate.getMinutes(),oDate.getSeconds()].join(":");
		
		
		this._htOption.data.submittedTime = sTs;
		
		
		
		this._htOption.data.contextText = this._htOption.rawData.request_context;
		
		this._htOption.data.originalLangPicPath = templateMap.resourceMapperLangFlag(this._htOption.data.originalLangIndex);
		this._htOption.data.targetLangPicPath = templateMap.resourceMapperLangFlag(this._htOption.data.targetLangIndex);
		this._htOption.data.formatIconPath = templateMap.resourceMapperFormatIcon(this._htOption.data.formatIndex);
		this._htOption.data.subjectIconPath = templateMap.resourceMapperSubjectIcon(this._htOption.data.subjectIndex);
		this._htOption.data.requesteeName = this._htOption.data.clientName;				
			
		this.loadTemplate({
			callback : function() {			
				this.initAfterLoaded();
			}.bind(this)
		});
	}

	TranslatedItemView.prototype.loadTemplate = function(htParam) {
		templateMap.get("TranslatedItemView", function(sTemplate, i18n) {
			this._htOption.i18n = $.extend({}, i18n);
			
			// this._htOption.data 를 이용해서 i18n 모든항목을 한번 더 컴파일한다.
			for(var sKey in this._htOption.i18n) {
				this._htOption.i18n[sKey] = Handlebars.compile(this._htOption.i18n[sKey])(this._htOption.data);
			}
						
			this._template = Handlebars.compile(sTemplate);
			htParam.callback();		
		}.bind(this));
	};

	TranslatedItemView.prototype.initAfterLoaded = function() {
		// 값에따라 동적으로 i18n을 적용해야하는 값을 적용한다.
		this._htOption.data.messages = this._htOption.data.messages || {};
		this._htOption.data.messages.originalLang = this._htOption.i18n[templateMap.getKeyForLanguage(this._htOption.data.originalLangIndex)];
		this._htOption.data.messages.targetLang = this._htOption.i18n[templateMap.getKeyForLanguage(this._htOption.data.targetLangIndex)];	
		this._htOption.data.messages.format = this._htOption.i18n[templateMap.getKeyForFormat(this._htOption.data.formatIndex)];
		this._htOption.data.messages.subject = this._htOption.i18n[templateMap.getKeyForSubject(this._htOption.data.subjectIndex)];	
	
		this._htOption.data.messages.feedbackmsg = 
this._htOption.i18n[templateMap.getKeyForFeedback(this._htOption.data.feedback - 1)];	
			
		this.buildUpElements();		
		this.initEvents();
	};
		
	TranslatedItemView.prototype.buildUpElements = function() {
		var $elRenderedHtml = $(this._template(this._htOption));
		
		if(this._htOption.isAppending) {
			this.$elContainer.append($elRenderedHtml);
		} else {
			this.$elContainer.html($elRenderedHtml);
		}

		this.$elItem = $elRenderedHtml;

		if(this._htOption.rawData.request_isSos === true) {
			this.$elItem.find(".category").remove();
			this.$elItem.find(".toggleBtn").remove();
			
		}
					
	};
	
	TranslatedItemView.prototype.initEvents = function() {
		this.$elItem.on("click", ".toggleBtn > img", function() {
			this.$elItem.find(".contextTextContainer").toggle();
//			this.$elContainer.find(".translatedTextContainer").toggle();
		}.bind(this));					
	};
	
	return TranslatedItemView;
});