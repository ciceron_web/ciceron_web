define(['jquery', 'eventemitter', 'app/RequestService', 'app/AuthService', 'app/TranslatedItemView'], function($, EventEmitter, requestService, authService, translatedItemView) {

	function TranslatedItemListController($elContainer) {
		this.$elContainer = $elContainer;
		this._htData;
			
		this._fetchTranslatedItemList();
		
		//this.buildUpElements();
		//this.initEvents();
	}
	
	TranslatedItemListController.prototype._fetchTranslatedItemList = function() {
		requestService.getTranslatedItem({
			success: function(data) {

				this._htData = data.data;
				this.render();
			}.bind(this)
		});
	};
	
	// _htData 를 이용해 $elContainer 속에 티켓들을 렌더링한다.
	TranslatedItemListController.prototype.render = function() {
		var athData = athData || this._htData;
		
		for(var i in this._htData) {
			new translatedItemView(this.$elContainer, {
				isAppending: true,
				rawData: this._htData[i],
				data: {
					clientName: this._htData[i].request_clientName,
					request_points: this._htData[i].request_points, // 금액
					remainingHours: this._getRemainingHoursFromNow(this._htData[i].request_dueTime), // "2 시간", // TODO 남은 시간을 효과적으로 표시하는 함수를 i18n 지원하도록 만들어야함
					words: this._htData[i].request_words,
					
					formatIndex: this._htData[i].request_format,
					subjectIndex: this._htData[i].request_subject,
										
					originalText: this._htData[i].request_text,
					contextText: this._htData[i].request_context,
					clientPicPath:	this._htData[i].request_clientPicPath || "profile_pic/2015-07-22_1.54.47.png",
					originalLangIndex: this._htData[i].request_originalLang,
					targetLangIndex: this._htData[i].request_targetLang,					
					translatorPicPath:	this._htData[i].request_translatorPicPath || "profile_pic/2015-07-22_1.54.47.png",
					translatorName:	this._htData[i].request_translatorName,
					translatorsInQueue: this._htData[i].request_translatorsInQueue,
					feedback: this._htData[i].request_feedbackScore + 1
				},
				messages: {
					context: "콘텍스트", //
					remainingHours: "남은시간", //
					translateNow: "번역하기",
					contents: "원문",
					pickToCart: "장바구니",
					words: "단어",
					'delete': "삭제"
				}			
			});		
		}
	};
	
	TranslatedItemListController.prototype._getRemainingHoursFromNow = function(sTargetDatetime) {
		var diff = Math.abs(new Date() - new Date(sTargetDatetime.replace(/-/g,'/')+ " GMT"));
		var sec = diff / 1000;
		var min = sec / 60;
		var hour = min / 60;
		return parseInt(hour);
	};	
	
	return TranslatedItemListController;
});