
$(document).ready(function(){
    i18n.setLanguage($.cookie($userInfomation.user_email + "translate"));
    $nowPage = 1;

    var alreadyGetting = false;
    function getNotification(page){
        if(alreadyGetting) {
            return false;
        }
        alreadyGetting = true;
        $('.loading').show();
        $.ajax({
            url: '/api/notification?page=' + page,
            processData: false,
            contentType: false,
            cache : false,
            dataType: 'JSON',
            type: 'GET',
            error: function(){
                //location.href = "/about";
            }
        }).done(function(data){
            $(data.data).each(function(i, item) {
                
                $("#notification").append('<div class="notification2 buttons" id="notification'+item.noti_id+'"> '+(item.is_read?'':'<div class="notification_unread"><i class="fa fa-circle" aria-hidden="true"></i></div>')+' <div class="notification_icon"> <img class="notification_icon_profile" src="api/access_file/'+item.profilePic+'"/> </div> <div class="notification_content"> <span class="notification_time">'+ (new Date(item.ts)).toLocaleString() +'</span> <span class="notification_text">'+i18n.getI18n('notification_' + item.noti_typeId, item)+'</span> </div> </div>');
                
                $("#notification" + item.noti_id).click(function(){
                    $.ajax({
                        url: '/api/notification/read?noti_id=' + item.noti_id,
                        processData: false,
                        contentType: false,
                        cache : false,
                        dataType: 'JSON',
                        type: 'GET',
                        error: function(){
                            //location.href = "/about";
                        }
                    }).done(function(data){});
                    $("#notification" + item.noti_id + " .notification_unread").remove();
                    showTicket(item.request_id);
                });
                // $(".notification_close").bind('click', function(){
                //     $(this).parent().remove();
                //     if(!$.trim($("#notification").html())){
                //         $("#notification").remove();
                //         $("#subjectNotification").remove();
                //     }
                // });
            });
            alreadyGetting = false;
            $(".loading").hide();
        });
        return true;
    }

    getNotification($nowPage);
    var startCoords = 0;
    var endCoords = 0;

    $("body").on('touchstart', '.notification2', function(e){
        // e.preventDefault();
        startCoords = endCoords = e.originalEvent.targetTouches[0].pageX;
        // tempY = e.originalEvent.targetTouches[0].pageY;
        
        $(this).css({'transform': 'translate3d(0px, 0px, 0px)',
                    '-webkit-transition': 'none',
                    '-moz-transition': 'none',
                    'transition': 'none'});
    });
    $("body").on('touchmove', '.notification2', function(e){
        
        endCoords = e.originalEvent.targetTouches[0].pageX;
//                            transform: translate3d(100%, 0px, 0px);

        if(startCoords - endCoords > 60 || endCoords - startCoords > 60){
            $(this).css('transform', 'translate3d(' + (endCoords - startCoords) +'px, 0px, 0px)');
        }
        
        if(startCoords - endCoords > 60 || endCoords - startCoords > 60){
            e.preventDefault();
        }

    });
    $("body").on('touchend', '.notification2', function(e){
        e.preventDefault();
        $(this).css({'transform': 'translate3d(0px, 0px, 0px)',
                    '-webkit-transition': 'all 0.2s ease-in-out',
                    '-moz-transition': 'all 0.2s ease-in-out',
                    'transition': 'all 0.2s ease-in-out'});
        if(startCoords - endCoords > 120 || endCoords - startCoords > 120){
            var item = $(this);
            $(this).css({'transform': 'translate3d('+(endCoords - startCoords < 120?'-':'')+'100%, 0px, 0px)',
                        '-webkit-transition': 'all 0.2s ease-in-out',
                        '-moz-transition': 'all 0.2s ease-in-out',
                        'transition': 'all 0.2s ease-in-out'});
            // $(this).slideUp(200);
            setTimeout(function() {
                
                item.remove();
            }, 500);
        }
    });

    var timer_getNotification = null;
    var scrollTimerForNotification = null;
    var functionScrollForNotification = function(e){
        if($(window).scrollTop() >= $(document).height() - $(window).height()){
            if (timer_getNotification) {
                return;
            }
            if(!getNotification(++$nowPage)){
                $nowPage--;
            }
            timer_getNotification = setTimeout(function() {
                clearTimeout(timer_getNotification);
                timer_getNotification = null;
            }, 500);
        }
    };

    $(".read_more").click(function(){
        if (timer_getNotification) {
            return;
        }
        getNotification(++$nowPage);
        timer_getNotification = setTimeout(function() {
            clearTimeout(timer_getNotification);
            timer_getNotification = null;
        }, 500);
    });
    $(this).scroll(function(){
        if (scrollTimerForNotification) {
            clearTimeout(scrollTimerForNotification);   // clear any previous pending timer
        }
        scrollTimerForNotification = setTimeout(functionScrollForNotification, 5);   // set new timer
    });
    $('body').on('touchmove', function () {
        if (scrollTimerForNotification) {
            clearTimeout(scrollTimerForNotification);   // clear any previous pending timer
        }
        scrollTimerForNotification = setTimeout(functionScrollForNotification, 5);   // set new timer
    });
    functionScrollForNotification();

});
