requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    }
});

requirejs(['jquery', 'app/AuthService', 'app/RequestService', 'app/CompletedRequestListController', 'app/CompletedRequestDetailView', 'app/GnbView', 'app/SideNavView'], function($, authService, requestService, completedRequestListController, completedRequestDetailView, GnbView, SideNavView) {
	$(".beTranslatorContainer").hide();		
	authService.setUserProfile(null);

	authService.getUserProfile({
		success: function(data) { 
			var htUserProfile = data.htUserProfile;
			
			var oGnbView = new GnbView($('.topbar'));		
			var oSideNavView = new SideNavView($("#page-container"));
			
/*
			$(".translator-nav").on("click", "a", function(event) {
				// 번역가 메뉴를 클릭했을 때 유저가 번역가가 아니라면
				if(htUserProfile.user_isTranslator === false) {
					// 번역가가 되세요 팝업을 띄움 
					// 팝업은 현재 API 가 없으므로 추후 구현 
					event.preventDefault();
				}	 
			});	
*/	

			var oCompletedRequestListController = new completedRequestListController($(".js-donerequests"));

			var oCompletedRequestDetailView = new completedRequestDetailView($(".js-donerequests")); 
			
			oCompletedRequestListController.on("clickDetail", function(htEvent) {
				oCompletedRequestDetailView.open(htEvent);
			});
	
		},
		error: function(){
			location.href='/about';
		}
	}); 
});