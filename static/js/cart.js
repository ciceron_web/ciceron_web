requirejs.config({
    baseUrl: '/static/js/lib',
    paths: {
        app: '../app'
    }
});

requirejs(['jquery', 'app/domain', 'app/AuthService', 'app/RequestService', 'app/CartListController', 'app/GnbView', 'app/SideNavView'], function($, domain, authService, requestService, cartListController, GnbView, SideNavView) {
	$(".beTranslatorContainer").hide();		
	authService.setUserProfile(null);

	authService.getUserProfile({
		success: function(data) { 
			var htUserProfile = data.htUserProfile;
			
			if(htUserProfile.user_isTranslator === false) {
				location.href='/';
			}

			var oGnbView = new GnbView($('.topbar'));		
			var oSideNavView = new SideNavView($("#page-container"));

			
			if(authService.state.htUserProfile.user_isTranslator === false) {
				location.href='/';
			}
			new cartListController($(".requestList-container"));	
		},
		error: function(){
			location.href='/about';
		}
	}); 
});